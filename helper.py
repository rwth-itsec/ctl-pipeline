import argparse

def keyboard_interrupt_ignoring(function):
    """Decorator catching KeyboardInterrupt, SystemExit exceptions to prevent cluttered commandline output on exit (e.g. using ctrl-c). The processes will be terminated nevertheless."""
    def wrapped(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except (KeyboardInterrupt, SystemExit):
            pass
    return wrapped

def san_to_url(san):
    while san.startswith('*.') and len(san) >= 2:
        san = san[2:]
    while san.startswith('.') and len(san) >= 1:
        san = san[1:]
    if len(san) == 0:
        return None

    return f'https://{san}'

import datetime
def valid_date_string(date_string):
    try:
        return datetime.datetime.strptime(date_string, "%d.%m.%Y")
    except ValueError:
        raise argparse.ArgumentTypeError(f'Invalid date: {date_string}')

import os
def valid_existing_file_path(file_path):
    if os.path.isfile(file_path):
        return file_path
    else:
        raise argparse.ArgumentTypeError(f'File does not exist at path: {file_path}')

def valid_existing_directory_path(file_path):
    if os.path.isdir(file_path):
        return file_path
    else:
        raise argparse.ArgumentTypeError(f'Directory does not exist at path: {file_path}')

def valid_existing_path(file_path):
    if os.path.isdir(file_path) or os.path.isfile(file_path):
        return file_path
    else:
        raise argparse.ArgumentTypeError(f'Directory/file does not exist at path: {file_path}')

import os
def get_env(env_name, default, type_cast):
    if os.getenv(env_name) == '' or os.getenv(env_name) is None:
        return default
    return type_cast(os.getenv(env_name))
