import argparse
import logging
import sys
import os
import time
import datetime
from multiprocessing import Process, Queue
from multiprocessing.queues import Empty as QueueEmptyException

from certificate import CertificateTransparencyLogSource
import pipeline_components
from classifier_configurations import ClassifierConfigurations
from helper import valid_existing_directory_path, valid_existing_file_path, get_env

class LiveRunner:

    """How many downloaders should run in parallel."""
    NUMBER_OF_DOWNLOADERS = get_env('PL_NUMBER_OF_DOWNLOADERS', 4, int)

    """How many classification runners should be used per classifier."""
    NUMBER_OF_CLASSIFIERS = get_env('PL_NUMBER_OF_CLASSIFIERS', 1, int)

    """How many screenshot grabbers should be running per classifier (not per classification runner)."""
    NUMBER_OF_SCREENSHOT_GRABBERS = get_env('PL_NUMBER_OF_SCREENSHOT_GRABBERS', 4, int)

    """Number of certificates which should be classified in parallel by the classification pipeline."""
    NUMBER_OF_PARALLEL_CLASSIFICATIONS = 8

    """Maximum size of any queue in the pipeline setup."""
    MAX_QUEUE_SIZE = 10_000


    def __init__(self, configuration):
        self.logger = logging.getLogger('pipeline')
        self._configuration = configuration
        self._setup_logging()

    def start(self):
        processes = []

        # Download stage
        ctl_download_outputs = []
        for _ in self._configuration.classifier_configurations():
            ctl_download_outputs.append(Queue(LiveRunner.MAX_QUEUE_SIZE))
        assert len(ctl_download_outputs) > 0, 'No classification configurations found.'

        for begin in range(LiveRunner.NUMBER_OF_DOWNLOADERS):
            live_downloader = pipeline_components.LiveDownloader(ctl_download_outputs[0], self._configuration.ctl)
            for output in ctl_download_outputs[1:]:
                live_downloader.add_additional_output(output) # Add additional outputs
            live_downloader.set_logger(self.logger)
            processes.append(live_downloader.process())

        for index, classifier_configuration in enumerate(self._configuration.classifier_configurations()):
            # Classification stage
            classification_output = Queue(LiveRunner.MAX_QUEUE_SIZE)
            for _ in range(LiveRunner.NUMBER_OF_CLASSIFIERS):
                processes.append(self._build_classifier_stage(ctl_download_outputs[index], classification_output, classifier_configuration).process())

            next_queue = classification_output
            number_of_predecessors = LiveRunner.NUMBER_OF_CLASSIFIERS
            if self._configuration.take_screenshots:
                screenshot_output_queue = Queue(LiveRunner.MAX_QUEUE_SIZE)
                next_queue = screenshot_output_queue
                number_of_predecessors = LiveRunner.NUMBER_OF_SCREENSHOT_GRABBERS
                for _ in range(LiveRunner.NUMBER_OF_SCREENSHOT_GRABBERS):
                    processes.append(self._build_screenshot_stage(classification_output, screenshot_output_queue, classifier_configuration['screenshots_directory']).process())

            # Store results stage
            processes.append(self._build_saver_stage(next_queue, classifier_configuration['results_database'], number_of_predecessors = number_of_predecessors).process())

        for process in processes:
            process.start()

        try:
            for process in processes:
                process.join()
        except (KeyboardInterrupt, SystemExit):
            pass

    def _build_classifier_stage(self, input_queue, output_queue, classifier_configuration):
        classification_runner = pipeline_components.ClassificationRunner(input_queue, output_queue, classifier_configuration, LiveRunner.NUMBER_OF_PARALLEL_CLASSIFICATIONS)
        classification_runner.set_logger(self.logger)
        classification_runner.set_number_of_direct_predecessors(int(LiveRunner.NUMBER_OF_DOWNLOADERS / LiveRunner.NUMBER_OF_CLASSIFIERS))
        return classification_runner

    def _build_screenshot_stage(self, input_queue, output_queue, screenshots_directory):
        screenshot_grabber = pipeline_components.ScreenshotGrabber(input_queue, output_queue, screenshots_directory, '', '')
        screenshot_grabber.set_logger(self.logger)
        screenshot_grabber.set_number_of_direct_predecessors(int(LiveRunner.NUMBER_OF_CLASSIFIERS / LiveRunner.NUMBER_OF_SCREENSHOT_GRABBERS))
        return screenshot_grabber

    def _build_saver_stage(self, input_queue, database_path, number_of_predecessors):
        saver = pipeline_components.ResultSaver(input_queue, None, database_path)
        saver.set_logger(self.logger)
        saver.set_number_of_direct_predecessors(number_of_predecessors)
        return saver

    def _setup_logging(self):
        # Setup logging
        self.logger.setLevel(logging.WARNING)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s', datefmt='%Y.%m.%d %H:%M:%S')

        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(logging.WARNING)
        stdout_handler.setFormatter(formatter)
        self.logger.addHandler(stdout_handler)

class LiveRunnerConfiguration:
    def __init__(self, arguments):
        super(LiveRunnerConfiguration, self).__init__()
        self._arguments = arguments
        self._timestamp = int(time.time())
        self.output_directory_base = arguments.output_directory
        self.classifier_pathes = arguments.classifiers
        self.log_name = arguments.ct_log_name
        self.ctl = CertificateTransparencyLogSource[self.log_name.upper()]
        self.take_screenshots = arguments.take_screenshots

        self.assert_validity()
        self.prepare_directories()

    def assert_validity(self):
        assert os.path.isdir(self.output_directory_base), f'The base output directory {self.output_directory_base} does not exist.'
        assert CertificateTransparencyLogSource[self.log_name.upper()] is not None, 'The provided log name is not known.'
        for path in self.classifier_pathes:
            self._assert_is_valid_classifier_identifier(path)

    def _assert_is_valid_classifier_identifier(self, identifier):
        if identifier in ClassifierConfigurations.special_classifiers:
            return
        assert os.path.isfile(identifier), f'Cannot find classifier: {identifier}'

    @property
    def base_subdirectory_path(self):
        return os.path.join(self.output_directory_base, f'run_{self.ctl.name}_live_{self._timestamp}')

    def prepare_directories(self):
        os.makedirs(self.base_subdirectory_path)
        for classifier_configuration in self.classifier_configurations():
            os.makedirs(classifier_configuration['screenshots_directory'])

        with open(os.path.join(self.base_subdirectory_path, 'used_config.txt'), 'w+') as file:
            file.write(f'Started run at: {datetime.datetime.now()}\n\n')
            for key in self._arguments.__dict__:
                file.write(f"{key}: {self._arguments.__dict__[key]}\n")
            file.write("\nClassifier configurations:\n")
            classifier_configurations = list(self.classifier_configurations())
            for configuration in classifier_configurations:
                file.write(f"- {configuration['name']}\n")
                for key in configuration:
                    file.write(f"{key}: {str(configuration[key] or '-')}\n")
                file.write("\n")

    def classifier_configurations(self):
        for path in self.classifier_pathes:
            name = None
            if path in ClassifierConfigurations.special_classifiers:
                name = path
                path = None
            else:
                name = os.path.splitext(os.path.basename(os.path.normpath(path)))[0]

            classifier_class, thresholds, used_features, meta_classifier = ClassifierConfigurations.configuration_for_classifier(name)
            yield {
                'path': path,
                'name': name,
                'results_database': os.path.join(self.base_subdirectory_path, f'results_{name}.db'),
                'used_features': used_features,
                'threshold': thresholds,
                'meta_classifier': meta_classifier,
                'classifier_class': classifier_class,
                'screenshots_directory': os.path.join(self.base_subdirectory_path, f'screenshots_{name}'),
            }


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Live classifiation of a CT log.')
    parser.add_argument('--ct-log-name', '-l', type=str, required=True, help="The name of the CT log to use.")
    parser.add_argument('--output-directory', '-o', type=valid_existing_directory_path, required=True, help="A directory where the output should be stores (all files will be placed in a sub-directory).")
    parser.add_argument('--classifiers', '-c', type=str, nargs='+', required=True, help="A list of classifiers to use.")
    parser.add_argument('--take-screenshots', '-s', action='store_true', help="A flag indicating whether or not to take screenshots.")

    arguments = parser.parse_args()

    if 0 in [LiveRunner.NUMBER_OF_DOWNLOADERS, LiveRunner.NUMBER_OF_CLASSIFIERS]:
        print('At least one downloader, classifier, verifier and negatives writer is needed.')
        exit(1)

    print('Running live classification, press CRTL-C to stop.')
    configuration = LiveRunnerConfiguration(arguments)
    LiveRunner(configuration).start()
