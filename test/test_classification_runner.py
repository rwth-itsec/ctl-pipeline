import unittest
from unittest.mock import Mock, patch
from .test_pipeline_component import TestPipelineComponent

from pipeline_components import ClassificationRunner

class ClassifierStub:
    def predict(self, certificates):
        return [[s] for s in ([0.32, 0.87] * len(certificates))[:len(certificates)]]

class TestClassificationRunner(TestPipelineComponent):

    def setUp(self):
        super(TestClassificationRunner, self).setUp()

        config = {
        'classifier_class': ClassifierStub,
        'threshold': 0.8,
        'meta_classifier': None,
        'used_features': None
        }

        self.sut = ClassificationRunner(self.input, self.output, config, 8)
        self.setup_component(self.sut)
        self.sut.before_start()

    def test_run_single(self):
        self.fill_input_with_certificates()

        self.sut.run_single()

        for i in range(7):
            self.assert_next_output_queue_element(i, 0.32 if i % 2 == 0 else 0.87, i % 2 != 0, f'{i}.test.com')

        self.wait_for_queues_to_finish()

    def assert_next_output_queue_element(self, serial_number, score, is_positive, first_san):
        certificate, pipeline_result = self.output.get()
        self.assertEqual(certificate.serial_number, str(serial_number))
        self.assertEqual(pipeline_result.classification_score, score)
        self.assertEqual(pipeline_result.is_positive, is_positive)
        self.assertEqual(pipeline_result.sans[0].san, first_san)


    def fill_input_with_certificates(self, count = 8):
        for i in range(count):
            self.input.put(self.build_mock_certificate(f'{i}.test.com', f'issuer{i}', i))
