import unittest
from unittest.mock import Mock

from classifier.public_suffixes import PublicSuffixes

class TestPublicSuffixes(unittest.TestCase):

    def test_get_valid_tlds(self):
        self.assertTrue('.bd' in PublicSuffixes.get_valid_tlds())
        self.assertTrue('.de' in PublicSuffixes.get_valid_tlds())
        self.assertTrue('.uk' in PublicSuffixes.get_valid_tlds())
        self.assertTrue('.az' in PublicSuffixes.get_valid_tlds())
        self.assertTrue('.bb' in PublicSuffixes.get_valid_tlds())
        self.assertFalse('com.ba' in PublicSuffixes.get_valid_tlds())
        self.assertFalse('*.bd' in PublicSuffixes.get_valid_tlds())
        self.assertFalse('co.bd' in PublicSuffixes.get_valid_tlds())
        self.assertTrue('.xn--80adxhks' in PublicSuffixes.tlds)
        self.assertFalse('.xn--mgba3a4fra.ir' in PublicSuffixes.tlds)

        for tld in PublicSuffixes.tlds:
            self.assertEqual(tld.count('.'), 1)
            self.assertTrue(tld.startswith('.'))

    def test_split_public_suffix_and_registered_name(self):
        # com is a public suffix
        self.assert_domain_name_split('com', '', 'com')
        self.assert_domain_name_split('test.com', 'test', 'com')
        self.assert_domain_name_split('domain.test.com', 'domain.test', 'com')
        self.assert_domain_name_split('*.test.com', 'test', 'com')
        self.assert_domain_name_split('*.*.test.com', 'test', 'com')

        # *.bd is a public suffix
        self.assert_domain_name_split('test.bd', 'test', 'bd')
        self.assert_domain_name_split('super.test.bd', 'super', 'test.bd')
        self.assert_domain_name_split('*.bd', '', 'bd')

        # co.uk is a public suffix
        # blogspot.co.uk is a public suffix
        self.assert_domain_name_split('test.co.uk', 'test', 'co.uk')
        self.assert_domain_name_split('testco.uk', 'testco', 'uk')
        self.assert_domain_name_split('other.test.co.uk', 'other.test', 'co.uk')
        self.assert_domain_name_split('other.blogspot.co.uk', 'other', 'blogspot.co.uk')
        self.assert_domain_name_split('blogspot.co.uk', 'blogspot', 'co.uk')

        # appspot.com is a public suffix
        self.assert_domain_name_split('appspot.com', 'appspot', 'com')
        self.assert_domain_name_split('my_page.appspot.com', 'my_page', 'appspot.com')
        self.assert_domain_name_split('sub_of.my_page.appspot.com', 'sub_of.my_page', 'appspot.com')
        self.assert_domain_name_split('*.*.appspot.com', 'appspot', 'com')

        # *.kobe.jp is a public suffix (excluding: !city.kobe.jp)
        self.assert_domain_name_split('a.b.kobe.jp', 'a', 'b.kobe.jp')
        self.assert_domain_name_split('a.kobe.jp', 'a', 'kobe.jp')
        self.assert_domain_name_split('city.kobe.jp', 'city', 'kobe.jp')
        self.assert_domain_name_split('test.city.kobe.jp', 'test.city', 'kobe.jp')

        # Case sensitivity
        self.assert_domain_name_split('TEST.COM', 'test', 'com')

    def test_hostname_without_public_suffix(self):
        registered_parts, suffix = PublicSuffixes.split_public_suffix_and_registered_name(('dotcom',))
        self.assertEqual(registered_parts, ('dotcom',))
        self.assertEqual(suffix, '')

    def test_hostname_with_invalid_public_suffix(self):
        registered_parts, suffix = PublicSuffixes.split_public_suffix_and_registered_name(('test','not_a_tld'))
        self.assertEqual(registered_parts, ('test','not_a_tld'))
        self.assertEqual(suffix, '')


    def assert_domain_name_split(self, domain_name, expected_registered_name, expected_public_suffix):
        registered_parts, suffix = PublicSuffixes.split_public_suffix_and_registered_name(domain_name.split('.'))
        self.assertEqual('.'.join(registered_parts), expected_registered_name, 'Assertion failure on the registered name.')
        self.assertEqual(suffix, '.' + expected_public_suffix, 'Assertion failure on the public suffix.')