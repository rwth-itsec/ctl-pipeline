import unittest
from datetime import datetime, timezone

from pipeline_components.pipeline_database import PipelineDatabase
from pipeline_components.pipeline_models import *
from certificate import CertificateTransparencyLogSource

from verification.malicious_url import MaliciousURLSource

class TestPipelineResultDatabase(unittest.TestCase):

    def setUp(self):
        # In memory testing database
        self.database = PipelineDatabase(":memory:")

    def test_save_pipeline_result(self):
        result = PipelineResult(ctl = CertificateTransparencyLogSource.ROCKETEER, ctl_index = 42, issuer_name = "Test Issuer", serial_number = "beef", created_at = datetime(2019, 11, 30, 16, 0), classification_score = 0.56)

        self.database.save_pipeline_result(result)

        fetched_result = self.database.cursor().execute("SELECT * FROM pipeline_results WHERE ctl_index=42 AND ctl = 'ROCKETEER'").fetchone()
        self.assertIsNotNone(fetched_result)
        self.assertEqual(fetched_result['ctl_index'], 42)
        self.assertEqual(fetched_result['ctl'], 'ROCKETEER')
        self.assertEqual(fetched_result['created_at'], result.created_at)
        self.assertEqual(fetched_result['classification_score'], result.classification_score)
        self.assertEqual(fetched_result['issuer_name'], 'Test Issuer')
        self.assertEqual(fetched_result['serial_number'], 'beef')

        # Test parsing into python object
        result_object = PipelineResult(**fetched_result)
        self.assertEqual(result_object._id, fetched_result['id'])
        self.assertEqual(result_object.ctl_index, 42)
        self.assertEqual(result_object.ctl, CertificateTransparencyLogSource.ROCKETEER)
        self.assertEqual(result_object.created_at, datetime(2019, 11, 30, 16, 0))
        self.assertEqual(result_object.classification_score, 0.56)

    def test_save_pipeline_result_as_update(self):
        result = PipelineResult(ctl = CertificateTransparencyLogSource.ROCKETEER, ctl_index = 42, created_at = datetime(2019, 11, 30, 16, 0), classification_score = 0.56)

        self.database.save_pipeline_result(result)

        fetched_result = self.database.cursor().execute("SELECT * FROM pipeline_results WHERE ctl_index=42 AND ctl = 'ROCKETEER'").fetchone()
        self.assertIsNotNone(fetched_result)
        # Set id to not duplicate the result
        result._id = fetched_result['id']

        self.database.save_pipeline_result(result)

        count = self.database.cursor().execute("SELECT COUNT(*) as count FROM pipeline_results").fetchone()
        self.assertEqual(count['count'], 1)

    def test_save_pipeline_result_with_relations(self):
        result = PipelineResult(ctl = CertificateTransparencyLogSource.ROCKETEER, ctl_index = 42, created_at = datetime(2019, 11, 30, 16, 0), classification_score = 0.56)

        self.database.save_pipeline_result(result)

        fetched_result = self.database.cursor().execute("SELECT * FROM pipeline_results WHERE ctl_index=42 AND ctl = 'ROCKETEER'").fetchone()
        self.assertIsNotNone(fetched_result)
        # Set id to not duplicate the result
        result._id = fetched_result['id']

        result.add_san(San(san = 'test.com'))
        result.add_san(San(san = 'example.com'))

        result.add_screenshot(Screenshot(file_path = 'test.png', created_at = datetime(2020, 1, 1, 20, 15)))

        result.add_verification_hit(VerificationHit(created_at = datetime(2020, 1, 1, 20, 0), source = MaliciousURLSource.VIRUSTOTAL))

        self.database.save_pipeline_result(result)

        sans_result = self.database.cursor().execute("SELECT * FROM sans WHERE result_id = ?", (fetched_result['id'],)).fetchall()
        self.assertIsNotNone(sans_result)
        self.assertEqual(len(sans_result), 2)
        self.assertEqual(sans_result[0]['san'], 'test.com')
        self.assertEqual(sans_result[1]['san'], 'example.com')

        screenshot_result = self.database.cursor().execute("SELECT * FROM screenshots WHERE result_id = ?", (fetched_result['id'],)).fetchone()
        self.assertIsNotNone(screenshot_result)
        self.assertEqual(screenshot_result['file_path'], 'test.png')
        self.assertEqual(screenshot_result['created_at'], datetime(2020, 1, 1, 20, 15))

        verification_hit_result = self.database.cursor().execute("SELECT * FROM verification_hits WHERE result_id = ?", (fetched_result['id'],)).fetchone()
        self.assertIsNotNone(verification_hit_result)
        self.assertEqual(verification_hit_result['source'], 'VIRUSTOTAL')
        self.assertEqual(verification_hit_result['created_at'], datetime(2020, 1, 1, 20, 0))

        # Test parsing into native python objects
        san = San(**sans_result[0])
        self.assertEqual(san._id, sans_result[0]['id'])
        self.assertEqual(san.san, 'test.com')
        self.assertEqual(san._result_id, result._id)


        screenshot = Screenshot(**screenshot_result)
        self.assertEqual(screenshot._id, screenshot_result['id'])
        self.assertEqual(screenshot.file_path, 'test.png')
        self.assertEqual(screenshot._result_id, result._id)
        self.assertEqual(screenshot.created_at, datetime(2020, 1, 1, 20, 15))


        verification_hit = VerificationHit(**verification_hit_result)
        self.assertEqual(verification_hit._id, verification_hit_result['id'])
        self.assertEqual(verification_hit.source, MaliciousURLSource.VIRUSTOTAL)
        self.assertEqual(verification_hit._result_id, result._id)
        self.assertEqual(verification_hit.created_at, datetime(2020, 1, 1, 20, 0))


