import unittest
from unittest.mock import Mock
import os

from verification.verification_pipeline import VerificationPipeline
from verification.hash_prefix_database import HashPrefixDatabase
from verification.hostname_database import HostnameDatabase
from certificate import Certificate, CertificateTransparencyLogSource

from classifier.feature_extractor import CertificateFeatures, CertificateSANFeatures

from verification.malicious_url import MaliciousURL, MaliciousURLSource

class TestVerificationPipeline(unittest.TestCase):

    def setUp(self):
        self.sut = VerificationPipeline('phish.db', 'prefixes.db')
        self.sut._url_database = Mock()
        self.sut._hash_prefix_database = Mock()
        self.sut._classifier = Mock()

        CertificateFeatures._alexa_top_one_million_frequencies = [1060042, 305183, 1060600, 401494, 1190886, 197749, 351826, 304054, 858306, 61825, 224797, 585072, 945137, 754238, 1508385, 370891, 20871, 911834, 770327, 780402, 447403, 163938, 141757, 68255, 188686, 85995, 16102, 23485, 21933, 13251, 14570, 8187, 7597, 7610, 9120, 7756, 0, 1166246, 126161, 45]
        # Only use the defined database interfaces
        import verification.verification_pipeline
        verification.verification_pipeline._HASH_PREFIX_DATABASE_CLASS = HashPrefixDatabase
        verification.verification_pipeline._HOSTNAME_DATABASE_CLASS = HostnameDatabase

    def tearDown(self):
        # Cleanup database files used only in the tests
        if os.path.exists('phish.db'):
            os.remove('phish.db')
        if os.path.exists('prefixes.db'):
            os.remove('prefixes.db')

    def test_check_hostname_in_url_database(self):
        self.sut._url_database.query_hostname.return_value = [
            MaliciousURL(url = 'http://test.com', source = MaliciousURLSource.PHISHTANK),
            MaliciousURL(url = 'http://test.com/hello', source = MaliciousURLSource.PHISHTANK),
            MaliciousURL(url = 'http://test.com', source = MaliciousURLSource.OPEN_PHISH),
        ]

        malicious_urls = self.sut._check_hostname_in_url_database('test.com')
        self.assertEqual(len(malicious_urls), 3)
        self.assertEqual(malicious_urls[0].url, 'http://test.com')
        self.sut._url_database.query_hostname.assert_called_with('test.com')

    def test_run_hostname_match(self):
        self.sut._check_hostname_in_url_database = Mock(return_value = [
            MaliciousURL(url = 'http://test.com', source = MaliciousURLSource.PHISHTANK),
            MaliciousURL(url = 'http://test.com', source = MaliciousURLSource.OPEN_PHISH)
        ])
        self.sut._online_cascade.check_hostname = Mock(return_value = [])
        self.sut._hash_prefix_database.check_hostname = Mock(return_value = False)

        result = self.sut.run(self.build_certificate('test.com'), full_check = True)
        self.assertEqual(len(result.found_in_sources), 2)
        # Actually asserts whether the lists are containing the same items (and not the count)
        self.assertCountEqual(result.found_in_sources, [MaliciousURLSource.OPEN_PHISH, MaliciousURLSource.PHISHTANK])
        self.assertCountEqual(result.malicious_urls, self.sut._check_hostname_in_url_database.return_value)
        self.sut._check_hostname_in_url_database.assert_called_with('test.com')
        self.sut._online_cascade.check_hostname.assert_not_called()

    def test_run_no_match(self):
        self.sut._check_hostname_in_url_database = Mock(return_value = [])
        self.sut._online_cascade.check_hostname = Mock(return_value = [])
        self.sut._hash_prefix_database.check_hostname_active = Mock(return_value = False)

        result = self.sut.run(self.build_certificate('test.com'), full_check = True)
        self.assertEqual(result.found_in_sources, None)
        self.sut._check_hostname_in_url_database.assert_called_with('test.com')
        self.sut._online_cascade.check_hostname.assert_called_with('test.com', False, full_check = True)

    def test_online_source_match(self):
        self.sut._check_hostname_in_url_database = Mock(return_value = [])
        self.sut._online_cascade.check_hostname = Mock(return_value = [
            MaliciousURL(url = 'http://test.com', source = MaliciousURLSource.VIRUSTOTAL)
        ])
        self.sut._hash_prefix_database.check_hostname_active = Mock(return_value = False)

        result = self.sut.run(self.build_certificate('test.com'), full_check = True)
        self.assertEqual(len(result.found_in_sources), 1)
        self.assertCountEqual(result.found_in_sources, [MaliciousURLSource.VIRUSTOTAL])
        self.assertCountEqual(result.malicious_urls, self.sut._online_cascade.check_hostname.return_value)
        self.sut._check_hostname_in_url_database.assert_called_with('test.com')
        self.sut._online_cascade.check_hostname.assert_called_with('test.com', False, full_check = True)

    def test_online_source_match_with_gsb(self):
        self.sut._check_hostname_in_url_database = Mock(return_value = [])
        self.sut._online_cascade.check_hostname = Mock(return_value = [
            MaliciousURL(url = 'http://test.com', source = MaliciousURLSource.VIRUSTOTAL),
            MaliciousURL(url = 'http://test.com', source = MaliciousURLSource.GOOGLE_SAFE_BROWSING_LOOKUP)
        ])
        self.sut._hash_prefix_database.check_hostname_active = Mock(return_value = True)

        result = self.sut.run(self.build_certificate('test.com'), full_check = True)
        self.assertEqual(len(result.found_in_sources), 2)
        self.assertCountEqual(result.found_in_sources, [MaliciousURLSource.VIRUSTOTAL, MaliciousURLSource.GOOGLE_SAFE_BROWSING_LOOKUP])
        self.assertCountEqual(result.malicious_urls, self.sut._online_cascade.check_hostname.return_value)
        self.sut._check_hostname_in_url_database.assert_called_with('test.com')
        self.sut._online_cascade.check_hostname.assert_called_with('test.com', True, full_check = True)

    def test_online_and_offline_match(self):
        self.sut._check_hostname_in_url_database = Mock(return_value = [
            MaliciousURL(url = 'http://test.com', source = MaliciousURLSource.PHISHTANK),
        ])
        self.sut._online_cascade.check_hostname = Mock(return_value = [
            MaliciousURL(url = 'http://test.com', source = MaliciousURLSource.VIRUSTOTAL)
        ])
        self.sut._hash_prefix_database.check_hostname = Mock(return_value = True)

        result = self.sut.run(self.build_certificate('test.com'), full_check = True)
        self.assertEqual(len(result.found_in_sources), 1)
        self.assertCountEqual(result.found_in_sources, [MaliciousURLSource.PHISHTANK])
        self.assertCountEqual(result.malicious_urls, self.sut._check_hostname_in_url_database.return_value)
        self.sut._check_hostname_in_url_database.assert_called_with('test.com')
        self.sut._online_cascade.check_hostname.assert_not_called()

    def test_non_full_check(self):
        self.sut._check_hostname_in_url_database = Mock(return_value = [])
        self.sut._online_cascade.check_hostname = Mock(return_value = [])
        self.sut._hash_prefix_database.check_hostname_active = Mock(return_value = True)

        result = self.sut.run(self.build_certificate('test.com'), full_check = False)

        self.sut._check_hostname_in_url_database.assert_called_with('test.com')
        self.sut._online_cascade.check_hostname.assert_called_with('test.com', True, full_check = False)

    def test_integration_with_databases(self):
        # Since all the methods of the databases are mocked, it needs to be checked,
        # whether they actually exist or were refactored / renamed
        # Since this pipeline implementation does not include the databases, we expect to receive NotImplementedErrors
        pipeline = VerificationPipeline('phish.db', 'prefixes.db')
        with self.assertRaises(NotImplementedError):
            pipeline._url_database.query_hostname('test')
        with self.assertRaises(NotImplementedError):
            pipeline._hash_prefix_database.check_hostname('test')

    def build_certificate(self, hostname):
        features = CertificateFeatures()
        features.feature_vectors = Mock(return_value = [[hostname]])
        san_feature = CertificateSANFeatures(['test.com'], hostname)
        features.extracted_san_features = [san_feature]
        return Certificate(CertificateTransparencyLogSource.ROCKETEER, 42, None, features)
