import unittest
from unittest.mock import Mock
from .test_pipeline_component import TestPipelineComponent

from pipeline_components import HistoricalDownloader
from certificate import CertificateTransparencyLogSource

from util.ct_query import CtLogQuery
from classifier.feature_extractor import CertificateFeatures

class TestHistoricalDownloader(TestPipelineComponent):

    def setUp(self):
        self.extract = CertificateFeatures.extract
        super(TestHistoricalDownloader, self).setUp()
        self.sut = HistoricalDownloader(self.output, CertificateTransparencyLogSource.ROCKETEER, 0, 10, 0, 1, 10)
        self.setup_component(self.sut)

    def tearDown(self):
        CertificateFeatures.extract = self.extract

    def test_run_single(self):
        self.mock_download_and_extraction(10)

        self.sut.run_single()

        out = self.output.get()
        self.assertEqual(out.log_index, 0)
        self.assertEqual(out.serial_number, '0')

        out = self.output.get()
        self.assertEqual(out.log_index, 1)
        self.assertEqual(out.serial_number, '1')

        self.wait_for_queues_to_finish()

    def mock_download_and_extraction(self, number_of_certs_to_inject):
        self.sut._certificate_log_query = Mock()

        certs = [Mock() for _ in range(number_of_certs_to_inject)]
        for (i, c) in enumerate(certs):
            c.serial_number = i
        certs = [{'leaf_cert': c} for c in certs]
        self.sut._certificate_log_query.get_certs_in_range = Mock(return_value = certs)
        CertificateFeatures.extract = Mock(return_value = self.build_mocked_certificate_features())
