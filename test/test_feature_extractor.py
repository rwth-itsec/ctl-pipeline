import unittest
from unittest.mock import Mock

from datetime import datetime, timedelta

import classifier.x509wrapper as x509wrapper
from classifier.feature_extractor import CertificateFeatures, CertificateSANFeatures, FeatureExtractionError
from classifier.feature_encoders import OneHotIntegerFeatureEncoder
from classifier.multivalued_feature import NGramFeature


class TestFeatureExtractor(unittest.TestCase):

    def setUp(self):
        CertificateFeatures._alexa_top_one_million_frequencies = [1060042, 305183, 1060600, 401494, 1190886, 197749, 351826, 304054, 858306, 61825, 224797, 585072, 945137, 754238, 1508385, 370891, 20871, 911834, 770327, 780402, 447403, 163938, 141757, 68255, 188686, 85995, 16102, 23485, 21933, 13251, 14570, 8187, 7597, 7610, 9120, 7756, 0, 1166246, 126161, 45]

    def test_feature_vector_without_encoding(self):
        self.mock_certificate(sans = ['test.com'])
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = ['is_ov', 'is_dv'], san_features = [])
        self.assertEqual(feature_vector, [[0, 1], [0, 1]])

        feature_vector = certificate.feature_vectors(features = ['is_ov', 'is_dv'], san_features = ['sub_cn_is_com'])
        self.assertEqual(feature_vector, [[1, 0, 1], [1, 0, 1]])

    def test_feature_vector_with_one_hot_encoding(self):
        self.mock_certificate(sans = ['test.com'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = ['is_ov', 'is_dv'], san_features = ['alphabet_size'], encoders = { 'alphabet_size': OneHotIntegerFeatureEncoder(5) })
        self.assertEqual(feature_vector, [[0,0,0,1,0, 0, 1]])

    def test_feature_vector_has_no_sublists_for_n_gram_stats(self):
        self.mock_certificate(sans = ['test.com'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = ['is_ov', 'is_dv'], san_features = ['n_grams'])
        self.assertEqual(feature_vector, [[0.4714045207910317, 1.0, 1.3333333333333333, 1, 2, 1.0, 1.5, 0.0, 1.0, 1.0, 1, 1, 1.0, 1.0, 0.0, 1.0, 1.0, 1, 1, 1.0, 1.0, 0, 1]])

    def test_has_uppercase_letter(self):
        self.mock_certificate(sans = ['test.com', 'TEST.com'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = [], san_features = ['has_uppercase_letters'])
        self.assertEqual(feature_vector, [[0] , [1]])

    def test_num_tokens(self):
        self.mock_certificate(sans = ['*.a-b.*.c_test.com', '*_TEST.com'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = [], san_features = ['num_tokens'])
        self.assertEqual(feature_vector, [[7] , [3], [2]])

    def test_extract_num_dash(self):
        self.mock_certificate(sans = ['test.com', 'test-example.com', 'so-many-dashes.de'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = [], san_features = ['num_dash'])
        self.assertEqual(feature_vector, [[0] , [1], [2]])

    def test_extract_num_dash_rd(self):
        self.mock_certificate(sans = ['te-st.test.com', 'example-test.test-example.com', 'hello-world.so-many-dashes.de'], oid_cn = 'te-st.test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = [], san_features = ['num_dash_rd'])
        self.assertEqual(feature_vector, [[0] , [1], [2]])

    def test_tld_in_token(self):
        self.mock_certificate(sans = ['*.abc.*.c_test.com', '*.ab._TEST.com'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = [], san_features = ['tld_in_token'])
        self.assertEqual(feature_vector, [[1] , [0], [0]])

    def test_https_in_domain(self):
        self.mock_certificate(sans = ['*.abc.*.c_test.cohttpsm.com', '*.ahttpb._TEST.com', 'http-s.com', 'hTtpS.cOm'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = [], san_features = ['https_in_domain'])
        self.assertEqual(feature_vector, [[1] , [0], [0], [1], [0]])

    def test_longest_token(self):
        self.mock_certificate(sans = ['*.ahttpb._TEST.com'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = [], san_features = ['longest_token'])
        self.assertEqual(feature_vector, [[6], [4]])

    def test_special_char_ratio(self):
        self.mock_certificate(sans = ['*.ahttpb._TEST.com'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = [], san_features = ['special_char_ratio'])
        self.assertEqual(feature_vector, [[0.2777777777777778] , [0.125]])

    def test_is_ip(self):
        self.mock_certificate(sans = ['192.225.0.3', '255.0.255.0', 'test.com'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = [], san_features = ['is_ip'])
        self.assertEqual(feature_vector, [[1], [1], [0]])

    def test_is_idn_domain(self):
        self.mock_certificate(sans = ['test.xn--test-rand.de','noxn.--in.this.com', 'xn--is-domain.sub2.rd.de', 'test.tld.xn--fiqs8s'], oid_cn = 'testxn--if.this.does.work')
        certificate = CertificateFeatures.extract({})
        feature_vector = certificate.feature_vectors(features = [], san_features = ['is_idn_domain'])
        self.assertEqual(feature_vector, [[1],[0],[1],[1],[0]])

    def test_multivalued_feature_column_names(self):
        self.mock_certificate(sans = ['test.com'], oid_cn = 'test.com')
        column_names = CertificateFeatures.feature_vector_column_names(features = ['is_ov'], san_features = ['n_grams'])
        self.assertEqual(column_names, [
            '1_gram_std', '1_gram_median', '1_gram_mean', '1_gram_min', '1_gram_max', '1_gram_bottom_quartile', '1_gram_top_quartile',
            '2_gram_std', '2_gram_median', '2_gram_mean', '2_gram_min', '2_gram_max', '2_gram_bottom_quartile', '2_gram_top_quartile',
            '3_gram_std', '3_gram_median', '3_gram_mean', '3_gram_min', '3_gram_max', '3_gram_bottom_quartile', '3_gram_top_quartile',
            'is_ov'
        ])

        column_names = CertificateFeatures.feature_vector_column_names(features = ['is_ov'], san_features = [NGramFeature(1, 'std'), NGramFeature(3, 'median')])
        self.assertEqual(column_names, ['1_gram_std', '3_gram_median', 'is_ov'])

        column_names = CertificateFeatures.feature_vector_column_names(features = ['is_ov'], san_features = ['length'], encoders = { 'length': OneHotIntegerFeatureEncoder(4) })
        self.assertEqual(column_names, ['length_one_hot_0', 'length_one_hot_1', 'length_one_hot_2', 'length_one_hot_3', 'is_ov'])

    def test_multivalued_feature_values(self):
        self.mock_certificate(sans = ['test.com'], oid_cn = 'test.com')
        certificate = CertificateFeatures.extract({})
        feature_vectors = certificate.feature_vectors(features = ['is_ov'], san_features = ['n_grams'])
        self.assertEqual(feature_vectors, [[0.4714045207910317, 1.0, 1.3333333333333333, 1, 2, 1.0, 1.5, 0.0, 1.0, 1.0, 1, 1, 1.0, 1.0, 0.0, 1.0, 1.0, 1, 1, 1.0, 1.0, 0]])


        feature_vectors = certificate.feature_vectors(features = ['is_ov'], san_features = [NGramFeature(1, 'std'), NGramFeature(1, 'mean'), NGramFeature(3, 'median')])
        self.assertEqual(feature_vectors, [[0.4714045207910317, 1.3333333333333333, 1.0, 0]])

        feature_vectors = certificate.feature_vectors(features = ['is_ov'], san_features = ['length'], encoders = { 'length': OneHotIntegerFeatureEncoder(4) })
        self.assertEqual(feature_vectors, [[0, 0, 0, 1, 0]])

    def test_key_algorithm_and_size(self):
        self.mock_certificate()
        certificate = CertificateFeatures.extract({})
        self.assertEqual(certificate.key_size, 1024)
        self.assertEqual(certificate.key_algorithm, 1)

    def test_extract_basic_features(self):
        self.mock_certificate(oid_c = 'DE', oid_cn = 'itsec.rwth-aachen.de', policy_count = 3)
        certificate = CertificateFeatures.extract({})
        self.assertTrue(certificate.is_dv)
        self.assertFalse(certificate.is_ov)
        self.assertFalse(certificate.is_ev)
        self.assertTrue(certificate.sub_has_c)
        self.assertFalse(certificate.sub_has_st)
        self.assertFalse(certificate.sub_has_l)
        self.assertTrue(certificate.sub_only_cn)
        self.assertTrue(certificate.sub_has_cn)
        self.assertFalse(certificate.cn_in_san)
        self.assertEqual(certificate.sub_dn_count, 1)
        self.assertEqual(certificate.sub_char_count, 10)
        self.assertEqual(certificate.sub_ext_count, 0)
        self.assertEqual(certificate.policies_count, 3)
        self.assertEqual(certificate.valid_period, 20)
        self.assertEqual(certificate.san_count, 3)
        self.assertAlmostEqual(certificate.average_sd_count, 2.6666666666666665)
        self.assertEqual(certificate.san_tld_count, 1)
        self.assertEqual(certificate.all_names, ['rwth-aachen.de', 'www.rwth-aachen.de', 'itsec.rwth-aachen.de'])
        self.assertEqual(certificate.sans, ['rwth-aachen.de', 'www.rwth-aachen.de'])
        self.assertFalse(certificate.is_wildcard)
        self.assertFalse(certificate.has_ocsp)
        self.assertFalse(certificate.has_cdp)
        self.assertEqual(certificate.issuer, 1)

        self.assertAlmostEqual(certificate.extracted_san_features[0].san_to_alexa_entropy, 0.8967711525633365)
        self.assertAlmostEqual(certificate.extracted_san_features[1].san_to_alexa_entropy, 1.1663310007288132)
        self.assertAlmostEqual(certificate.extracted_san_features[2].san_to_alexa_entropy, 0.6125203386813524)

    def test_basic_features_cn_is_san(self):
        self.mock_certificate(oid_cn = 'rwth-aachen.de', policy_count = 0)
        certificate = CertificateFeatures.extract({})
        self.assertTrue(certificate.is_dv)
        self.assertFalse(certificate.is_ov)
        self.assertFalse(certificate.is_ev)
        self.assertFalse(certificate.sub_has_c)
        self.assertFalse(certificate.sub_has_st)
        self.assertFalse(certificate.sub_has_l)
        self.assertTrue(certificate.sub_only_cn)
        self.assertTrue(certificate.sub_has_cn)
        self.assertTrue(certificate.cn_in_san)
        self.assertEqual(certificate.sub_dn_count, 1)
        self.assertEqual(certificate.sub_char_count, 10)
        self.assertEqual(certificate.sub_ext_count, 0)
        self.assertEqual(certificate.policies_count, 0)
        self.assertEqual(certificate.valid_period, 20)
        self.assertEqual(certificate.san_count, 2)
        self.assertEqual(certificate.average_sd_count, 2.5)
        self.assertEqual(certificate.san_tld_count, 1)
        self.assertEqual(certificate.all_names, ['rwth-aachen.de', 'www.rwth-aachen.de'])
        self.assertEqual(certificate.sans, ['rwth-aachen.de', 'www.rwth-aachen.de'])
        self.assertFalse(certificate.is_wildcard)
        self.assertFalse(certificate.has_ocsp)
        self.assertFalse(certificate.has_cdp)
        self.assertEqual(certificate.issuer, 1)

        self.assertAlmostEqual(certificate.extracted_san_features[0].san_to_alexa_entropy, 0.8967711525633365)
        self.assertAlmostEqual(certificate.extracted_san_features[1].san_to_alexa_entropy, 1.1663310007288132)

    def test_common_name_not_existent(self):
        self.mock_certificate(oid_cn = None)
        certificate = CertificateFeatures.extract({})
        self.assertAlmostEqual(certificate.extracted_san_features[0].san_to_alexa_entropy, 0.8967711525633365)
        self.assertAlmostEqual(certificate.extracted_san_features[1].san_to_alexa_entropy, 1.1663310007288132)
        self.assertEqual(len(certificate.extracted_san_features), 2)

    def test_basic_san_features(self):
        self.mock_certificate(sans = ['rwth-aachen.de'], oid_cn = None)
        certificate = CertificateFeatures.extract({})
        san_features = certificate.extracted_san_features[0]
        self.assertAlmostEqual(san_features.sub_cn_entropy, 2.341994252232425)
        self.assertEqual(san_features.name_san_entropy, 0)
        self.assertFalse(san_features.sub_cn_is_com)

    def test_basic_san_features_with_multiple_sans(self):
        self.mock_certificate(sans = ['rwth-aachen.de', 'other.com'])
        certificate = CertificateFeatures.extract({})
        san_features = certificate.extracted_san_features[1]
        self.assertAlmostEqual(san_features.sub_cn_entropy, 2.0431918705451206)
        self.assertAlmostEqual(san_features.name_san_entropy, 0.325012553432638)
        self.assertTrue(san_features.sub_cn_is_com)

    def test_san_features_ip_cn_and_hostname_san(self):
        self.mock_certificate(sans = ['rwth-aachen.de'], oid_cn = '10.0.0.1')
        certificate = CertificateFeatures.extract({})
        self.assertAlmostEqual(certificate.extracted_san_features[0].name_san_entropy, 0.35296409794877925)
        self.assertAlmostEqual(certificate.extracted_san_features[1].name_san_entropy, 0.903720134509062)

    def test_fancy_san_features_helper(self):
        self.mock_certificate(sans = ['www.rwth-aachen.de', 'other.com'])
        certificate = CertificateFeatures.extract({})
        san_features = certificate.extracted_san_features[0]
        self.assertEqual(san_features._extract_dot_split({}), ('www', 'rwth-aachen', 'de'))
        self.assertEqual(san_features._extract_dot_split_suffix_free({}), ('www', 'rwth-aachen',))
        self.assertEqual(san_features._extract_public_suffix({}), '.de')
        self.assertEqual(san_features._extract_joined_dot_split_suffix_free({}), 'wwwrwth-aachen')
        self.assertEqual(list(san_features._extract_unigram({})), [4, 1, 1, 2, 1, 2, 1, 1, 1])

    def test_fancy_san_features(self):
        self.mock_certificate(sans = ['www.rwth-aachen.de'])
        certificate = CertificateFeatures.extract({})
        san_features = certificate.extracted_san_features[0]

        self.assertAlmostEqual(san_features._extract_vowel_ratio({}), 0.23076923076923078)
        self.assertEqual(san_features._extract_digit_ratio({}), 0)
        self.assertEqual(san_features._extract_length({}), 18)
        self.assertTrue(san_features._extract_contains_wwwdot({}))
        self.assertFalse(san_features._extract_contains_subdomain_of_only_digits({}))
        self.assertEqual(san_features._extract_subdomain_lengths_mean({}), 7)
        self.assertEqual(san_features._extract_parts({}), 2)
        self.assertFalse(san_features._extract_contains_ipv4_addr({}))
        self.assertFalse(san_features._extract_contains_digits({}))
        self.assertTrue(san_features._extract_has_valid_tld({}))
        self.assertFalse(san_features._extract_contains_one_char_subdomains({}))
        self.assertFalse(san_features._extract_prefix_repetition({}))
        self.assertAlmostEqual(san_features._extract_char_diversity({}), 0.6428571428571429)
        self.assertFalse(san_features._extract_contains_tld_as_infix({}))
        self.assertEqual(san_features._extract_n_grams({}), [0.9558139185602919, 1.0, 1.5555555555555556, 1, 4, 1.0, 2.0, 0.2763853991962833, 1.0, 1.0833333333333333, 1, 2, 1.0, 1.0, 0.0, 1.0, 1.0, 1, 1, 1.0, 1.0])
        self.assertEqual(san_features._extract_alphabet_size({}), 9)
        self.assertAlmostEqual(san_features._extract_shannon_entropy({}), 2.9502120649147465)
        self.assertEqual(san_features._extract_hex_part_ratio({}), 0)
        self.assertEqual(san_features._extract_underscore_ratio({}), 0)
        self.assertAlmostEqual(san_features._extract_ratio_of_repeated_chars({}), 0.3333333333333333)
        self.assertAlmostEqual(san_features._extract_consecutive_consonant_ratio({}), 0.6428571428571429)
        self.assertAlmostEqual(san_features._extract_consecutive_digits_ratio({}), 0)

    def test_fancy_san_features_with_more_suspicious_domain(self):
        self.mock_certificate(sans = ['120.1.2.98.brand_name.com.accounts.mydomain.net'])
        certificate = CertificateFeatures.extract({})
        san_features = certificate.extracted_san_features[0]

        self.assertAlmostEqual(san_features._extract_vowel_ratio({}), 0.35714285714285715)
        self.assertEqual(san_features._extract_digit_ratio({}), 0.19444444444444445)
        self.assertEqual(san_features._extract_length({}), 47)
        self.assertFalse(san_features._extract_contains_wwwdot({}))
        self.assertTrue(san_features._extract_contains_subdomain_of_only_digits({}))
        self.assertEqual(san_features._extract_subdomain_lengths_mean({}), 4.5)
        self.assertEqual(san_features._extract_parts({}), 8)
        self.assertTrue(san_features._extract_contains_ipv4_addr({}))
        self.assertTrue(san_features._extract_contains_digits({}))
        self.assertTrue(san_features._extract_has_valid_tld({}))
        self.assertTrue(san_features._extract_contains_one_char_subdomains({}))
        self.assertFalse(san_features._extract_prefix_repetition({}))
        self.assertAlmostEqual(san_features._extract_char_diversity({}), 0.5555555555555556)
        self.assertTrue(san_features._extract_contains_tld_as_infix({}))
        self.assertEqual(san_features._extract_alphabet_size({}), 20)
        self.assertAlmostEqual(san_features._extract_shannon_entropy({}), 4.07243125132212)
        self.assertEqual(san_features._extract_hex_part_ratio({}), 0.5)
        self.assertEqual(san_features._extract_underscore_ratio({}), 0.027777777777777776)
        self.assertAlmostEqual(san_features._extract_ratio_of_repeated_chars({}), 0.4)
        self.assertAlmostEqual(san_features._extract_consecutive_consonant_ratio({}), 0.3333333333333333)
        self.assertAlmostEqual(san_features._extract_consecutive_digits_ratio({}), 0.1388888888888889)
        self.assertEqual(san_features._extract_n_grams({}), [1.1224972160321824, 1.0, 1.8, 1, 4, 1.0, 2.25, 0.3352356401746214, 1.0, 1.1290322580645162, 1, 2, 1.0, 1.0, 0.17141982574219336, 1.0, 1.0303030303030303, 1, 2, 1.0, 1.0])

    def test_public_suffixes_wildcards(self):
        self.mock_certificate(sans = ['test.name.bd'])
        certificate = CertificateFeatures.extract({})
        san_features = certificate.extracted_san_features[0]

        self.assertEqual(san_features.san, 'test.name.bd')
        self.assertEqual(san_features.public_suffix, '.name.bd')
        self.assertEqual(san_features.dot_split_suffix_free, ('test',))

    def test_public_suffixes_case_sensitivity(self):
        self.mock_certificate(sans = ['TEST.name.BD'])
        certificate = CertificateFeatures.extract({})
        san_features = certificate.extracted_san_features[0]

        self.assertEqual(san_features.san, 'test.name.bd')
        self.assertEqual(san_features.public_suffix, '.name.bd')
        self.assertEqual(san_features.dot_split_suffix_free, ('test',))

    def test_public_suffixes_without_wildcards(self):
        self.mock_certificate(sans = ['test.name.aero'])
        certificate = CertificateFeatures.extract({})
        san_features = certificate.extracted_san_features[0]

        self.assertEqual(san_features.san, 'test.name.aero')
        self.assertEqual(san_features.public_suffix, '.aero')
        self.assertEqual(san_features.dot_split_suffix_free, ('test', 'name'))

    def test_invalid_hostname(self):
        self.mock_certificate(sans = ['test.not_a_tld'])
        with self.assertRaises(FeatureExtractionError):
            certificate = CertificateFeatures.extract({})

        self.mock_certificate(sans = ['mail'])
        with self.assertRaises(FeatureExtractionError):
            certificate = CertificateFeatures.extract({})


    def mock_certificate(self, **options):
        is_dv = options.get('is_dv', True)
        x509wrapper.is_dv = Mock(return_value = is_dv)
        is_ov = options.get('is_ov', False)
        x509wrapper.is_ov = Mock(return_value = is_ov)
        is_ev = options.get('is_ev', False)
        x509wrapper.is_ev = Mock(return_value = is_ev)

        def subject_get_from_oid(certificate, oid):
            if oid == x509wrapper.oid_c:    # country
                return options.get('oid_c')
            if oid == x509wrapper.oid_st:   # state
                return options.get('oid_st')
            if oid == x509wrapper.oid_l:    # locality
                return options.get('oid_l')
            if oid == x509wrapper.oid_cn:   # common name
                return options.get('oid_cn', 'CommonName.com')
        x509wrapper.subject_get_from_oid = Mock(side_effect = subject_get_from_oid)

        x509wrapper.get_subject_dn_count = Mock(return_value = options.get('dn_count', 1))
        x509wrapper.get_subject_length = Mock(return_value = options.get('subject_length', 10))
        x509wrapper.get_extension_count = Mock(return_value = options.get('extension_count', 0))
        x509wrapper.get_policies = Mock(return_value = ([{}] * options.get('policy_count', 0))) # only count is needed, not the content
        x509wrapper.valid_from = Mock(return_value = options.get('valid_from', datetime.now() - timedelta(days = 10)))
        x509wrapper.valid_until = Mock(return_value = options.get('valid_until', datetime.now() + timedelta(days = 10)))
        x509wrapper.get_san = Mock(return_value = options.get('sans', ['rwth-aachen.de', 'www.rwth-aachen.de']))
        x509wrapper.is_wildcard = Mock(return_value = options.get('is_wildcard', False))
        x509wrapper.has_ocsp_in_aia = Mock(return_value = options.get('has_ocsp_in_aia', False))
        x509wrapper.has_cdp = Mock(return_value = options.get('has_cdp', False))
        x509wrapper.issuer_get_from_oid = Mock(return_value = options.get('issuer', "Let's Encrypt Authority X3"))
        x509wrapper.get_key_algorithm = Mock(return_value = options.get('key_algorithm', "rsa"))
        x509wrapper.get_key_size = Mock(return_value = options.get('key_size', 1024))