import unittest

from classifier.hostname_validator import HostnameValidationError, assert_valid_hostname


class TestHostnameValidator(unittest.TestCase):
    def test_ip_address(self):
        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('111.222.333.444')

        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('111.a.333.444')

        assert_valid_hostname('1.2.3.4')

    def test_hostname(self):
        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('test')

        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('mail.not_a_tld')

        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('mail.something.')

        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('.mail.something')

        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('*')

        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('*.*')

        assert_valid_hostname('test.com')
        assert_valid_hostname('example.com.de.com')

    def test_invalid_characters(self):
        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('(not a domain)')

        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('website/.com')

        with self.assertRaises(HostnameValidationError):
            assert_valid_hostname('this .com')

        assert_valid_hostname('*.wildcard.domain.xyz')
