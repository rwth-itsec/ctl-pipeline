import unittest
from unittest.mock import Mock
from pipeline_components.pipeline_component import PipelineComponent

from classifier.feature_extractor import CertificateFeatures, CertificateSANFeatures
import classifier.x509wrapper as x509wrapper
from certificate import Certificate, CertificateTransparencyLogSource
from multiprocessing import Queue
import logging
import time

class TestPipelineComponent(unittest.TestCase):

    def setUp(self):
        self.output = Queue()
        self.input = Queue()

    def test_output(self):
        self.sut = PipelineComponent(self.input, self.output)

        second_output = Queue()
        self.sut.add_additional_output(second_output)

        self.sut._output(1)

        self.assertEqual(self.output.get(), 1)
        self.assertEqual(second_output.get(), 1)

    def wait_for_queues_to_finish(self):
        time.sleep(0.1) # Let the queues finish to not get a broken pipe exception: https://stackoverflow.com/a/36360020

    def setup_component(self, component):
        logger = logging.getLogger('test')
        component.set_logger(logger)

    def build_mock_certificate(self, hostname, issuer, serial_number):
        features = CertificateFeatures()
        features.extracted_san_features = [CertificateSANFeatures([hostname], hostname)]
        x509wrapper.issuer_get_from_oid = Mock(return_value = issuer)
        certificate = Certificate(CertificateTransparencyLogSource.ROCKETEER, serial_number, self.mock_raw_certificate(serial_number, issuer), features)
        return certificate

    def mock_raw_certificate(self, serial_number, issuer):
        class MockRawCertificate:
            pass
        certificate = MockRawCertificate()
        certificate.serial_number = serial_number
        certificate.issuer = issuer
        return certificate

    def build_mocked_certificate_features(self):
        return CertificateFeatures()