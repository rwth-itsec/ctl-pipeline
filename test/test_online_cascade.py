import unittest
from unittest.mock import Mock

from online_sources.online_cascade import OnlineCascade

from verification.malicious_url import MaliciousURL, MaliciousURLSource


class TestOnlineCascade(unittest.TestCase):

    def setUp(self):
        mock_database = Mock()
        self.sut = OnlineCascade(mock_database)
        self.sut._virustotal = Mock()
        self.sut._google_safe_browsing_lookup = Mock()
        mock_database.save_malicious_url = Mock()

    def test_check_hostname_all_sources_positive(self):
        test_url = 'https://example.com'
        vt_url = MaliciousURL(url = test_url, source = MaliciousURLSource.VIRUSTOTAL)
        gsb_url = MaliciousURL(url = test_url, source = MaliciousURLSource.GOOGLE_SAFE_BROWSING_LOOKUP)

        self.sut._virustotal.check_url = Mock(return_value = (False, vt_url))
        self.sut._google_safe_browsing_lookup.check_url = Mock(return_value = (False, gsb_url))

        self.assertEqual(len(self.sut.check_hostname('example.com', True, full_check = True)), 2)
        self.assertEqual(self.sut._url_database.save_malicious_url.call_count, 2)
        self.sut._url_database.save_malicious_url.assert_any_call(vt_url)
        self.sut._url_database.save_malicious_url.assert_any_call(gsb_url)

    def test_check_hostname_no_sources_positive(self):
        self.sut._virustotal.check_url = Mock(return_value = (False, None))
        self.sut._google_safe_browsing_lookup.check_url = Mock(return_value = (False, None))

        self.assertEqual(len(self.sut.check_hostname('example.com', True, full_check = True)), 0)
        self.assertEqual(self.sut._url_database.save_malicious_url.call_count, 0)


    def test_check_hostname_not_in_gsb(self):
        test_url = 'https://example.com'
        vt_url = MaliciousURL(url = test_url, source = MaliciousURLSource.VIRUSTOTAL)
        self.sut._virustotal.check_url = Mock(return_value = (True, vt_url))
        self.sut._google_safe_browsing_lookup.check_url = Mock(return_value = (False, None))

        self.assertEqual(len(self.sut.check_hostname('example.com', False, full_check = True)), 1)
        self.assertEqual(self.sut._url_database.save_malicious_url.call_count, 1)

        self.assertEqual(self.sut._google_safe_browsing_lookup.check_url.call_count, 0)

    def test_non_full_check(self):
        test_url = 'https://example.com'
        vt_url = MaliciousURL(url = test_url, source = MaliciousURLSource.VIRUSTOTAL)
        self.sut._virustotal.check_url = Mock(return_value = (True, vt_url))
        self.sut._google_safe_browsing_lookup.check_url = Mock(return_value = (False, None))

        self.assertEqual(len(self.sut.check_hostname('example.com', False, full_check = False)), 0)
        self.assertEqual(self.sut._url_database.save_malicious_url.call_count, 0)

        self.assertEqual(self.sut._virustotal.check_url.call_count, 0)
        self.assertEqual(self.sut._google_safe_browsing_lookup.check_url.call_count, 0)
