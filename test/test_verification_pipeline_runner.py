import unittest
from unittest.mock import Mock, patch
from .test_pipeline_component import TestPipelineComponent

from multiprocessing import Queue
from pipeline_components import VerificationPipelineRunner, PipelineResult
from verification.verification_pipeline import VerificationPipelineResult

from verification.malicious_url import MaliciousURL, MaliciousURLSource

class TestVerificationPipelineRunner(TestPipelineComponent):

    def setUp(self):
        super(TestVerificationPipelineRunner, self).setUp()
        self.negatives_output_queue = Queue()
        self.sut = VerificationPipelineRunner(self.input, self.output, self.negatives_output_queue, '', '')
        self.setup_component(self.sut)

    def test_run_single_negative_no_hits(self):
        certificate, pipeline_result = self.build_queue_element_for_index(0, 0.3)
        self.mock_verification_pipeline(certificate, False)
        self.input.put((certificate, pipeline_result))

        self.sut.run_single()

        self.assert_next_output(False, 0)

    def test_run_single_positive_no_hits(self):
        certificate, pipeline_result = self.build_queue_element_for_index(0, 0.9)
        self.mock_verification_pipeline(certificate, False)
        self.input.put((certificate, pipeline_result))

        self.sut.run_single()

        self.assert_next_output(True, 0)

    def test_run_single_negative_with_hits(self):
        certificate, pipeline_result = self.build_queue_element_for_index(0, 0.3)
        self.mock_verification_pipeline(certificate, True)
        self.input.put((certificate, pipeline_result))

        self.sut.run_single()

        self.assert_next_output(False, 1)

    def test_run_single_positive_with_hits(self):
        certificate, pipeline_result = self.build_queue_element_for_index(0, 0.9)
        self.mock_verification_pipeline(certificate, True)
        self.input.put((certificate, pipeline_result))

        self.sut.run_single()

        self.assert_next_output(True, 1)

    def assert_next_output(self, positive, number_of_verification_hits):
        if positive:
            certificate, pipeline_result = self.output.get()
        else:
            certificate, pipeline_result = self.negatives_output_queue.get()

        self.assertEqual(pipeline_result.is_positive, positive)
        self.assertEqual(len(pipeline_result.verification_hits), number_of_verification_hits)

    def mock_verification_pipeline(self, certificate, found):
        self.sut._verification_pipeline = Mock()
        self.sut._verification_pipeline.run = Mock(side_effect = [self.build_verification_pipeline_result(certificate, found)])

    def build_queue_element_for_index(self, index, score, threshold = 0.8):
        certificate = self.build_mock_certificate(f'{index}.test.com', f'issuer{index}', index)
        pipeline_result = PipelineResult(ctl = certificate.certificate_transparency_log, ctl_index = certificate.log_index, issuer_name = certificate.issuer, serial_number = certificate.serial_number, classification_score = score, is_positive = score >= threshold)
        return (certificate, pipeline_result)

    def build_verification_pipeline_result(self, certificate, found):
        if not found:
            return VerificationPipelineResult(certificate, None)
        else:
            return VerificationPipelineResult(certificate, [MaliciousURL(source = MaliciousURLSource.PHISHTANK, url = 'http://test.com')])