import unittest
from unittest.mock import Mock

from classifier.rf_classifier import RFClassifier

class StubClassifier:
    def __init__(self):
        self.prediction_samples = []
        self.training_features = None
        self.training_labels = None

    def predict_proba(self, samples):
        self.prediction_samples += [samples]
        return [[0, 0.1] * len(samples)]

    def fit(self, features, labels):
        self.training_features = features
        self.training_labels = labels

class StubFeatures:
    def __init__(self, feature_count):
        self.features = [i for i in range(feature_count)]

    def feature_vectors(self):
        return [self.features]

class TestRFClassifier(unittest.TestCase):

    def setUp(self):
        self.sut = RFClassifier()
        self.sut.classifier = StubClassifier()


    def test_predict_colums_removal(self):
        self.sut.feature_names = ['f_' + str(i) for i in range(10)]
        self.sut.used_features = ['f_0', 'f_2', 'f_3', 'f_4', 'f_6', 'f_7', 'f_8', 'f_9']

        self.sut.predict([StubFeatures(10)])

        self.assertEqual(self.sut.classifier.prediction_samples[0].columns.tolist(), ['f_0', 'f_2', 'f_3', 'f_4', 'f_6', 'f_7', 'f_8', 'f_9'])
        self.assertEqual(self.sut.classifier.prediction_samples[0].values.tolist(), [[0, 2, 3, 4, 6, 7, 8, 9]])


    def test_fit_colums_removal(self):
        self.sut.feature_names = ['f_' + str(i) for i in range(10)]
        self.sut.used_features = ['f_0', 'f_2', 'f_3', 'f_4', 'f_8', 'f_9']

        self.sut.fit([StubFeatures(10)], [0])

        self.assertEqual(self.sut.classifier.training_features.columns.tolist(), ['f_0', 'f_2', 'f_3', 'f_4', 'f_8', 'f_9'])
        self.assertEqual(self.sut.classifier.training_features.values.tolist(), [[0, 2, 3, 4, 8, 9]])
