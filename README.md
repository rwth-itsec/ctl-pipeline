# Finding Phish in a Haystack: A Pipeline for Phishing Classification on Certificate Transparency Logs

This is the repository containing the code which was written to perform the evaluations presented in the paper:

Arthur Drichel, Vincent Drury, Justus von Brandt, and Ulrike Meyer. 2021. Finding Phish in a Haystack: A Pipeline for Phishing Classification on Certificate Transparency Logs. In _The 16th International Conference on Availability, Reliability and Security (ARES 2021), August 17–20, 2021, Vienna, Austria_. ACM, New York, NY, USA, 12 pages. https://doi.org/10.1145/3465481.3470111

## Requirements
- Python 3.7 or higher
- Packages listed in requirements.txt `pip install -r requirements.txt`

The assumption in the documentation is, that Python is installed as `python` and pip as `pip`.

To create screenshots a selenium setup is needed. Currently the Firefox browser is used, which leads to the requirements:
- Firefox (needs to be in the PATH variable)
- Geckodriver for installed Firefox version (needs to be in the PATH variable)
- Working/tested setup is: Mozilla Firefox 80.0 and geckodriver 0.27.0
- Relevant code location: `pipeline_components/screenshot_grabber.py`

If the pipeline is executed on a server/a system without a display, the `Xvfb` program can be used to create virtual displays.

## What is this and how does it work?
Check the `docs` folder for detailed instructions on the different components.

## Todo before getting started
In order to be able to verify the positives and negatives of the classifier (distinguish true positives, false positives, true negatives and false negatives), some ground truth data is needed.

The pipeline uses a database containing hostnames and a database for Google Safe Browsing (GSB) hash prefixes as offline sources. Furthermore the GSB Lookup API and the VirusTotal API are used as online sources. This leads to several todos:

### Open Source Intelligence data
In order to be able to verify the classifications in the pipeline, the open source intelligence (OSINT) databases are used. They need to be created and populated using the `osint_downloader` module. More information on the usage can be found in the `docs/osint_downloader.md` file.

The pipeline is configured to use the `osint_downloader` databases on default. They can however be exchanged by other databases. To use other databases the two interfaces found in `verification/hostname_database.py` and `verification_pipeline/hash_prefix_database.py` need to be implemented in the custom database classes. To use the custom databases in the pipeline, the `_HOSTNAME_DATABASE_CLASS` and `_HASH_PREFIX_DATABASE_CLASS` variables need to be changed in the `verification/verification_pipeline.py` file. Since the two databases are independent, they can also be exchanged individually.

### Top domain list
Add a top domains list to the repository at the path: `classifier/top_domains.csv`. This list should countain an index and a domain name in each line (comma separated). E.g. `"42,example.com"` is a valid line, without the `"`.

### Public suffix list
Add a file containing public suffixes at the path: `classifier/public_suffixes`. This file should contain one public suffix per line, not perceeded with a dot. An example public suffix list can be found at: https://publicsuffix.org/list/public_suffix_list.dat

### API credentials
To use the APIs some secrets are needed. All of these secrets should be inserted in a file called `credentials.py`. This file can be created from the template called `credentials.py.example`, by renaming/copying it and the inserting the credentials at the indicated positions.

## Tests
The unit tests can be run using the command `python -m unittest` in the root directory of the project.

## Datasets
For reproducibility we make the used training datasets available on request.
The used test data can easily be retrieved by the pipeline.

## Acknowledgments
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 833418.
It was also supported by the research training group “Human Centered Systems Security” sponsored by the state of North-Rhine Westphalia.


## Licenses
This repository contains licensed code, here is a comprehensive list of all third party code used:

### CT leaf decoder
This code in the file `util/ct_leaf_decoder.py` is adapted from: https://github.com/CaliDog/certstream-server-python/blob/master/certstream/certlib.py

The license can be found in the file itself, as well as in the linked repository (https://github.com/CaliDog/certstream-server-python/blob/master/LICENSE). The code was modified in some locations to adapt it for the use case, the changes are mainly deletions / simplifications.


### FANCI Features
The `CertificateSANFeatures` class, which can be found in `classifier/feature_extractor.py` also extracts some features, which were taken from FANCI (https://github.com/fanci-dga-detection/fanci). The feature extraction code from the FANCI repository was modified to fit into the projects architecture and coding style. The FANCI license can be found here: (https://github.com/fanci-dga-detection/fanci/blob/master/LICENSE)
