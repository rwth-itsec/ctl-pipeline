from classifier.rf_classifier import RFClassifier, MetaClassifier
from classifier.simple_keyword_classifier import SimpleKeywordClassifier

class ClassifierConfigurations:

    special_classifiers = ['simple_keyword_classifier']

    """The features used by the classifier."""
    used_features = {
        'clf_rf_demo': ['sub_cn_entropy', 'sub_cn_is_com', 'name_san_entropy', 'has_uppercase_letters', 'num_dash', 'num_dash_rd', 'num_tokens', 'tld_in_token', 'https_in_domain', 'longest_token', 'special_char_ratio', 'is_ip', 'is_idn_domain', 'san_to_alexa_entropy', 'vowel_ratio', 'digit_ratio', 'length', 'contains_wwwdot', 'contains_subdomain_of_only_digits', 'subdomain_lengths_mean', 'parts', 'contains_digits', 'has_valid_tld', 'contains_one_char_subdomains', 'prefix_repetition', 'char_diversity', 'contains_tld_as_infix', 'alphabet_size', 'shannon_entropy', 'hex_part_ratio', 'underscore_ratio', 'ratio_of_repeated_chars', 'consecutive_consonant_ratio', 'consecutive_digits_ratio', '1_gram_std', '1_gram_median', '1_gram_mean', '1_gram_min', '1_gram_max', '1_gram_bottom_quartile', '1_gram_top_quartile', '2_gram_std', '2_gram_median', '2_gram_mean', '2_gram_min', '2_gram_max', '2_gram_bottom_quartile', '2_gram_top_quartile', '3_gram_std', '3_gram_median', '3_gram_mean', '3_gram_min', '3_gram_max', '3_gram_bottom_quartile', '3_gram_top_quartile', 'is_ov', 'is_ev', 'is_dv', 'sub_has_c', 'sub_has_st', 'sub_has_l', 'sub_only_cn', 'sub_has_cn', 'sub_dn_count', 'sub_char_count', 'sub_ext_count', 'valid_period', 'policies_count', 'is_wildcard', 'has_ocsp', 'has_cdp', 'san_count', 'average_sd_count', 'san_tld_count', 'key_algorithm', 'key_size', 'issuer'],
        'simple_keyword_classifier': None,
    }

    """The positive threshold for the classifier. If a classification score is above this threshold the pipeline will treat it as positive and store more metadata."""
    thresholds = {
        'clf_rf_demo': 0.939,
        'simple_keyword_classifier': 1.0,
    }

    """The meta classifier, which should be used. Pass None if no meta classifier is needed, e.g. when the `AvgClassifier` is used."""
    meta_classifier = {
        'clf_rf_demo': MetaClassifier.max,
        'simple_keyword_classifier': None,
    }

    """The classifier class to initialize with the pickled classifier.

    Do not initialize the class here, but just pass the name. The name needs to be imported for this to work.
    """
    classifier_class = {
        'clf_rf_demo': RFClassifier,
        'simple_keyword_classifier': SimpleKeywordClassifier,
    }

    def configuration_for_classifier(name):
        assert name in ClassifierConfigurations.thresholds and name in ClassifierConfigurations.classifier_class and name in ClassifierConfigurations.meta_classifier and name in ClassifierConfigurations.used_features, f'The configuration is incomplete for the classifier: {name}'

        return ClassifierConfigurations.classifier_class.get(name), ClassifierConfigurations.thresholds.get(name), ClassifierConfigurations.used_features.get(name), ClassifierConfigurations.meta_classifier.get(name)
