from classifier.feature_extractor import CertificateFeatures, FeatureExtractionError
import util.ct_leaf_decoder as ct_leaf_decoder
import classifier.x509wrapper as x509wrapper

import os
import json
from multiprocessing import Pool
import tqdm
import logging

class CertificateReader:
    def __init__(self, directory, number_of_processes = 4):
        self.directory = directory
        self.number_of_processes = number_of_processes

    def get_certificate_paths(self):
        if not self.directory:
            return []
        all_file_pathes = []
        for root, dirs, files in os.walk(self.directory):
            all_file_pathes.extend([root + '/' + file for file in files])
        return all_file_pathes

    def read_and_extract_features(self, extraction_function):
        all_file_pathes = self.get_certificate_paths()
        pool = Pool(processes = self.number_of_processes)
        features = []
        for f in tqdm.tqdm(pool.imap_unordered(extraction_function, all_file_pathes), total=len(all_file_pathes)):
            features.append(f)

        features = list(filter(lambda f: f is not None, features))
        return features


    def extract_from_certificate_pem_file(file_path):
        return CertificateReader.extract_data(file_path)

    def extract_from_certificate_pem_file_with_url(file_path):
        features, url = extract_data(file_path, return_url = True)
        # Return None if features are None, since then the filter in the read_and_extract_features will be able to filter it
        if features is None:
            return None
        return features, url

    def raw_certificate_pem_file_and_url(file_path):
        return CertificateReader.extract_raw_certificate(file_path)

    def raw_certificate_json_file_and_url(file_path):
        with open(file_path, 'r') as file:
            loaded_json = None
            try:
                loaded_json = json.loads(file.read())
            except Exception as e:
                print(f'Cannot load JSON file: {file_path}')
                return None, None

            return ct_leaf_decoder.parse_ctl_entry(loaded_json)['leaf_cert'], None
        return None, None

    def extract_from_ctl_json(file_path):
        with open(file_path, 'r') as file:
            loaded_json = None
            try:
                loaded_json = json.loads(file.read())
            except Exception as e:
                print(f'Cannot load JSON file: {file_path}')
                return None

            entry = ct_leaf_decoder.parse_ctl_entry(loaded_json)
            try:
                return CertificateFeatures.extract(entry['leaf_cert'])
            except FeatureExtractionError as exception:
                print(f'Feature extraction failed with error: {exception}. In certificate file: {file_path}')
        return None



    def extract_data(file_path, return_url = False):
        """Extracts selected features from certificate at file_path.

        :returns: CertificateFeatures object of extracted information or None if error
        """
        try:
            with open(file_path, 'r') as f:
                raw_data = f.read()
            cert = x509wrapper.get_cert_from_pem(raw_data[raw_data.find("\n")+1:])
            url = x509wrapper.get_url(raw_data)
        except Exception as err:
            logging.warning("Unable to open file or not a certificate: {}.".format(file_path))
            logging.warning(err)
            if return_url:
                return None, None
            return None

        try:
            if x509wrapper.is_valid(raw_data, cert, url):
                if return_url:
                    return CertificateFeatures.extract(cert), url
                return CertificateFeatures.extract(cert)
        except Exception as err:
            logging.error("Encountered error in certificate: {}.".format(file_path))
            logging.exception(err)
            if return_url:
                return None, None
            return None

        if return_url:
            return None, None
        return None

    def extract_raw_certificate(file_path):
        """ Load certificate from file_path.

        :returns: X509 certificate object or None if error
        """
        try:
            with open(file_path, 'r') as f:
                raw_data = f.read()
            cert = x509wrapper.get_cert_from_pem(raw_data[raw_data.find("\n")+1:])
            url = x509wrapper.get_url(raw_data)
        except Exception as err:
            logging.warning("Unable to open file or not a certificate: {}.".format(file_path))
            logging.warning(err)
            return None, None
        try:
            if x509wrapper.is_valid(raw_data, cert, url):
                return cert, url
        except Exception as err:
            logging.error("Encountered error in certificate: {}.".format(file_path))
            logging.exception(err)
            return None, None
        return None, None
