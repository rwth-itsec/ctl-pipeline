import argparse
import fnmatch
import re
from multiprocessing import Pool
from urllib.parse import urlparse
import logging
import numpy as np
import os
import tqdm
from itertools import repeat
import shutil

from helper import valid_existing_directory_path, valid_existing_path, valid_existing_file_path
from .certificate_reader import CertificateReader
from .data_processing import DataSet
import classifier.x509wrapper as x509wrapper
from .hostname_filter import HostnameFilter

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
logger = logging.getLogger('create_dataset')
logger.setLevel(logging.INFO)

#
# Create dataset
#
def create_data_set(arguments, dataset_name, include_benign, include_malicious):
    """Create a pickled dataset from the given benign and malicious certificates.

    The dataset may contain both benign and malicious certificates or only one type.
    To balance the dataset it is advised to create two datasets, one containing only
    benign certificates and one containing only malicious ones. Then the `combine_with_balance`
    method can be used to combine them into one dataset.

    :param arguments: The arguments given to the script.
    :param dataset_name: The desired name of the dataset.
    :param include_benign: Whether or not to include benign certificates in the resulting dataset.
    :param include_malicious: Whether or not to include malicious certificates in the resulting dataset.
    """
    number_of_processes = arguments.processes or 4

    crb = CertificateReader(arguments.benign, number_of_processes=number_of_processes)
    crm = CertificateReader(arguments.malicious, number_of_processes=number_of_processes)

    logging.info(f'Reading certificates to create dataset {dataset_name}...')
    certs_benign = []
    if include_benign:
        certs_benign = crb.read_and_extract_features(CertificateReader.extract_from_ctl_json)
    certs_malicious = []
    if include_malicious:
        certs_malicious = crm.read_and_extract_features(CertificateReader.extract_from_certificate_pem_file)

    logging.info(f'Benign certificates: {len(certs_benign)}')
    logging.info(f'Malicious certificates: {len(certs_malicious)}')

    group_map = {0: 'benign', 1: 'malicious'}
    groups = [0] * len(certs_benign)
    groups = groups + [1] * len(certs_malicious)
    certs = certs_benign + certs_malicious

    DataSet(certs, groups, group_map, dataset_name).serialize(arguments.target)


#
# Combine datasets
#
def combine(datasets):
    certificates = np.array([])
    labels = np.array([])
    for dataset in datasets:
        ds_certs, ds_labels = dataset.expand()
        certificates = np.concatenate((certificates, ds_certs))
        labels = np.concatenate((labels, ds_labels))
    return certificates, labels


def combine_with_balance(beingn_path, malicious_path, malicious_ratio, certificate_counts, dataset_name, output_dataset_base_path):
    """Combine datasets to a balanced dataset.

    :param benign_path: Path to a benign dataset.
    :param malicious_path: Path to a malicious dataset.
    :param malicious_ratio: Ratio of benign certificates. 0.5 is balanced, 1 only malicious, 0 no malicious at all (mutually exclusive with certificate_counts)
    :param certificate_counts: Counts for the certificates, tuple of the form (int, int) where the first entry is the benign count, the second one the malicious count. (mutually exclusive with malicious_ratio)
    :param dataset_name: The desired name of the dataset.
    :param output_dataset_base_path: The directory to which to write the resulting dataset.
    """
    certificates, labels = combine([DataSet.load(beingn_path), DataSet.load(malicious_path)])
    number_of_benign_certificates = np.count_nonzero(labels == 0)
    number_of_malicious_certificates = np.count_nonzero(labels == 1)
    logger.info(f'Loaded {number_of_benign_certificates} benign certificates and {number_of_malicious_certificates} malicious certificates.')

    benign_certificates = certificates[labels == 0]
    malicious_certificates = certificates[labels == 1]
    del certificates, labels

    if malicious_ratio is not None:
        # Formula to calculate required benign certificates from given malicious certificates and ratio
        b = lambda m, r: (1/r - 1) * m
        m = lambda b, r: 1/(1/r - 1) * b
        required_number_of_benign_certificates = int(b(number_of_malicious_certificates, malicious_ratio))
        if required_number_of_benign_certificates > number_of_benign_certificates:
            logger.error('There are not enough benign certificates to achieve the desired balancing. For simplicity the case of malicious certificate reduction is not implemented yet.')
            exit(1)

        benign_certificates = np.random.permutation(benign_certificates)
        benign_certificates = benign_certificates[0:required_number_of_benign_certificates]
    elif certificate_counts is not None:
        benign_count = certificate_counts[0]
        malicious_count = certificate_counts[1]
        if benign_count > number_of_benign_certificates and not benign_count == -1:
            logger.error(f'There are not enough benign certificates to satisfy required count (got {number_of_benign_certificates}, need {benign_count}).')
            exit(1)
        if malicious_count > number_of_malicious_certificates and not malicious_count == -1:
            logger.error(f'There are not enough malicious certificates to satisfy required count (got {number_of_malicious_certificates}, need {malicious_count}).')
            exit(1)

        if benign_count != -1:
            benign_certificates = np.random.permutation(benign_certificates)
            benign_certificates = benign_certificates[0:benign_count]
        if malicious_count != -1:
            malicious_certificates = np.random.permutation(malicious_certificates)
            malicious_certificates = malicious_certificates[0:malicious_count]


    group_map = {0: 'benign', 1: 'malicious'}
    groups = [0] * len(benign_certificates)
    groups = groups + [1] * len(malicious_certificates)
    certificates = np.concatenate((benign_certificates, malicious_certificates))

    logger.info(f'Serializing dataset with {len(benign_certificates)} benign certificates and {len(malicious_certificates)} malicious certificates.')

    DataSet(certificates, groups, group_map, dataset_name).serialize(output_dataset_base_path)


#
# Make certificates unique
#
def copy_certificate(function_args):
    extraction_function, target_directory, path = function_args
    certificate, _ = extraction_function(path)
    if certificate is None:
        return []
    issuer = x509wrapper.issuer_get_from_oid(certificate, x509wrapper.oid_cn)
    serial_number = x509wrapper.get_serial_number(certificate)
    sans = x509wrapper.get_san(certificate)
    sans.append(x509wrapper.subject_get_from_oid(certificate, x509wrapper.oid_cn))
    file_name = issuer+'_'+serial_number
    file_name = file_name.replace('/', '_')
    shutil.copy2(path, os.path.join(target_directory, file_name))

def make_certificates_unique(arguments):
    """Make the given certificates unique.

    The idea here is, that a given certificate directory containing certificates is made unique (issuer + serial number) by
    copying the certificates to another directory and renaming them to a combination of the unique id (issuer + serial).

    :param arguments: The arguments passed to the script.
    """
    number_of_processes = arguments.processes or 4

    logger.info('Copying certificates to output directory making them unique...')

    def perform(certificate_directory, target, extraction_function):
        reader = CertificateReader(certificate_directory)
        pathes = reader.get_certificate_paths()
        pool = Pool(processes = 4)
        for _ in tqdm.tqdm(pool.imap_unordered(copy_certificate, zip(repeat(extraction_function), repeat(target), pathes)), total=len(pathes)):
            pass

    if arguments.benign is not None:
        perform(arguments.benign, arguments.target, CertificateReader.raw_certificate_json_file_and_url)
    if arguments.malicious is not None:
        perform(arguments.malicious, arguments.target, CertificateReader.raw_certificate_pem_file_and_url)

#
# Filter certificates
#
def filter_certificate(function_args):
    extraction_function, target_directory, hostname_filter, path = function_args
    certificate, _ = extraction_function(path)
    sans = x509wrapper.get_san(certificate)
    sans.append(x509wrapper.subject_get_from_oid(certificate, x509wrapper.oid_cn))
    if all(hostname_filter.matches(san) == False for san in sans):
        file_name = os.path.basename(path)
        shutil.copy2(path, os.path.join(target_directory, file_name))

def filter_certificates_san_based(arguments):
    """Filter the given certificates against a list of hostnames.

    This method filters the certificates in the input directory against the given list of filter hostnames.
    If one of the sans is in the filtering list the certificate is not copied over to the target directory.

    :param arguments: The arguments passed to the script.
    """
    number_of_processes = arguments.processes or 4

    logger.info('Filtering certificates...')

    def perform(certificate_directory, target, extraction_function):
        reader = CertificateReader(certificate_directory)
        pathes = reader.get_certificate_paths()
        hostname_filter = HostnameFilter(arguments.filter_hostnames)
        pool = Pool(processes = 4)
        for _ in tqdm.tqdm(pool.imap_unordered(filter_certificate, zip(repeat(extraction_function), repeat(target), repeat(hostname_filter), pathes)), total=len(pathes)):
            pass

    if arguments.benign is not None:
        perform(arguments.benign, arguments.target, CertificateReader.raw_certificate_json_file_and_url)
    if arguments.malicious is not None:
        perform(arguments.malicious, arguments.target, CertificateReader.raw_certificate_pem_file_and_url)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create datasets from certificates.')
    subparsers = parser.add_subparsers(dest='command')
    subparsers.required = True
    parser.add_argument('--target', '-t', type=valid_existing_directory_path, required=True, help='Path where to store the created dataset.')
    parser.add_argument('--benign', '-b', type=valid_existing_path, help='Path to benign certificates.')
    parser.add_argument('--malicious', '-m', type=valid_existing_path, help='Path to malicious certificates.')
    parser.add_argument('--name', '-n', type=str, help='Name for the resulting dataset.')

    combine_parser = subparsers.add_parser('combine')
    group = combine_parser.add_mutually_exclusive_group()
    group.add_argument('--malicious-ratio', '-r', type=float, help='The ratio of malicious certificates in the final set.')
    group.add_argument('--counts', '-c', type=int, nargs=2, metavar=('benign', 'malicious'), help='Benign and malicious certificate counts. -1 for all.')

    create_parser = subparsers.add_parser('create')
    create_parser.add_argument('--processes', '-p', type=int, help='Max number of parallelization processes to use.')

    create_parser = subparsers.add_parser('unique')
    create_parser.add_argument('--processes', '-p', type=int, help='Max number of parallelization processes to use.')

    filter_parser = subparsers.add_parser('filter')
    filter_parser.add_argument('--filter-hostnames', '-f', type=valid_existing_file_path, help='A path to a file containing a list of hostnames to filter.')
    filter_parser.add_argument('--processes', '-p', type=int, help='Max number of parallelization processes to use.')

    arguments = parser.parse_args()

    if arguments.command == 'combine':
        assert arguments.name is not None and arguments.name != '', 'You need to provide a name for the dataset'
        logger.info(f'combine {arguments.__dict__}')
        combine_with_balance(arguments.benign, arguments.malicious, arguments.malicious_ratio, arguments.counts, arguments.name, arguments.target)
    elif arguments.command == 'create':
        assert arguments.name is not None and arguments.name != '', 'You need to provide a name for the dataset'
        logger.info(f'create {arguments.__dict__}')
        create_data_set(arguments, arguments.name, include_benign = arguments.benign is not None, include_malicious = arguments.malicious is not None)
    elif arguments.command == 'unique':
        logger.info(f'unique {arguments.__dict__}')
        make_certificates_unique(arguments)
    elif arguments.command == 'filter':
        logger.info(f'filter {arguments.__dict__}')
        filter_certificates_san_based(arguments)
