import os
import time
import numpy
import pickle
import random
import string


class DataSet:
    def __init__(self, domains, groups, group_map, id=None):
        if id:
            self.id = id
        else:
            self.id = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))

        if isinstance(domains, numpy.ndarray):
            self.domains = domains.tolist()
        else:
            self.domains = list(domains)

        if isinstance(groups, numpy.ndarray):
            self.groups = groups.tolist()
        else:
            self.groups = list(groups)

        self.group_map = group_map

    def expand(self):
        """
        Return domains and the group labels as numpy arrays.
        :return: 2 numpy arrays
        """
        return numpy.array(self.domains), numpy.array(self.groups)

    def serialize(self, where=None, keep_copy=True):
        """
        Save data set to disk.
        :param where: path to location to store set file
        :param keep_copy: if file exists saves this one as a copy. If False nothing is stored to disk.
        :return: void
        """

        if not where:
            set_file = 'sets/' + '{!s}.pkl'.format(self.id)
        else:
            set_file = where + '/' + '{!s}.pkl'.format(self.id)

        if os.path.isfile(set_file):
            if not keep_copy:
                return
            set_file += '_' + ''.join(str(time.time()).replace('.', '_'))

        with open(set_file, 'wb') as f:
            pickle.dump(self, f, protocol=4)

    @staticmethod
    def load(file: str):
        """
        Load data set from disk.
        :param file: to load
        :return: deserialized DataSet object.
        """
        with open(file, 'rb') as f:
            dataset = pickle.load(f)

        return dataset