import fnmatch

class HostnameFilter:
    def __init__(self, rule_file_path):
        super(HostnameFilter, self).__init__()
        self.rule_file_path = rule_file_path
        self.rules = []
        with open(rule_file_path, 'r') as file:
            for line in file:
                rule = line.strip()
                self.rules.append(rule)
                # Also add version without wildcard
                if rule.startswith('*.'):
                    while rule.startswith('*.'):
                        rule = rule[2:]
                    self.rules.append(rule)


    # Method to check hostname against rule
    def matches_with_rule(self, hostname):
        """Check whether or not the given hostname matches a rule in the rule set.

        :param hostname: The hostname to check.
        :returns: Tuple: (match: bool, rule: str)
        """
        for rule in self.rules:
            if fnmatch.fnmatch(hostname, rule):
                return True, rule
        return False, None

    def matches(self, hostname):
        """Check whether or not the given hostname matches a rule in the rule set.

        :param hostname: The hostname to check.
        :returns: A bool, True if a match is found in the loaded rule set.
        """
        return self.matches_with_rule(hostname)[0]

