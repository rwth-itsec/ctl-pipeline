import requests
import datetime
import argparse

import util.ct_leaf_decoder as ct_leaf_decoder

from certificate import CertificateTransparencyLogSource
import helper

class CTLSearch:
    def __init__(self, log, silent = False):
        super(CTLSearch, self).__init__()
        self.silent = silent
        self.api_url = log.value
        if not self.api_url.endswith('/'):
            self.api_url += '/'

    def get_tree_size(self):
        get_sth = requests.get(self.api_url + 'get-sth')
        decoded_get_sth = get_sth.json()
        return decoded_get_sth["tree_size"]

    def get_index_time(self, index):
        get_entries = requests.get(self.api_url + 'get-entries?start={}&end={}'.format(index, index))
        decoded_get_entries = get_entries.json()["entries"]
        return datetime.datetime.fromtimestamp(ct_leaf_decoder.parse_ctl_entry(decoded_get_entries[0])['timestamp'])

    def closest_index_to_datetime(self, date_time, silent = False):
        first_entry_date = self.get_index_time(0)
        if first_entry_date >= date_time:
            if not self.silent:
                print("The first index in the CTL is after the searched date and time.")
            return None

        top = self.get_tree_size()
        last_entry_date = self.get_index_time(top-1)
        if last_entry_date < date_time:
            if not self.silent:
                print("The last index in the CTL is before the searched date and time.")
            return None
        bottom = 0

        while top > bottom:
            center = int(bottom + ((top - bottom) / 2))
            if center == top or center == bottom:
                top_delta = abs(self.get_index_time(top) - date_time)
                bottom_delta = abs(self.get_index_time(bottom) - date_time)
                if top_delta < bottom_delta:
                    return top
                else:
                    return bottom
            else:
                center_time = self.get_index_time(center)
                if not self.silent:
                    print(f' - Visited index {center}, which is at time: {center_time}')
                if center_time < date_time:
                    bottom = center
                elif center_time > date_time:
                    top = center
                else:
                    return center

    def map_log(self, start, step_size):
        for index in range(start, self.get_tree_size(), step_size):
            index_time = self.get_index_time(index)
            print(f'Index {index} is at time: {index_time}')



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Search index for given date in CTL.')
    parser.add_argument('--log', '-l', type=str, required=True, help="The CT log to search in.")
    parser.add_argument('--date', '-d', type=helper.valid_date_string, required=True, help="The date for which to search in the given CTL. (format: dd.mm.YYYY)")
    parser.add_argument('--map', '-m', nargs = 2, type=int, metavar=('start', 'step_size'), help = "Additionally create a 'map' of the log indices and dates using the step size and start given with this argument")
    arguments = parser.parse_args()
    log = CertificateTransparencyLogSource[arguments.log.upper()]

    print(f'Searching for index near date: {arguments.date} in {log.name} log...')

    ctl = CTLSearch(log)
    index = ctl.closest_index_to_datetime(arguments.date)
    if index is not None:
        time = ctl.get_index_time(index)
        print(f'Got index {index} which is close to searched time with time {time}.')

    if arguments.map is not None:
        ctl.map_log(arguments.map[0], arguments.map[1])
