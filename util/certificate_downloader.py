import argparse
import time
import sys
import os
import json
import logging
from multiprocessing import Process

from .ct_query import CtLogQuery
from classifier.feature_extractor import CertificateFeatures, FeatureExtractionError
from .data_processing import DataSet

from verification.verification_pipeline import VerificationPipeline
import helper
from certificate import CertificateTransparencyLogSource, Certificate
from .ctl_search import CTLSearch
from .hostname_filter import HostnameFilter

class CertificateDownloader:
    CERTIFICATES_PER_BATCH = 100
    MALICIOUS_FILTER_SCALE_FACTOR = 1.1

    def __init__(self, arguments):
        self.logger = logging.getLogger('certificate_download')
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s', datefmt='%Y.%m.%d %H:%M:%S')

        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(logging.DEBUG)
        stdout_handler.setFormatter(formatter)
        self.logger.addHandler(stdout_handler)

        self._configuration = arguments
        self._load_filter_dataset(only_labels = [1])
        self._load_filter_hostnaems()

    @helper.keyboard_interrupt_ignoring
    def downloader(self, log_source, start_leaf_id, begin, advance, batch_size, end_index):
        certificate_log_query = CtLogQuery(log_source.value)
        pipeline = VerificationPipeline(self._configuration.url_database, self._configuration.hash_prefix_database)
        i = begin
        while True:
            batch_start = start_leaf_id + (i * batch_size)
            if batch_start > (end_index - CertificateDownloader.CERTIFICATES_PER_BATCH):
                return
            new_certificates = certificate_log_query.get_certs_in_range_with_raw_certificate(batch_start, batch_start + CertificateDownloader.CERTIFICATES_PER_BATCH - 1)
            for (offset, (certificate, raw_certificate)) in enumerate(new_certificates):
                try:
                    entry_date = certificate['leaf_cert'].not_valid_before
                    if entry_date < self._configuration.start_date or entry_date > self._configuration.end_date:
                        continue
                    certificate = Certificate(log_source, batch_start + offset, certificate['leaf_cert'], CertificateFeatures.extract(certificate['leaf_cert']))
                    if not self._should_include_certificate(certificate):
                        self.logger.info(f'Filtering certificate with sans: {certificate.hostnames_string}')
                        continue
                    is_benign = True
                    for hostname in certificate.hostnames or []:
                        result = pipeline.is_hostname_malicious(hostname)
                        if result == True:
                            is_benign = False
                            self.logger.info(f"Found hostname in any database: {hostname}")
                            break
                    if is_benign == True:
                        with open(self._configuration.output_directory + '/' + certificate.unique_id.replace('/', '_'), 'w+') as file:
                            json.dump(raw_certificate, file)
                except FeatureExtractionError as exception:
                    self.logger.error(exception)

            i += advance

    def start(self):
        ct_logs = self._configuration.ct_log_set

        total_number_of_certificates_in_range = 0
        log_meta = []

        for log in ct_logs:
            searcher = CTLSearch(log, silent = True)
            start_index = searcher.closest_index_to_datetime(self._configuration.start_date)
            end_index = searcher.closest_index_to_datetime(self._configuration.end_date)
            self.logger.info(f'Found indices for log {log.name}: {start_index} - {end_index}')
            if start_index is not None and end_index is not None:
                assert start_index < end_index, 'The log start and end indices are in the wrong order.'
                log_meta.append((log, start_index, end_index))
                total_number_of_certificates_in_range += end_index - start_index
            else:
                self.logger.info(f'Excluding {log.name} log, since it has no entries in the searched range.')

        if not os.path.isdir(self._configuration.output_directory):
            os.makedirs(self._configuration.output_directory)

        for log, start, end in log_meta:
            ctl_downloaders = []
            self.logger.info(f'Start downloading certificates from: {log.name}')

            log_factor = (end - start) / total_number_of_certificates_in_range
            number_of_certificates_to_download = int(self._configuration.desired_number_of_certificates * log_factor * CertificateDownloader.MALICIOUS_FILTER_SCALE_FACTOR)
            if number_of_certificates_to_download < CertificateDownloader.CERTIFICATES_PER_BATCH:
                self.logger.info(f'Skipping {log.name}, since the number of certificates to download is less than {CertificateDownloader.CERTIFICATES_PER_BATCH} (CERTIFICATES_PER_BATCH).')
                continue

            number_of_batches = int(number_of_certificates_to_download / CertificateDownloader.CERTIFICATES_PER_BATCH)
            batch_size = int((end - start) / number_of_batches)

            self.logger.info(f'{log.name}: {start} - {end} (#certificates from this log = {number_of_certificates_to_download}, batch_size = {batch_size}, number_of_batches = {number_of_batches})')

            assert batch_size > CertificateDownloader.CERTIFICATES_PER_BATCH, 'The number of certificates per batch is larger than the batch itself.'

            for begin in range(self._configuration.number_of_downloaders):
                ctl_downloaders.append(Process(target=self.downloader, args=(log, start, begin, self._configuration.number_of_downloaders, batch_size, end)))

            for p in ctl_downloaders:
                p.start()

            try:
                for p in ctl_downloaders:
                    p.join()
            except (KeyboardInterrupt, SystemExit):
                exit()

    def _load_filter_dataset(self, only_labels = [0, 1]):
        """Load a pickled dataset to filter out the downloaded certificates, which are matching a feature vector.

        :param only_labels: A list of labels from the dataset, which should be used in the filter, in case not all labels should be used to filter.
        """
        self.filter_features = {}
        if self._configuration.filter_dataset is not None:
            dataset = DataSet.load(self._configuration.filter_dataset)
            for certificate, label in zip(*dataset.expand()):
                if label not in only_labels:
                    continue
                for san in certificate.sans:
                    if san in self.filter_features:
                        self.filter_features[san] += [certificate.feature_vectors()]
                    else:
                        self.filter_features[san] = [certificate.feature_vectors()]
            self.logger.info(f'Loaded {len(self.filter_features)} sans to filter.')

    def _load_filter_hostnaems(self):
        if self._configuration.filter_hostnames is not None:
            self._malicious_hostnames = HostnameFilter(self._configuration.filter_hostnames)
        else:
            self._malicious_hostnames = None

    def _should_include_certificate(self, certificate):
        if len(certificate.hostnames) == 0:
            return True
        if self._malicious_hostnames is not None and any([self._malicious_hostnames.matches(san) for san in certificate.hostnames]):
            return False
        if len(self.filter_features) == 0:
            return True
        filter_certificate_feature_vectors = self.filter_features.get(certificate.hostnames[0])
        if filter_certificate_feature_vectors == None:
            return True
        assert len(certificate.features.feature_vectors()[0]) == len(filter_certificate_feature_vectors[0][0]), 'The length of the filter and certificate feature vectors are not matching.'
        return certificate.features.feature_vectors() not in filter_certificate_feature_vectors

def valid_certificate_log_source(name):
    try:
        return CertificateTransparencyLogSource[name.upper()]
    except KeyError:
        return name

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Download certificates and save non malicious ones from minimal set of CTLs.')
    parser.add_argument('--url-database', '-u', type=str, required=True, help="Path to the url database.")
    parser.add_argument('--hash-prefix-database', '-p', type=str, required=True, help="Path to a GSB hash prefix database.")
    parser.add_argument('--desired-number-of-certificates', '-c', type=int, required=True, help='The number if certificates, which should be downloaded in total.')
    parser.add_argument('--output-directory', '-o', type=str, required=True, help="The directory in which to store the certificates")
    parser.add_argument('--number-of-downloaders', '-t', type=int, default=2, help="Number of download processes to run in parallel (optional, default = 2).")

    parser.add_argument('--start-date', '-s', type=helper.valid_date_string, required=True, help="The start date of the timespan from which to fetch certificates from the logs.")
    parser.add_argument('--end-date', '-e', type=helper.valid_date_string, required=True, help="The end date of the timespan from which to fetch certificates from the logs.")
    parser.add_argument('--ct-log-set', '-l', type=valid_certificate_log_source, nargs='+', required=True, choices=list(CertificateTransparencyLogSource), help='The list of logs from which to download certificates in the given time range.')

    parser.add_argument('--filter-dataset', '-fd', type=helper.valid_existing_file_path, required=False, help="A DataSet of feature vectors against which the extracted features of the downloaded certificates should be filtered.")
    parser.add_argument('--filter-hostnames', '-fh', type=helper.valid_existing_file_path, required=False, help="A list of malicious hostnames, against which the certificates should be filtered.")

    arguments = parser.parse_args()

    print("Running certificate download with arguments:", arguments.__dict__)

    CertificateDownloader(arguments).start()
