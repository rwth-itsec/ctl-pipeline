import os
import sys
import logging
from sklearn.model_selection import train_test_split
import argparse

from util.data_processing import DataSet
from classifier.rf_classifier import RFClassifier, MetaClassifier
from classifier.dl_classifier import DLClassifier
from classifier.multivalued_feature import MultivaluedFeature, NGramFeature, MaliciousTokenFeature
from helper import valid_existing_directory_path, valid_existing_file_path
from classifier.models import lstm, resnet


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
logger = logging.getLogger('train_rf')
logger.setLevel(logging.INFO)

def train_and_evaluate(dataset_path, output_base_directory, feature_set_name, classifier_type):
    logger.info(f'Start dataset: {dataset_path}')

    ds = DataSet.load(dataset_path)

    path = os.getcwd()
    if not os.path.isdir(output_base_directory):
        os.makedirs(output_base_directory)
        os.makedirs(os.path.join(output_base_directory, 'results'))
    os.chdir(output_base_directory)

    certs, labels = ds.expand()

    logger.info(f'Train test split')
    c_train, c_test, l_train, l_test = train_test_split(certs, labels, test_size=0.2, stratify=labels)

    all_features = ['sub_cn_entropy', 'sub_cn_is_com', 'name_san_entropy', 'has_uppercase_letters', 'num_dash', 'num_dash_rd', 'num_tokens', 'tld_in_token', 'https_in_domain', 'longest_token', 'special_char_ratio', 'is_ip', 'is_idn_domain', 'san_to_alexa_entropy', 'vowel_ratio', 'digit_ratio', 'length', 'contains_wwwdot', 'contains_subdomain_of_only_digits', 'subdomain_lengths_mean', 'parts', 'contains_digits', 'has_valid_tld', 'contains_one_char_subdomains', 'prefix_repetition', 'char_diversity', 'contains_tld_as_infix', 'alphabet_size', 'shannon_entropy', 'hex_part_ratio', 'underscore_ratio', 'ratio_of_repeated_chars', 'consecutive_consonant_ratio', 'consecutive_digits_ratio', '1_gram_std', '1_gram_median', '1_gram_mean', '1_gram_min', '1_gram_max', '1_gram_bottom_quartile', '1_gram_top_quartile', '2_gram_std', '2_gram_median', '2_gram_mean', '2_gram_min', '2_gram_max', '2_gram_bottom_quartile', '2_gram_top_quartile', '3_gram_std', '3_gram_median', '3_gram_mean', '3_gram_min', '3_gram_max', '3_gram_bottom_quartile', '3_gram_top_quartile', 'keyword_secure', 'keyword_login', 'keyword_mail', 'keyword_account', 'keyword_online', 'keyword_support', 'keyword_sites', 'keyword_services', 'keyword_service', 'keyword_docs', 'keyword_update', 'keyword_signin', 'keyword_info', 'keyword_security', 'keyword_help', 'keyword_verify', 'keyword_recovery', 'keyword_mobile', 'keyword_secureserver', 'keyword_storage', 'keyword_center', 'keyword_verification', 'keyword_auth', 'keyword_promo', 'keyword_free', 'keyword_paypal', 'keyword_runescape', 'keyword_google', 'keyword_apple', 'keyword_jppost', 'keyword_sharepoint', 'keyword_sagawa', 'keyword_appleid', 'keyword_amazon', 'keyword_icloud', 'keyword_windows', 'keyword_office', 'keyword_facebook', 'keyword_1drv', 'keyword_live', 'keyword_onedrive', 'keyword_ebay', 'keyword_allegro', 'keyword_itau', 'keyword_bankofamerica', 'keyword_cartetitolari', 'keyword_viabcp', 'contains_malicious_token', 'malicious_token_count', 'mal_token_top_1', 'mal_token_top_2', 'mal_token_top_3', 'mal_token_top_4', 'mal_token_top_5', 'mal_token_top_6', 'mal_token_top_7', 'mal_token_top_8', 'mal_token_top_9', 'mal_token_top_10', 'is_ov', 'is_ev', 'is_dv', 'sub_has_c', 'sub_has_st', 'sub_has_l', 'sub_only_cn', 'sub_has_cn', 'sub_dn_count', 'sub_char_count', 'sub_ext_count', 'valid_period', 'policies_count', 'is_wildcard', 'has_ocsp', 'has_cdp', 'san_count', 'average_sd_count', 'san_tld_count', 'key_algorithm', 'key_size', 'issuer']
    after_fs_features = ['sub_cn_entropy', 'sub_cn_is_com', 'name_san_entropy', 'num_dash', 'num_dash_rd', 'num_tokens', 'tld_in_token', 'longest_token', 'special_char_ratio', 'is_idn_domain', 'san_to_alexa_entropy', 'vowel_ratio', 'digit_ratio', 'length', 'contains_wwwdot', 'subdomain_lengths_mean', 'parts', 'contains_digits', 'contains_one_char_subdomains', 'char_diversity', 'contains_tld_as_infix', 'alphabet_size', 'shannon_entropy', 'hex_part_ratio', 'ratio_of_repeated_chars', 'consecutive_consonant_ratio', 'consecutive_digits_ratio', '1_gram_std', '1_gram_median', '1_gram_mean', '1_gram_max', '1_gram_bottom_quartile', '1_gram_top_quartile', '2_gram_std', '2_gram_mean', '2_gram_max', '2_gram_top_quartile', '3_gram_std', '3_gram_mean', '3_gram_max', '3_gram_top_quartile', 'is_ev', 'sub_only_cn', 'sub_char_count', 'valid_period', 'has_cdp', 'san_count', 'average_sd_count', 'san_tld_count', 'issuer']

    features = None
    if feature_set_name == 'all':
        features = all_features
    elif feature_set_name == 'selection':
        features = after_fs_features
    assert features is not None, 'Invalid feature set name.'

    logger.info(f'Start training')

    cf = None
    if classifier_type == 'random_forest':
        cf = RFClassifier()
    elif classifier_type == 'resnet':
        cf = DLClassifier(resnet.build_model(flen=len(features) - 1))
    elif classifier_type == 'lstm':
        cf = DLClassifier(lstm.build_model(flen=len(features) - 1))
    assert cf is not None, f'Invalid classifier type {classifier_type}'

    cf.used_features = features
    cf.meta_classifier = MetaClassifier.max

    cf.fit(c_train, l_train)

    if classifier_type == 'random_forest':
        cf.serialize('rf_classifier.pkl')
    elif classifier_type in ['resnet', 'lstm']:
        cf.serialize(f'{classifier_type}_classifier')

    logger.info('Starting evaluation...')
    cf.predict(c_test, l_test, log_confusion_matrix=True, plot_roc=True)

    os.chdir(path)


if __name__ == '__main__':
    logger.info('Staring...')

    parser = argparse.ArgumentParser(description='Train classifiers and create test evaluation.')
    parser.add_argument('--dataset', '-d', type=valid_existing_file_path, required=True, help='Path to the training dataset.')
    parser.add_argument('--output', '-o', type=str, required=True, help='Output directory path.')
    parser.add_argument('--classifier', '-c', type=str, required=True, choices=['random_forest', 'resnet', 'lstm'], help='Which classifier to train, a random forest or a deep learning classifier.')
    parser.add_argument('--features', '-f', type=str, required=True, choices=['all', 'selection'], help='Which feature set to use for classifier training.')
    arguments = parser.parse_args()
    print(arguments.__dict__)
    train_and_evaluate(arguments.dataset, arguments.output, arguments.features, arguments.classifier)
