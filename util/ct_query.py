import requests
import json

import util.ct_leaf_decoder as ct_leaf_decoder

class CtLogQuery:

    def __init__(self, api_url = None, log_start_info = True):
        if not api_url:
            api_url = 'https://ct.googleapis.com/rocketeer/ct/v1/'
        if not api_url[-1] == '/':
            api_url += '/'
        self.query_url = api_url
        self.next_leaf_id = self.get_tree_size()
        if log_start_info:
            print("Query url: {}\nLeaf ID: {}".format(self.query_url, self.next_leaf_id))

    def get_tree_size(self):
        get_sth = requests.get(self.query_url + 'get-sth')
        if not get_sth.ok:
            print(get_sth.content)
            return None
        try:
            decoded_get_sth = json.loads(get_sth.content)
        except Exception as e:
            print(e)
            print(get_sth.content)
            return None
        return decoded_get_sth["tree_size"]

    def get_entries(self, start, end):
        def get_page(start, end):
            get_entries = requests.get(self.query_url + 'get-entries?start={}&end={}'.format(start,end))
            if not get_entries.ok:
                print(get_entries.content)
                return []
            try:
                decoded_get_entries = json.loads(get_entries.content)
            except Exception as e:
                print(e)
                print(get_entries.content)
                return []
            return decoded_get_entries["entries"]

        entries = []
        while start <= end:
            new_entries = get_page(start, end)
            entries.extend(new_entries)
            start += len(new_entries)

        return entries

    def get_new_entries(self):
        ret = []
        new_size = self.get_tree_size()

        # If the new_size is None set it to the current size, to not crash unexpectedly
        # The new_size is none at this point, most likely due the request failing or responding unexpectedly,
        # so a retry at the next iteration can then simply include the missed certificates of this run.
        if new_size is None:
            new_size = self.next_leaf_id

        start_index = self.next_leaf_id
        if new_size > start_index:
            ret = self.get_entries(start_index, new_size - 1)
            # Set next_leaf_id to previous value + len(ret)
            # because it is possible that we asked for too many entries
            # and the endpoint returned the maximum value
            self.next_leaf_id = self.next_leaf_id + len(ret)
        return ret, start_index

    def get_new_certs(self):
        ret = []
        new_ct_entries, start_index = self.get_new_entries()
        for entry in new_ct_entries:
            cert = ct_leaf_decoder.parse_ctl_entry(entry)
            ret.append(cert)
        return ret, start_index

    def get_certs_in_range(self, start, end):
        ret = []
        new_ct_entries = self.get_entries(start, end)
        for entry in new_ct_entries:
            cert = ct_leaf_decoder.parse_ctl_entry(entry)
            ret.append(cert)
        return ret

    def get_certs_in_range_with_raw_certificate(self, start, end):
        """Get raw and parsed certificates in a given index range.

        :param start: The start index.
        :param end: The end index.
        :returns: The parsed and raw representation of the certificate in a tuple (parsed, raw).
        """
        ret = []
        new_ct_entries = self.get_entries(start, end)
        for entry in new_ct_entries:
            cert = ct_leaf_decoder.parse_ctl_entry(entry)
            ret.append((cert, entry))
        return ret

