# MIT License
#
# Copyright (c) 2017 Cali Dog Security
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import base64
import time

from collections import OrderedDict

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec,rsa,dsa


from construct import Struct, Byte, Int16ub, Int64ub, Enum, Bytes, \
    Int24ub, this, GreedyBytes, GreedyRange, Terminated, Embedded

# Source: https://github.com/CaliDog/certstream-server-python/blob/master/certstream/certlib.py

MerkleTreeHeader = Struct(
    "Version"         / Byte,
    "MerkleLeafType"  / Byte,
    "Timestamp"       / Int64ub,
    "LogEntryType"    / Enum(Int16ub, X509LogEntryType=0, PrecertLogEntryType=1),
    "Entry"           / GreedyBytes
)

Certificate = Struct(
    "Length" / Int24ub,
    "CertData" / Bytes(this.Length)
)

CertificateChain = Struct(
    "ChainLength" / Int24ub,
    "Chain" / GreedyRange(Certificate),
)

PreCertEntry = Struct(
    "LeafCert" / Certificate,
    Embedded(CertificateChain),
    Terminated
)


def parse_ctl_entry(entry):
    mtl = MerkleTreeHeader.parse(base64.b64decode(entry['leaf_input']))

    cert_data = {}
    if mtl.LogEntryType == "X509LogEntryType":
        cert_data['update_type'] = "X509LogEntry"
        chain = [x509.load_der_x509_certificate(Certificate.parse(mtl.Entry).CertData, default_backend())]
        extra_data = CertificateChain.parse(base64.b64decode(entry['extra_data']))
        for cert in extra_data.Chain:
            chain.append(x509.load_der_x509_certificate(cert.CertData, default_backend()))
    else:
        cert_data['update_type'] = "PreCertEntry"
        extra_data = PreCertEntry.parse(base64.b64decode(entry['extra_data']))
        chain = [x509.load_der_x509_certificate(extra_data.LeafCert.CertData, default_backend())]
        for cert in extra_data.Chain:
            chain.append(x509.load_der_x509_certificate(cert.CertData, default_backend()))

    cert_data.update({
        "leaf_cert" : chain[0],
        "chain" : chain[1:],
        "seen": time.time(),
        "timestamp": mtl.Timestamp / 1000.0
    })

    return cert_data
