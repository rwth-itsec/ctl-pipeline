from osint_downloader.database.hash_prefix_database import HashPrefixDatabase
from .hash_prefix_database import HashPrefixDatabase as HashPrefixDatabaseInterface

class OSINTHashDBAdapter(HashPrefixDatabaseInterface):

    def __init__(self, database_path):
        super(OSINTHashDBAdapter, self).__init__(database_path)
        self._database_path = database_path
        self._hash_prefix_database = HashPrefixDatabase(self._database_path)

    def check_hostname_active(self, hostname):
        """Check whether the given hostname matches any of the hash prefixes in the database, which are currently active.

        Only returns true, when the hostname check finds a hit, which is currently still active. Hash prefix matches from
        the past are ignored.

        :param: A hostname like 'example.com'
        :returns: A Bool indicating whether the hostname has an active hash prefix match.
        """
        return self._hash_prefix_database.check_hostname_has_active_prefix(hostname)

    def check_hostname(self, hostname):
        """Check whether the given hostname matches any of the hash prefixes in the database.

        :param: A hostname like 'example.com'
        :returns: A Bool indicating whether the hostname is contained in the database and such may be malicious.
        """
        return self._hash_prefix_database.check_hostname(hostname)
