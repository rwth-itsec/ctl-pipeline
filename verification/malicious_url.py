from enum import Enum
from datetime import datetime, timezone
from urllib.parse import urlparse

class MaliciousURLSource(Enum):
    """An enumeration indicating a source of a URL."""
    # You may add other sources here...
    PHISHTANK = "phishtank"
    OPEN_PHISH = "open_phish"
    PHISHSTAT_INFO = "phishstat_info"
    VIRUSTOTAL = "virustotal"
    GOOGLE_SAFE_BROWSING_LOOKUP = "google_safe_browsing_lookup"
    OTHER = 'other'

class MaliciousURL:
    """A class representing a malicious URL"""

    def __init__(self, **kwargs):
        super(MaliciousURL, self).__init__()

        self.url = kwargs.get('url')
        self.date = kwargs.get('date', datetime.now(timezone.utc))
        self.source = kwargs.get('source', MaliciousURLSource.OTHER)

        self.hostname = urlparse(self.url).hostname
