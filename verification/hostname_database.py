from .malicious_url import MaliciousURL

class HostnameDatabase:
    """A database which contains malicious hostnames/URLs used for the verification step in the pipeline."""

    def __init__(self, database_path):
        super(HostnameDatabase, self).__init__()
        self._database_path = database_path

    def query_hostname(self, hostname):
        """Query the database for the given hostname.

        :param: A hostname like 'example.com'
        :returns: A list of `MaliciousURL`s matching the searched hostname.
        """

        # ATTENTION: The hostnames are X509 SANs & CNs, so they may include wildcards.

        # Search for hostname in database, respecing wildcards, return True if the hostname is in the database and such malicious.

        raise NotImplementedError('TO IMPLEMENT: HostnameDatabase.check_hostname function.')
        return []


    def save_malicious_url(self, malicious_url):
        """Insert a new `MaliciousURL` entry into the database (if it does not exist already).

        In the pipeline online sources are checked, which may yield new unknown malicious urls. They
        can be written to the database here in order to find them faster the next time.

        :param malicious_url: A malicious url which may be new and could be saved/updated in the database.
        """
        raise NotImplementedError('TO IMPLEMENT: HostnameDatabase.save_malicious_url')
