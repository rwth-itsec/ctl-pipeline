from osint_downloader.database.url_database import URLDatabase
from osint_downloader.common.hostname_helper import HostnameHelper
from osint_downloader.common.malicious_url import MaliciousURL as OSINTMaliciousURL
from osint_downloader.common.malicious_url import MaliciousURLSource as OSINTMaliciousURLSource
from .malicious_url import MaliciousURL, MaliciousURLSource
from .hostname_database import HostnameDatabase

class OSINTURLDBAdapter(HostnameDatabase):
    def __init__(self, database_path):
        super(OSINTURLDBAdapter, self).__init__(database_path)
        self._database_path = database_path
        self._url_database = URLDatabase(self._database_path)

    def query_hostname(self, hostname):
        """Query the database for the given hostname.

        :param: A hostname like 'example.com'
        :returns: A list of `MaliciousURL`s matching the searched hostname.
        """
        return self._check_hostname_in_url_database(hostname)

    def _check_hostname(self, hostname):
        database_hostname = HostnameHelper.database_hostname_from_hostname(hostname)
        database_hostname_without_wildcards = HostnameHelper.remove_wildcards(database_hostname)
        possible_matches = self._url_database.fetch_hostnames_with_prefix(database_hostname_without_wildcards)
        # Check whether the hostname is contained in the database
        if database_hostname in possible_matches:
            return True
        # Check whether the hostname contains a wildcard and one of the hostnames in the database matches the wildcard
        if database_hostname.endswith('.*'):
            for possible_hostname in possible_matches:
                if possible_hostname.startswith(database_hostname_without_wildcards + '.') or possible_hostname == database_hostname_without_wildcards:
                    return True

        # No match was found
        return False

    def _check_hostname_in_url_database(self, hostname):
        # Perform a quicker inclusion check first to speed up the verification
        if not self._check_hostname(hostname):
            return []

        database_hostname = HostnameHelper.database_hostname_from_hostname(hostname)
        database_hostname_without_wildcards = HostnameHelper.remove_wildcards(database_hostname)
        malicious_urls = self._url_database.fetch_malicious_urls_for_hostname_prefix(database_hostname_without_wildcards)

        # Filter to only contain valid hostnames respecting wildcards
        malicious_urls = list(filter(lambda url:
            (url.database_hostname.startswith(database_hostname_without_wildcards + '.') and database_hostname.endswith('.*'))
            or url.database_hostname == database_hostname_without_wildcards, malicious_urls))

        return [OSINTURLDBAdapter._osint_malicious_url_to_verification_malicious_url(malicious_url) for malicious_url in malicious_urls]

    def save_malicious_url(self, malicious_url):
        """Insert a new `MaliciousURL` entry into the database (if it does not exist already).

        In the pipeline online sources are checked, which may yield new unknown malicious urls. They
        can be written to the database here in order to find them faster the next time.

        :param malicious_url: A malicious url which may be new and could be saved/updated in the database.
        """
        # Convert the malicious url from the pipeline to the one of the osing_downloader
        self._url_database.save_malicious_url(OSINTURLDBAdapter._verification_malicious_url_to_osint_malicious_url(malicious_url))


    def _osint_malicious_url_to_verification_malicious_url(malicious_url):
        converted_source = MaliciousURLSource(malicious_url.source.value)
        return MaliciousURL(url = malicious_url.url, date = malicious_url.date, source = converted_source)

    def _verification_malicious_url_to_osint_malicious_url(malicious_url):
        converted_source = OSINTMaliciousURLSource(malicious_url.source.value)
        return OSINTMaliciousURL(url = malicious_url.url, date = malicious_url.date, source = converted_source)
