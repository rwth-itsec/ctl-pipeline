from .hostname_database import HostnameDatabase
from .hash_prefix_database import HashPrefixDatabase
from online_sources.online_cascade import OnlineCascade
from .osint_url_db_adapter import OSINTURLDBAdapter
from .osint_hash_db_adapter import OSINTHashDBAdapter

_HOSTNAME_DATABASE_CLASS = OSINTURLDBAdapter
_HASH_PREFIX_DATABASE_CLASS = OSINTHashDBAdapter

class VerificationPipelineResult:
    """Class wrapping the results from the verification pipeline."""
    def __init__(self, certificate, malicious_urls):
        self.certificate = certificate
        self.malicious_urls = malicious_urls

    @property
    def found_in_sources(self):
        if self.malicious_urls is None:
            return None
        return list(set(map(lambda url: url.source, self.malicious_urls)))


class VerificationPipeline:
    """A class representing the verification pipeline.

    The pipeline gets certificate features as input and check it with all the supported sources.
    It also classifies the input certificate using a classifier.
    """
    def __init__(self, hostname_database_path, hash_prefix_database_path):
        """Create a verification pipeline to check if hostnames are occuring in any datasources.

        :params hostname_database_path: Path to a hostname database containing malicious hostnames.
        :params hash_prefix_database_path: Path to a Google Safe Browsing hash prefix database used to check if a lookup using the Lookup API is necessary.
        """
        super(VerificationPipeline, self).__init__()

        self._url_database = _HOSTNAME_DATABASE_CLASS(hostname_database_path)
        self._hash_prefix_database = _HASH_PREFIX_DATABASE_CLASS(hash_prefix_database_path)

        self._online_cascade = OnlineCascade(self._url_database)

    def run(self, certificate, full_check = False):
        """Runs the Verification pipeline for the given certificate.

        :param certificate: A certificate, for which to run the pipeline.
        :param full_check: Boolean flag to indicate, whether the certificate should be checked against all online sources. Passing False will be faster in most cases.
        :returns: A VerificationPipelineResult containing the aggregates verification information
        """
        found_malicious_urls = []
        for hostname in certificate.hostnames:
            url_database_malicious_urls = self._check_hostname_in_url_database(hostname)
            found_malicious_urls.extend(url_database_malicious_urls)

            if len(found_malicious_urls) == 0:
                online_sources_malicious_urls = self._check_hostname_in_online_sources(hostname, full_check = full_check)
                found_malicious_urls.extend(online_sources_malicious_urls)

        if len(found_malicious_urls) > 0:
            return VerificationPipelineResult(certificate, found_malicious_urls)

        return VerificationPipelineResult(certificate, None)

    def _check_hostname_in_url_database(self, hostname):
        return self._url_database.query_hostname(hostname)

    def is_hostname_malicious(self, hostname):
        if len(self._check_hostname_in_url_database(hostname)) > 0:
            return True
        if self._hash_prefix_database.check_hostname(hostname):
            return True
        return False

    def _check_hostname_in_online_sources(self, hostname, full_check = False):
        exists_currently_active_hash_prefix = self._hash_prefix_database.check_hostname_active(hostname)
        return self._online_cascade.check_hostname(hostname, exists_currently_active_hash_prefix, full_check = full_check)

