class HashPrefixDatabase:
    """A database which contains Google Safe Browsing hash prefixes."""

    def __init__(self, database_path):
        super(HashPrefixDatabase, self).__init__()
        self._database_path = database_path

    def check_hostname_active(self, hostname):
        """Check whether the given hostname matches any of the hash prefixes in the database, which are currently active.

        Only returns true, when the hostname check finds a hit, which is currently still active. Hash prefix matches from
        the past are ignored.

        :param: A hostname like 'example.com'
        :returns: A Bool indicating whether the hostname has an active hash prefix match.
        """

        # Steps here should be:
        # 1. Calculate prefixes as explained here: https://developers.google.com/safe-browsing/v4/urls-hashing
        # 2. Check if any of the hashes is in the database and currently still active.
        # 3. Return True if there was a match found, else return false.

        # ATTENTION: The hostnames are X509 SANs & CNs, so they may include wildcards.

        # To disable the usage of the GSB API just return False and remove the raise of the NotImplementedError.
        raise NotImplementedError('TO IMPLEMENT: HashPrefixDatabase.check_hostname_active function.')
        return False

    def check_hostname(self, hostname):
        """Check whether the given hostname matches any of the hash prefixes in the database.

        :param: A hostname like 'example.com'
        :returns: A Bool indicating whether the hostname is contained in the database and such may be malicious.
        """

        # Steps here should be:
        # 1. Calculate prefixes as explained here: https://developers.google.com/safe-browsing/v4/urls-hashing
        # 2. Check if any of the hashes is in the database
        # 3. Return True if there was a match found, else return false.

        # ATTENTION: The hostnames are X509 SANs & CNs, so they may include wildcards.

        # To disable the usage of the GSB API just return False and remove the raise of the NotImplementedError.
        raise NotImplementedError('TO IMPLEMENT: HashPrefixDatabase.check_hostname function.')
        return False
