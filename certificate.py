from enum import Enum

import classifier.x509wrapper as x509wrapper


class CertificateTransparencyLogSource(Enum):
    AVIATOR = 'https://ct.googleapis.com/aviator/ct/v1'
    ROCKETEER = 'https://ct.googleapis.com/rocketeer/ct/v1'
    PILOT = 'https://ct.googleapis.com/pilot/ct/v1'
    ICARUS = 'https://ct.googleapis.com/icarus/ct/v1'
    LE2019 = 'https://oak.ct.letsencrypt.org/2019/ct/v1'
    SKYDIVER = 'https://ct.googleapis.com/skydiver/ct/v1'
    MAMMOTH = 'https://mammoth.ct.comodo.com/ct/v1'
    SABRE = 'https://sabre.ct.comodo.com/ct/v1'
    # PLAUSIBLE = 'https://plausible.ct.nordu.net/ct/v1'
    SUBMARINER = 'https://ct.googleapis.com/submariner/ct/v1'
    DODO = 'https://dodo.ct.comodo.com/ct/v1'
    DAEDALUS = 'https://ct.googleapis.com/daedalus/ct/v1'
    DIGICERT1 = 'https://ct1.digicert-ct.com/log/ct/v1'
    # DIGICERT2 = 'https://ct2.digicert-ct.com/log/ct/v1'
    ARGON2017 = 'https://ct.googleapis.com/logs/argon2017/ct/v1'
    ARGON2018 = 'https://ct.googleapis.com/logs/argon2018/ct/v1'
    ARGON2019 = 'https://ct.googleapis.com/logs/argon2019/ct/v1'
    ARGON2020 = 'https://ct.googleapis.com/logs/argon2020/ct/v1'
    ARGON2021 = 'https://ct.googleapis.com/logs/argon2021/ct/v1'
    ARGON2022 = 'https://ct.googleapis.com/logs/argon2022/ct/v1'
    ARGON2023 = 'https://ct.googleapis.com/logs/argon2023/ct/v1'
    NIMBUS2017 = 'https://ct.cloudflare.com/logs/nimbus2017/ct/v1'
    NIMBUS2018 = 'https://ct.cloudflare.com/logs/nimbus2018/ct/v1'
    NIMBUS2019 = 'https://ct.cloudflare.com/logs/nimbus2019/ct/v1'
    NIMBUS2020 = 'https://ct.cloudflare.com/logs/nimbus2020/ct/v1'
    NIMBUS2021 = 'https://ct.cloudflare.com/logs/nimbus2021/ct/v1'
    NIMBUS2022 = 'https://ct.cloudflare.com/logs/nimbus2022/ct/v1'
    NIMBUS2023 = 'https://ct.cloudflare.com/logs/nimbus2023/ct/v1'
    XENON2020 = 'https://ct.googleapis.com/logs/xenon2020/ct/v1'
    XENON2019 = 'https://ct.googleapis.com/logs/xenon2019/ct/v1'
    XENON2018 = 'https://ct.googleapis.com/logs/xenon2018/ct/v1'
    XENON2021 = 'https://ct.googleapis.com/logs/xenon2021/ct/v1'
    XENON2022 = 'https://ct.googleapis.com/logs/xenon2022/ct/v1'
    XENON2023 = 'https://ct.googleapis.com/logs/xenon2023/ct/v1'
    OAK2019 = 'https://oak.ct.letsencrypt.org/2019/ct/v1'
    OAK2020 = 'https://oak.ct.letsencrypt.org/2020/ct/v1'
    OAK2021 = 'https://oak.ct.letsencrypt.org/2021/ct/v1'
    OAK2022 = 'https://oak.ct.letsencrypt.org/2022/ct/v1'
    OAK2023 = 'https://oak.ct.letsencrypt.org/2023/ct/v1'
    # YETI2018 = 'https://yeti2018.ct.digicert.com/log/ct/v1'
    # YETI2019 = 'https://yeti2019.ct.digicert.com/log/ct/v1'
    YETI2020 = 'https://yeti2020.ct.digicert.com/log/ct/v1'
    YETI2021 = 'https://yeti2021.ct.digicert.com/log/ct/v1'
    YETI2022 = 'https://yeti2022.ct.digicert.com/log/ct/v1'
    YETI2023 = 'https://yeti2023.ct.digicert.com/log/ct/v1'
    # NESSIE2018 = 'https://nessie2018.ct.digicert.com/log/ct/v1'
    # NESSIE2019 = 'https://nessie2019.ct.digicert.com/log/ct/v1'
    NESSIE2020 = 'https://nessie2020.ct.digicert.com/log/ct/v1'
    NESSIE2021 = 'https://nessie2021.ct.digicert.com/log/ct/v1'
    NESSIE2022 = 'https://nessie2022.ct.digicert.com/log/ct/v1'
    NESSIE2023 = 'https://nessie2023.ct.digicert.com/log/ct/v1'

    @property
    def api_url(self):
        return self.value



class Certificate:
    """A class wrapping a certificate identifier and its features."""

    def __init__(self, certificate_transparency_log, log_index, certificate, features):
        self.certificate_transparency_log = certificate_transparency_log
        self.log_index = log_index
        self.features = features
        self.fields = None
        if certificate is not None:
            self.serial_number = x509wrapper.get_serial_number(certificate)
            self.issuer = x509wrapper.issuer_get_from_oid(certificate, x509wrapper.oid_cn)
        else:
            self.serial_number = None
            self.issuer = None

    @property
    def hostnames(self):
        return list(map(lambda features: features.san, self.features.extracted_san_features))

    @property
    def hostnames_string(self):
        return ', '.join(self.hostnames)

    @property
    def identifier(self):
        return f"{self.certificate_transparency_log.name}:{self.log_index}"

    @property
    def unique_id(self):
        assert self.serial_number is not None and self.issuer is not None, "The unique id of a certificate requires issuer and serial number to be known."
        return f'{self.issuer}_{self.serial_number}'



