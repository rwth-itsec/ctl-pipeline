from .pipeline_component import PipelineComponent
from .pipeline_models import PipelineResult, VerificationHit
from verification.verification_pipeline import VerificationPipeline

class VerificationPipelineRunner(PipelineComponent):
    """This component runs the `VerificationPipeline` on the incoming `PipelineResults`.

    Depending on whether the result is a positive or negative a full online check is performed or not. The online check is only done for positives for performance reasons and due to API limits.
    """

    def __init__(self, input_queue, positives_output_queue, negatives_output_queue, url_database_path, gsb_hash_prefix_database_path):
        super(VerificationPipelineRunner, self).__init__(input_queue, positives_output_queue)
        self._url_database_path = url_database_path
        self._gsb_hash_prefix_database_path = gsb_hash_prefix_database_path

        self._positives_output_queue = positives_output_queue
        self._negatives_output_queue = negatives_output_queue
        self.add_additional_output(self._negatives_output_queue)

    def before_start(self):
        self._verification_pipeline = VerificationPipeline(self._url_database_path, self._gsb_hash_prefix_database_path)

    def run_single(self):
        next_queue_element = self._get_from_input_queue()
        if next_queue_element is None:
            return
        certificate, pipeline_result = next_queue_element

        verification_result = self._verification_pipeline.run(certificate, full_check = pipeline_result.is_positive)

        for malicious_url in verification_result.malicious_urls or []:
            pipeline_result.add_verification_hit(VerificationHit(source = malicious_url.source, created_at = malicious_url.date))

        self.logger.info(f'Verification found: {len(verification_result.malicious_urls or [])} hits ({certificate.identifier}). (full_check = {pipeline_result.is_positive})')

        # Split the outputs dependent on whether they were classified positively or negatively
        if pipeline_result.is_positive:
            self._positives_output_queue.put((certificate, pipeline_result))
        else:
            self._negatives_output_queue.put((certificate, pipeline_result))

