from .pipeline_component import PipelineComponent
from .pipeline_models import PipelineResult, San

class ClassificationRunner(PipelineComponent):
    """Pipeline component runs the classifier on the incoming certificates.

    This component is special, since it takes in only batches of certificates and outputs tuples of certificates and a new `PipelineResult`, which contains the classification score and other metadata.
    """

    def __init__(self, input_queue, output_queue, classifier_configuration, parallel_classifications):
        super(ClassificationRunner, self).__init__(input_queue, output_queue)
        self._parallel_classifications = parallel_classifications
        self._classifier_configuration = classifier_configuration

    def before_start(self):
        self._load_classifier()

    def run_single(self):
        certificates = self._get_input_batch(self._parallel_classifications)
        if len(certificates) == 0:
            return

        certificate_features = [certificate.features for certificate in certificates]
        classifications = []

        # The classifier crashes sometimes, so wrap it in a try except block
        try:
            classifications = self._classifier.predict(certificate_features)
        except Exception as exception:
            all_certificates_identifiers = [certificate.identifier for certificate in certificates]
            self.logger.error(f'Classifier crashed for one of the following identifiers: {", ".join(all_certificates_identifiers)}. {exception}')

        try:
            classifications = [c[0] for c in classifications]
        except (IndexError, TypeError):
            pass

        for (certificate, classification_score) in zip(certificates, classifications):
            classification_score = float(classification_score)
            pipeline_result = PipelineResult(ctl = certificate.certificate_transparency_log, ctl_index = certificate.log_index, issuer_name = certificate.issuer, serial_number = certificate.serial_number, classification_score = classification_score, is_positive = classification_score >= self._classifier.classification_threshold)
            for san in certificate.hostnames:
                pipeline_result.add_san(San(san = san))
            self._output((certificate, pipeline_result))

    def _load_classifier(self):
        self._classifier = self._classifier_configuration['classifier_class']()
        if self._classifier_configuration.get('path') is not None:
            self._classifier.load(self._classifier_configuration['path'])

        self._classifier.classification_threshold = self._classifier_configuration['threshold']
        if self._classifier_configuration['meta_classifier'] is not None:
            self._classifier.meta_classifier = self._classifier_configuration['meta_classifier']
        if self._classifier_configuration['used_features'] is not None:
            self._classifier.used_features = self._classifier_configuration['used_features']
