from certificate import CertificateTransparencyLogSource

from datetime import datetime
from verification.malicious_url import MaliciousURLSource


class PipelineResult(object):
    """Model class containing the metadata to identify the certificate belonging to the data, as well as the classification score ans other metadata."""

    def __init__(self, **kwargs):
        super(PipelineResult, self).__init__()

        self._id = kwargs.get('id')
        if isinstance(kwargs.get('ctl'), CertificateTransparencyLogSource):
            self.ctl = kwargs.get('ctl')
        else:
            self.ctl = CertificateTransparencyLogSource[kwargs.get('ctl')]
        self.ctl_index = kwargs.get('ctl_index')
        self.issuer_name = kwargs.get('issuer_name')
        self.serial_number = kwargs.get('serial_number')
        self.classification_score = kwargs.get('classification_score')
        self.created_at = kwargs.get('created_at', datetime.now())
        self.is_positive = kwargs.get('is_positive', False)

        self._sans = None
        self._verification_hits = None
        self._screenshots = None

    def add_san(self, san):
        if self._sans is None:
            self._sans = []
        self._sans.append(san)

    def add_verification_hit(self, hit):
        if self._verification_hits is None:
            self._verification_hits = []
        self._verification_hits.append(hit)

    def add_screenshot(self, screenshot):
        if self._screenshots is None:
            self._screenshots = []
        self._screenshots.append(screenshot)

    @property
    def sans(self):
        return self._sans or []

    @property
    def verification_hits(self):
        return self._verification_hits or []

    @property
    def screenshots(self):
        return self._screenshots or []


class San(object):
    """A model class representing one SAN of the certificate belonging to a `PipelineResult`. (relationship: many `SAN`s to one `PipelineResult`)
    """

    def __init__(self, **kwargs):
        super(San, self).__init__()

        self._id = kwargs.get('id')
        self._result_id = kwargs.get('result_id')
        self.san = kwargs.get('san')


class VerificationHit(object):
    """A model class storing the metadata for one positive verification result belonging to a `PipelineResult`. (relationship: many `VerificationHit`s to one `PipelineResult`)
    """

    def __init__(self, **kwargs):
        super(VerificationHit, self).__init__()

        self._id = kwargs.get('id')
        self._result_id = kwargs.get('result_id')
        if isinstance(kwargs.get('source'), MaliciousURLSource):
            self.source = kwargs.get('source')
        else:
            self.source = MaliciousURLSource[kwargs.get('source')]
        self.created_at = kwargs.get('created_at', datetime.now())

class Screenshot(object):
    """A model class storing the metadata for one screenshot of a SAN of the certificate belonging to a `PipelineResult`. (relationship: many `Screenshot`s to one `PipelineResult`)
    """

    def __init__(self, **kwargs):
        super(Screenshot, self).__init__()

        self._id = kwargs.get('id')
        self._result_id = kwargs.get('result_id')
        self.created_at = kwargs.get('created_at', datetime.now())
        self.file_path = kwargs.get('file_path')
