import time
import os.path
import random
from selenium import webdriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, WebDriverException
from PIL import Image
from io import BytesIO

from .pipeline_component import PipelineComponent
from .pipeline_models import PipelineResult, Screenshot
from helper import san_to_url

class ScreenshotGrabber(PipelineComponent):
    """This compoenet grabs screenshots for all the SANs of the incoming `PipelineResults`."""

    def __init__(self, input_queue, output_queue, screenshot_base_directory, screenshot_tp_directory, screenshot_fp_directory):
        super(ScreenshotGrabber, self).__init__(input_queue, output_queue)
        self._screenshot_base_directory = screenshot_base_directory
        self._screenshot_tp_directory = screenshot_tp_directory
        self._screenshot_fp_directory = screenshot_fp_directory

    def before_start(self):
        self._start_webdriver()

    def _start_webdriver(self):
        try:
            self._driver = webdriver.Firefox()
            self._driver.set_page_load_timeout(10)
            self._driver.set_script_timeout(10)
            self._driver.set_window_size(1920, 1080)
        except WebDriverException:
            self.logger.error("Could not start webdriver, setting component to finished. Check the selenium setup and restart the pipeline.")
            self.set_finished()

    def run_single(self):
        next_queue_element = self._get_from_input_queue()
        if next_queue_element is None:
            return
        certificate, pipeline_result = next_queue_element

        try:
            for san in pipeline_result.sans:
                url = san_to_url(san.san)
                if url is None:
                    continue

                image = self._grab_screenshot(url)
                if image is None:
                    continue

                # Convert to RGB to be able to store it as jpg
                image = image.convert('RGB')

                # Downscale to width 1400
                image = image.resize((1400, int(1400 / image.size[0] * image.size[1])))

                directory = None
                if len(pipeline_result.verification_hits) > 0:
                    directory = self._screenshot_tp_directory
                else:
                    directory = self._screenshot_fp_directory
                file_name = os.path.join(directory, f'screenshot-{str(time.time()).split(".")[0]}-{san.san.replace(".", "_")}.jpg')
                image.save(os.path.join(self._screenshot_base_directory, file_name), 'JPEG', quality = 85)

                pipeline_result.add_screenshot(Screenshot(file_path = file_name))
        except Exception as e:
            # Catch basicall anything to make sure, that no positives are lost in the process
            self.logger.exception(f'Screenshot grabber crash recovered: {e}')

        self._output((certificate, pipeline_result))

    def cleanup(self):
        try:
            self._driver.quit()
        except:
            pass

    def _grab_screenshot(self, url):
        try:
            self._driver.get(url)

            # Wait for the root element to load
            element_present = expected_conditions.presence_of_element_located((By.XPATH, '/*'))
            WebDriverWait(self._driver, 10).until(element_present)

            # Wait for eventual other elements/requests to load performed by JavaScript
            time.sleep(5)

            element = self._driver.find_element_by_xpath("/*")
            image = Image.open(BytesIO(element.screenshot_as_png))
            self.logger.info(f'ScreenshotGrabber: Took screenshot of website: {url}')

            return image
        except TimeoutException as exception:
            self.logger.error(f'Timeout, skipping screenshot: {url}\n{str(exception).strip()}')

        except WebDriverException as exception:
            self.logger.error(f'A problem with the WebDriver occurred, skipping screenshot: {url}\n{str(exception).strip()}')

        return None
