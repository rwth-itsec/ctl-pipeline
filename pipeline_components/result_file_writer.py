import csv
from .pipeline_component import PipelineComponent

class ResultFileWriter(PipelineComponent):
    """This component writes the incoming `PipelineResults` to a csv file.

    Compared to the `ResultSaver` this component can write results very fast. The trade off is, that not all available metadata is stored.
    """

    def __init__(self, input_queue, output_queue, result_file_path, writer_batch_size):
        super(ResultFileWriter, self).__init__(input_queue, output_queue)
        self._result_file_path = result_file_path
        self._writer_batch_size = writer_batch_size

    def before_start(self):
        with open(self._result_file_path, 'a+') as file:
            writer = csv.writer(file, delimiter = ';')
            writer.writerow(self._csv_header())

    def run_single(self):
        batch = self._get_input_batch(self._writer_batch_size)
        if len(batch) == 0:
            return

        with open(self._result_file_path, 'a+') as file:
            writer = csv.writer(file, delimiter = ';')
            for certificate, pipeline_result in batch:
                writer.writerow(self._output_line_for(certificate, pipeline_result))

        for certificate, pipeline_result in batch:
            self._output((certificate, pipeline_result))


    def _output_line_for(self, certificate, pipeline_result):
        return [certificate.identifier, certificate.issuer, certificate.serial_number, certificate.hostnames_string, pipeline_result.classification_score, len(pipeline_result.verification_hits)]

    def _csv_header(self):
        return ['log_name:index', 'issuer', 'serial_number', 'SANs', 'classification_score', 'number_of_verification_hits']
