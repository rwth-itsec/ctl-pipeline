from .pipeline_component import PipelineComponent
from .pipeline_database import PipelineDatabase

class ResultSaver(PipelineComponent):
    """This component writes the incoming results to a SQLite database."""

    def __init__(self, input_queue, output_queue, database_path):
        super(ResultSaver, self).__init__(input_queue, output_queue)
        self._database_path = database_path

    def before_start(self):
        self._database = PipelineDatabase(self._database_path)

    def run_single(self):
        next_queue_element = self._get_from_input_queue()
        if next_queue_element is None:
            return
        certificate, pipeline_result = next_queue_element

        self._database.save_pipeline_result(pipeline_result)
        self._output((certificate, pipeline_result))

    def cleanup(self):
        self._database.close()
