from util.ct_query import CtLogQuery
from classifier.feature_extractor import CertificateFeatures, FeatureExtractionError

from certificate import Certificate, CertificateTransparencyLogSource
from .pipeline_component import PipelineComponent

class HistoricalDownloader(PipelineComponent):
    """This pipeline component downloads past certificates from a CT log.

    The certificates downloaded are not in a consecutive range, but in many small ranges. The start and end indices are not surpassed when downloading certificates. Between these indices many batches of `batch_size` are downloaded, they are advanced using the `begin` and `advance` parameters.
    """

    def __init__(self, output_queue, certificate_transparency_log_source, start_leaf_id, end_lead_id, begin, advance, batch_size):
        super(HistoricalDownloader, self).__init__(None, output_queue)
        self._certificate_transparency_log_source = certificate_transparency_log_source
        self._start_leaf_id = start_leaf_id
        self._end_leaf_id = end_lead_id
        self._begin = begin
        self._advance = advance
        self._batch_size = batch_size
        self._current_index = self._begin

    def before_start(self):
        self._certificate_log_query = CtLogQuery(self._certificate_transparency_log_source.api_url, log_start_info = False)

    def run_single(self):
        start = self._start_leaf_id + (self._current_index * self._batch_size)
        # Finish if the start index is larger than the end index.
        # Here manual finish is needed, since it is the first component in the pipeline and such
        # also the component defining when no new data is available to analyze.
        if start > self._end_leaf_id:
            self.set_finished()
            return
        end = start + self._batch_size - 1
        new_certificates = self._certificate_log_query.get_certs_in_range(start, end)
        for index_offset, certificate in enumerate(new_certificates):
            if start + index_offset <= self._end_leaf_id:
                try:
                    certificate = Certificate(self._certificate_transparency_log_source, start + index_offset, certificate['leaf_cert'], CertificateFeatures.extract(certificate['leaf_cert']))
                    self._output(certificate)
                except FeatureExtractionError as error:
                    self.logger.error(f'Feature extraction error in {self._certificate_transparency_log_source.name}:{start + index_offset}: {error}')
        self._current_index += self._advance
