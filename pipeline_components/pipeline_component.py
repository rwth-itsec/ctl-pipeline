from multiprocessing import Queue, Process
from multiprocessing.queues import Empty as QueueEmptyException
import time
from helper import keyboard_interrupt_ignoring
import random

class TerminationToken:
    """A class representing a termination token. It can be put into the output queue to tell the next process that one of its predecessors has finished."""
    pass

class PipelineComponent(object):
    """Base class for pipeline components.

    It defines some common interface for the components and also manages the execution flow of the component. So the looping and finishing logic is abstracted in this class.
    Furthermore it also deines some common utility functions, which can be used in the components and which will automatically respect the component state.
    """

    def __init__(self, input_queue, output_queue):
        super(PipelineComponent, self).__init__()
        self._input_queue = input_queue
        self._finished = False
        self._number_of_direct_predecessors = 1
        self._number_of_direct_successors = 1
        self._number_of_registered_terminations = 0
        self._outputs = []
        self._id = random.randint(1000000, 9999999)

        if output_queue is not None:
            self._outputs.append(output_queue)

    def set_number_of_direct_predecessors(self, count):
        """Wait for this many processes to finish before it finished itself."""
        self._number_of_direct_predecessors = count

    def set_number_of_direct_successors(self, count):
        """Add this many termination tokens to the queue when finished."""
        self._number_of_direct_successors = count

    def set_logger(self, logger):
        """Set the logger to be used by this component.

        :param logger: A logging logger.
        """
        self._logger = logger

    def add_additional_output(self, queue):
        """Add an additional output queue. If another queue is added the elements are put in all output queues,
        effectively performing a fork of the pipeline.

        :param queue: A multiprocessing queue.
        """
        self._outputs.append(queue)

    @property
    def logger(self):
        """The logger used by this component."""
        return self._logger

    def timeout(self):
        """Override this method and return a float/int to wait for a given amount of time after each `run_single` execution.

        :returns: Seconds (int or float).
        """
        return None

    @keyboard_interrupt_ignoring
    def start(self):
        """This function handles the execution flow of the component.

        It is the method called by the runner process of this component. So the process will exit when this method returns.
        """
        self.before_start()
        while not self._finished:
            try:
                self.run_single()
            except Exception as e:
                self.logger.exception(f'PipelineComponent {self._id} run_single crashed: {e}')
            if self.timeout() is not None:
                time.sleep(self.timeout())
        self.cleanup()
        self.finish_queues()
        self.logger.debug(f"Exiting: {self.__class__.__name__}")

    def set_finished(self):
        """Set the finished flag of the component to True"""
        self._finished = True

    def finish_queues(self):
        """Calling this method will signal termination to the following components and stop execution."""
        for _ in range(self._number_of_direct_successors):
            self._output(TerminationToken())
        self.logger.debug(f"Finished: {self.__class__.__name__}")

    def before_start(self):
        """This method is called before the first `run_single` call.

        A subclass can override this method to perform setup before the component starts execution.
        """
        pass

    def cleanup(self):
        """This method is called when the component finished.

        A subclass can override this method to perform cleanup.
        """
        pass

    def run_single(self):
        """This method should be implemented in the subclass and perform an individual execution of the component. So in case of a downloader one batch should be downloaded.

        The method is called continuously until the component received the finishing signal."""
        raise NotImplemented

    def process(self):
        """Get the multiprocessing process for this component. Will return a new process on each call."""
        return Process(target = self.start)

    def _output(self, obj):
        """Output the given object to all output queues."""
        # Put the object in all output queues, effectively copying it
        for queue in self._outputs:
            queue.put(obj)

    def _get_from_input_queue(self, block = True):
        """Retrieve a single object from the input queue.

        :param block: Whether or not to wait for an element if the input queue is empty, or not.
        :returns: None if no object is there (and block == False) / the previous component finished. Else one element from the input queue.
        """
        element = self._input_queue.get(block)
        if isinstance(element, TerminationToken):
            self._number_of_registered_terminations += 1
            if self._number_of_registered_terminations >= self._number_of_direct_predecessors:
                self.set_finished()
            return None
        return element

    def _get_input_batch(self, size):
        """Helper method to get a match from the input queue. If the queue contains no / not enough elements, less elements will be returned. This call is non blocking, so no waiting for input is performed.

        :param size: Desired batch size.
        :returns: A list containing up to size many elements from the input queue, never returns None.
        """
        batch = []
        for _ in range(size):
            try:
                element = self._get_from_input_queue(False)
                if element is not None:
                    batch.append(element)
                else:
                    return batch # Do not get more elements here, since it could remove another end token, which is eventually not wanted (only a small performance hit)
            except QueueEmptyException as e:
                pass
        return batch
