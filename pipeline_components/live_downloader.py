from util.ct_query import CtLogQuery
from classifier.feature_extractor import CertificateFeatures, FeatureExtractionError

from certificate import Certificate, CertificateTransparencyLogSource
from .pipeline_component import PipelineComponent

class LiveDownloader(PipelineComponent):
    """This component downloads the most recent certificates from a CT log.

    This component can be used as starting point for the live classification, where always the most recent certificates are needed.
    """

    def __init__(self, output_queue, certificate_transparency_log_source):
        super(LiveDownloader, self).__init__(None, output_queue)
        self._certificate_transparency_log_source = certificate_transparency_log_source

    def timeout(self):
        return 1

    def before_start(self):
        self._certificate_log_query = CtLogQuery(self._certificate_transparency_log_source.api_url)
        self.start = self._certificate_log_query.get_tree_size()

    def run_single(self):
        new_certificates, start_index = self._certificate_log_query.get_new_certs()
        for index_offset, certificate in enumerate(new_certificates):
            try:
                certificate = Certificate(self._certificate_transparency_log_source, self.start + index_offset, certificate['leaf_cert'], CertificateFeatures.extract(certificate['leaf_cert']))
                self._output(certificate)
            except FeatureExtractionError as error:
                self.logger.error(f'Feature extraction error in {self._certificate_transparency_log_source.name}:{self.start + index_offset}: {error}')
