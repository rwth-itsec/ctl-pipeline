import sqlite3

from pipeline_components.pipeline_models import *

class PipelineDatabase(object):

    def __init__(self, database_file):
        super(PipelineDatabase, self).__init__()
        self.database_file = database_file
        self._common_init_database()

    def cursor(self):
        try:
            return self.database_connection.cursor()
        except Error as error:
            raise error
        return None

    def commit(self):
        self.database_connection.commit()

    def close(self):
        self.database_connection.close()

    def init_database(self):
        self.cursor().execute("""
            CREATE TABLE IF NOT EXISTS pipeline_results(
                id integer PRIMARY KEY AUTOINCREMENT,
                created_at timestamp,
                ctl text,
                ctl_index integer,
                issuer_name text,
                serial_number text,
                classification_score real,
                is_positive boolean
            );
        """)
        self.cursor().execute("""
            CREATE TABLE IF NOT EXISTS sans(
                id integer PRIMARY KEY AUTOINCREMENT,
                result_id integer,
                san text
            );
        """)
        self.cursor().execute("""
            CREATE TABLE IF NOT EXISTS verification_hits(
                id integer PRIMARY KEY AUTOINCREMENT,
                created_at timestamp,
                result_id integer,
                source text
            );
        """)
        self.cursor().execute("""
            CREATE TABLE IF NOT EXISTS screenshots(
                id integer PRIMARY KEY AUTOINCREMENT,
                created_at timestamp,
                result_id integer,
                file_path text
            );
        """)

        # Do not create the san index for now, since it will reduce insertion speed
        # self.cursor().execute("CREATE INDEX IF NOT EXISTS sans_san_index ON sans(san COLLATE NOCASE)")

        self.commit()


    def _common_init_database(self):
        self.database_connection = sqlite3.connect(self.database_file, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
        self.database_connection.row_factory = sqlite3.Row

        self.init_database()


    def save_pipeline_result(self, pipeline_result):
        cursor = self.cursor()
        is_positive = 1 if pipeline_result.is_positive == True else 0
        cursor.execute("INSERT OR REPLACE INTO pipeline_results (id, created_at, ctl, ctl_index, issuer_name, serial_number, classification_score, is_positive) VALUES (?,?,?,?,?,?,?,?)", (pipeline_result._id, pipeline_result.created_at, pipeline_result.ctl.name, pipeline_result.ctl_index, pipeline_result.issuer_name, pipeline_result.serial_number, pipeline_result.classification_score, is_positive))
        pipeline_result_id = cursor.lastrowid

        if pipeline_result._verification_hits is not None:
            for hit in pipeline_result._verification_hits:
                hit._result_id = pipeline_result_id
                self._save_verification_hit(cursor, hit)

        if pipeline_result._sans is not None:
            for san in pipeline_result._sans:
                san._result_id = pipeline_result_id
                self._save_san(cursor, san)

        if pipeline_result._screenshots is not None:
            for screenshot in pipeline_result._screenshots:
                screenshot._result_id = pipeline_result_id
                self._save_screenshot(cursor, screenshot)

        self.commit()

    def _save_verification_hit(self, cursor, hit):
        cursor.execute("INSERT OR REPLACE INTO verification_hits (id, created_at, result_id, source) VALUES (?,?,?,?)", (hit._id, hit.created_at, hit._result_id, hit.source.name))

    def _save_san(self, cursor, san):
        cursor.execute("INSERT OR REPLACE INTO sans (id, result_id, san) VALUES (?,?,?)", (san._id, san._result_id, san.san))

    def _save_screenshot(self, cursor, screenshot):
        cursor.execute("INSERT OR REPLACE INTO screenshots (id, created_at, result_id, file_path) VALUES (?,?,?,?)", (screenshot._id, screenshot.created_at, screenshot._result_id, screenshot.file_path))


    def fetch_all(self):
        pipeline_results = []
        cursor = self.cursor()

        for result in cursor.execute('SELECT * FROM pipeline_results').fetchall():
            pipeline_result = PipelineResult(**self.dictionary(result))
            for san in cursor.execute('SELECT * FROM sans WHERE sans.result_id = ?', (pipeline_result._id,)).fetchall():
                pipeline_result.add_san(San(**self.dictionary(san)))
            for hit in cursor.execute('SELECT * FROM verification_hits WHERE verification_hits.result_id = ?', (pipeline_result._id,)).fetchall():
                pipeline_result.add_verification_hit(VerificationHit(**self.dictionary(hit)))
            pipeline_results.append(pipeline_result)

        return pipeline_results

    def dictionary(self, record):
        dictionary = {}
        for key in record.keys():
            dictionary[key] = record[key]
        return dictionary