import dateutil.parser
from datetime import datetime

def parse_date(date_string):
    """Convert the given ISO8601 date string to a `datetime` object."""
    try:
        date = datetime.fromisoformat(date_string)
        return date
    except:
        pass

    return dateutil.parser.isoparse(date_string)