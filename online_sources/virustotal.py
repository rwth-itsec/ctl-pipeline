import requests

from verification.malicious_url import MaliciousURL, MaliciousURLSource

from .date_helper import parse_date
from .config import *

class VirusTotal:

    def __init__(self, api_key):
        super(VirusTotal, self).__init__()
        self.api_key = api_key

    def check_url(self, url):
        history_url = "https://www.virustotal.com/vtapi/v2/url/report?apikey=%s&resource=%s" % (self.api_key, url)
        parsed_response = None
        try:
            response = requests.get(history_url, timeout = NETWORK_TIMEOUT)
        except requests.exceptions.RequestException as error:
            print("VirusTotal.check_url: An error occurred during checking the url: %s" % (error))
            return False, None

        if response.status_code == 204:
            return True, None

        # The request limit is not reached, but another error occurred
        if not response.ok:
            return False, None

        parsed_response = response.json()
        if parsed_response is not None and "positives" in parsed_response and parsed_response["positives"] > 0:
            return False, MaliciousURL(url = url, date = parse_date(parsed_response["scan_date"]), source = MaliciousURLSource.VIRUSTOTAL)

        return False, None
