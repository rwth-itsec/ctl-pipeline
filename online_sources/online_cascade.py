from datetime import datetime

from credentials import *

from .virustotal import VirusTotal
from .google_safe_browsing_lookup import GoogleSafeBrowsingLookup
from helper import san_to_url

class OnlineCascade:

    def __init__(self, url_database):
        super(OnlineCascade, self).__init__()
        self._virustotal = VirusTotal(VIRUSTOTAL_API_KEY)
        self._google_safe_browsing_lookup = GoogleSafeBrowsingLookup(GOOGLE_SAFE_BROWSING_API_KEY, GOOGLE_SAFE_BROWSING_CLIENT_ID)
        self._url_database = url_database

    def check_hostname(self, hostname, check_google_safe_browsing_lookup, full_check = False):
        # Build a https url, since we are checking certificate domain names
        url = san_to_url(hostname)

        # Request all online sources (for now) and record malicious url results
        malicious_urls = []

        if full_check:
            virustotal_rate_limit_reached, virustotal_malicious_url = self._virustotal.check_url(url)
            if virustotal_malicious_url is not None:
                malicious_urls.append(virustotal_malicious_url)

        if check_google_safe_browsing_lookup:
            gsb_rate_limit_reached, gsb_malicious_url = self._google_safe_browsing_lookup.check_url(url)
            if gsb_malicious_url is not None:
                malicious_urls.append(gsb_malicious_url)

        # Store all non None malicious url results in the url database
        for malicious_url in malicious_urls:
            self._url_database.save_malicious_url(malicious_url)

        return malicious_urls
