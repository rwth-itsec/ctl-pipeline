import requests

from verification.malicious_url import MaliciousURL, MaliciousURLSource

from .date_helper import parse_date
from .config import *

class GoogleSafeBrowsingLookup:

    def __init__(self, api_key, client_id):
        super(GoogleSafeBrowsingLookup, self).__init__()
        self.api_key = api_key
        self.client_id = client_id

    def check_url(self, url):
        lookup_url = "https://safebrowsing.googleapis.com/v4/threatMatches:find?key=%s" % (self.api_key)
        body = {
            "client": { "clientId": self.client_id, "clientVersion": "0.1" },
            "threatInfo": {
                "threatTypes": ["SOCIAL_ENGINEERING", "MALWARE"],
                "platformTypes": ["ANY_PLATFORM"],
                "threatEntryTypes": ["URL"],
                "threatEntries": [{ "url": url }]
            }
        }
        response = requests.post(lookup_url, json = body, headers = { "Content-Type": "application/json" })

        if not response.ok:
            return False, None

        parsed_response = response.json()

        if 'matches' in parsed_response and len(parsed_response['matches']) > 0:
            for match in parsed_response['matches']:
                # Check if the thread is social engineering (phishing) and if the threat url is the same as the requested one
                # Normally in case of phishing there is only one match containing exactly the requested url.
                if match['threatType'] == 'SOCIAL_ENGINEERING' and match['threatEntryType'] == 'URL' and match['threat']['url'] == url:
                    return False, MaliciousURL(url = match['threat']['url'], source = MaliciousURLSource.GOOGLE_SAFE_BROWSING_LOOKUP)
        return False, None
