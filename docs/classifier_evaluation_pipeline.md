# Classifier evaluation pipeline

The job of the classifier evaluation pipeline is to evaluate a classifier on CT log certificates in a given time range. Therefore the classifier to be evaluated is classifying the certificates in a CT log, if multiple classifiers should be evaluated this process can be done in parallel. The classification positives and negatives are checked against offline databases, while the positives are additionally checked against online sources using their APIs. This is done to check, whether the positives are true positives or not.

The general idea is, that the certificates are downloaded from the logs, the features for each certificate are extracted using the `feature_extractor.py` in the `classifier` directory. Then the classifier is asked to make a prediction on the certificate. After the prediction the offline and online sources are checked (depending on the outcome of the prediction), to check whether the hostname is known malicious or not. Finally the results are stored. Then in a second separate step the raw results can be used for an evaluation of the classifier.


## Mode of operation

The pipeline currently only runs on one CT log. The steps of the evaluation can be summarized roughly as following:

1. Download certificates from CT log in a given range of indices.
2. Extract features of the certificates
3. Classify certificates using the classifier to evaluate
4. Verify all results against the offline databases (phishing urls and Google Safe Browsing hash prefixes)
5. If there is a hash prefix match, check online using the Google Safe Browsing lookup API
6. Check all positives against online databases as well
7. Take screenshots of websites if needed/wanted
8. Write positives to SQLite database
9. Write negatives to csv files (with reduced information set)

## The data structure & DB schema
- `PipelineResult` holds all metadata for the result (ctl, leaf index, serial number, issuer, classification score, ...)
- `San` stores the sans of a `PipelineResult` (1-to-many relation)
- `VerificationHit` for each verification hit (e.g. PhishTank match), there is one `VerificationHit` created storing important metadata (1-to-many relation)
- `Screenshot` if positives should be screenshoted, this model stores the relevant metadata for the screenshot (1-to-many relation)

## The components
The pipeline consists of multiple components, which all have their own responsibility. One can think of the pipeline as a multistage producer consumer pattern, where the information is propagated using `multiprocessing.Queue`s. The components have one input and output queue, there are some exceptions, but generally this is the case. The components are then processing the entries from the input queue(s) and pushing them to the output queue(s).


### Historical Downloader
`pipeline_components/historical_downloader.py`

There are multiple downloaders running in a pipeline. Each downloader is responsible for a batch of certificates (which is not a consecutive range of certificates). After downloading a batch it extracts the features for each certificate using the `feature_extractor` and then pushes it to the output queue. In case multiple classifiers should be evaluated in parallel, the extracted certificate features are put into multiple queues (one per classifier). Only the downloader stage is shared for the evaluation of multiple classifiers in parallel.

### Classifier stage
`pipeline_components/classification_runner.py`

The next step in the pipeline is to classify all certificates based on the extracted features. The `ClassificationRunner` executes the given classifier on all certificates in the input queue. It then places the newly created `PipelineResult` containing the general information and classification metadata into the output queue (together with the classified certificate).

### Verification stage
`pipeline_components/verification_pipeline_runner.py`

The `VerificationPipelineRunner` runs the `VerificationPipeline` for all classified certificates. All classifications/certificates are checked against the offline url database containing the phishing urls collected from various sources. Furthermore all positives are checked against the GoogleSafeBrowsing hash database, which is also kept offline. If a hash match is found offline, the corresponding url is checked also using the GoogleSafeBrowsing Lookup API. For positives (classifications with a classification score larger than the threshold for the classifier) also online sources are checked, which is currently only VirusTotal.

There are multiple `VerificationPipelineRunner`s running in parallel, to speed up the verification. At the end of the verification stage the positives and negatives are separated. There are two output queues, one for positives and one for negatives. This is also a performance measure, since for the negatives less metadata is stored than for the positives. The negatives are not written to a SQLite database, but to a csv file, which is much faster than the SQLite database used for the positives, so it can keep up with the amount of negatives coming from the pipeline. Another advantage is, that now positives could be processed further (e.g. take screenshots) without the need for later filtering.

### Negatives saving
`pipeline_components/result_file_writer.py`

The `ResultFileWriter` writes the incoming `PipelineResult`s into a csv file. Currently this writer is only used for negatives, which are stored with less metadata to speed up the pipeline. It writes a csv file, one line for each result containing unique identifiers, classification score, hostnames/sans and the number of verification hits (to determine if it was a true negative or not later).

In each pipeline multiple file writers are used to write to multiple files in parallel. Currently the files are just numbered and all located in one directory designated only for the negatives csv files.

### Screenshot stage
`pipeline_components/screenshot_grabber.py`

If a screenshot should be taken of a website in the pipeline, a `ScreenshotGrabber` stage can be injected into the chain of components. Then all entries in the input queue will be "enriched" with a screenshot. Meaning a screenshot is taken and written to a file, while the screenshot metadata, in the form of a `Screenshot` object, is added to the incoming `PipelineResult`. Then the certificate and the `PipelineResult` are added to the output queue again.

The screenshots itself are taken using selenium and the Firefox browser. The `ScreenshotGrabber` tries to take a screenshot of the whole page by taking a screenshot not of the screen itself, but the root element. The screenshot is then downscaled, JPEG compressed and saved to a file. The file path and creation date is added in a `Screenshot` model object to the `PipelineResult`.

### Positives saving
`pipeline_components/result_saver.py`

The `ResultSaver` can store a `PipelineResult` together with all relations in a SQLite database. This is currently done for all pipeline results, which were classified as positives by the used classifier. The schema of the SQLite database has the same object structure and relations as described above.

### Live usage
`pipeline_components/live_downloader.py`

There is also the option to run the pipeline "live" on a CT log. This can be done by exchanging the historical downloader by a live downloader. The live downloader checks the CT log for new certificates in a regular interval and pushes them in the output queue for processing.

## Running a pipeline
Currently the pipeline can be executed using the `pipeline_runner.py` script. It initializes and "wires up" all the pipeline components used in the pipeline run.

The `pipeline_runner.py` also configures the classifiers and writes a file called `used_config.txt` to store the used configuration and some more metadata on the pipeline execution. This file can help later to know exactly which parameters were used to get to the results.

To start a pipeline several input parameters are needed. A basic execution using one classifier could look like this:

```
python pipeline_runner.py -u ../db/phishing_urls.db -p ../db/gsb_hash_prefixes.db -o ../db -b 1041131705 -e 1047364548 -l ROCKETEER -c ../classifiers/demo_classifier.pkl
```
- `-u` path to the url database file
- `-p` path to the Google Safe Browsing hash prefix database
- `-o` an output directory, in which the directory containing all output data should be created
- `-b` the start CT leaf index for the evaluation
- `-e` the end CT leaf index for the evaluation
- `-l` the log to run the evaluation on
- `-c` list of file paths to the classifiers to evaluate in this run

The created directory containing the results has a name according to the following scheme: `run_[logname]_[start_index]_[end_index]_[timestamp]`

If screenshots should be taken add the `-s` flag/parameter to the command. The screenshots will also be written to the output directory.

## Configuring a classifier to run
Before a new classifier can be used in the pipeline some information needs to be given to the pipeline. The `classifier_configurations.py` file contains a class called `ClassifierConfigurations`, which currently contains the configurations for the classifiers. Each classifier is identified by the file name (without the file type extension). In the above example the classifier name would be `demo_classifier`.

When a new classifier is added, the configuration in this file needs to be extended with the information for this classifier. The classifier class, positive threshold, meta classifier and used features need to be configured in the respective dictionaries found in this file.

## Other parameters
There are also other parameters in the `PipelineRunner`, which can be used to control the concurrency of the pipeline. One example would be the `PipelineRunner.NUMBER_OF_DOWNLOADERS` attribute, which defines how many downloader processes are started. All of these configuration attributes are explained in docstrings, so they are not explained in detail again here. There are some requirements on the interdependence of these values, but they are checked dynamically on execution, so the danger of misconfiguration is limited. These parameters can be controlled using environment variables.


## Results format
After the pipeline finished, the raw results are stored in the given output directory in a sub-directory, named according the already mentioned schema: `run_[logname]_[start_index]_[end_index]_[timestamp]`. This directory contains one file called `used_config.txt` containing the parameters of the pipeline execution. Then for each classifier a SQLite database can be found, which contains the positives of the classifier. For each positively classified certificate it contains the identifier, the classification score, the SANs, information on the verification and eventually some screenshot metadata. The SQL schema for this database can be found in `pipeline_components/pipeline_database.py`. The negatives of the classifier are also stored, but in csv files. For each classifier there is a directory prefixed with negatives containing the csv files. Each classified certificate has one line of information containing some but not all of the data also stored in the SQLite database.

### Results aggregation
There are two possible evaluations which can be performed on the pipeline results. One is a report for a classifier for a specific threshold, which will output the scores of the classifier (tpr, fpr, acc, ...) as well as the list of true positives and false positives. The second evaluation is a threshold evaluation, which iterates the same scores for the evaluated classifier in a given threshold range.

The evaluation with a given threshold is performed as following:
```
python -m evaluation.pipeline_results_evaluation -b path/to/pipeline/output -o path/to/output/directory -n classifier_name -t 0.998
```
The path to the pipeline output should be the directory which starts with `run_`, which is described above. Independent from the classifiers in this directory the evaluation will only be performed for one classifier, which must be indicated using the `-n` option. The threshold for the evaluation is given using the `-t` option.


The threshold evaluation for one classifier can be performed as following:
```
python -m evaluation.pipeline_results_evaluation -b path/to/pipeline/output -o path/to/output/directory -n classifier_name -e -r 0.9 1.0 0.001
```
Here the only difference is, that instead of `-t`, the threshold range for the evaluation is given using the `-r` option. The order of the values is start, end, step size. Additionally the `-e` flag indicates, that a threshold evaluation should be performed.

Finally there is the option to filter the true positives as they are coming from the pipeline results against a list of known benign hostnames. This option is disabled on default, but can be turned on in both commands by adding the `-fp` flag to the command.

