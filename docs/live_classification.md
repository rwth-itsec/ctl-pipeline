# Live classification
To perform live classification of new certificates in a certificate transparency log the `live_runner.py` can be used as following:

```
python live_runner.py -l XENON2021 -o path/to/output/directory -c demo_classifier
```

This command will classify all the new certificates coming into the log over time. To also take screenshots of the hostnames/root pages, the `-s` option can be used.

- `-l` Name of the log to run the live classification on.
- `-o` Path to a directory in which the output directory should be created.
- `-c` Name(s) of the classifiers to use.
- `-s` Flag indicating, that screenshots should be taken.
