# Open Source Intelligence downloader
The directory `osint_downloader` contains the code used to download URL from open source intelligence feeds and the update routine for the Google Safe Browsing hash prefixes.

There are two scripts to perform these updates, they create a SQLite database each, which can later be used in the verification process.

Before the scripts can be executed API credentials need to be stored in the `credentials.py` file, like explained in the Readme.

## URL download
The malicious URLs downloaded from the OSINT feeds are stored in a SQLite database. The update of this database should be performed regularly, so it is advised to e.g. setup a hourly crontab to perform the update. The command to perform the update can be as simple as:

```
python -m osint_downloader.url_update
```

Running this command in the root directory of the project will perform an URL update and write all output files to a folder called `tmp`. The pathes at which the different output files should be stored can be adjusted. The options are:

- `-l` Path to a directory in which the logs should be stored.
- `-k` Path to a metadata cache file, which is e.g. used to remember the last update time.
- `-s` Path to a temporary directory in which downloaded files can be stored temporarily.
- `-d` Path to the database in which the malicious urls are kept. At the first execution this file will be created.

The `-h` option prints more help. Generally it is advised to move the database out of the `tmp` directory to not accidentally remove it.


## Google Safe Browsing hash update
To update the local Google Safe Browsing hash prefix database the `osint_downloader/google_safe_browsing_update.py` script can be used. The basic usage is:

```
python -m osint_downloader.google_safe_browsing_update
```

Using this command without any more arguments will also store the results in the `tmp` directory. There are two available arguments:

- `-l` Path to a directory in which the log file should be stored.
- `-d` Path to the hash prefix database.

The `-h` option can be used to print more help.
