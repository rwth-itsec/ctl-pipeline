# Datasets
To create datasets the `util/create_datasets.py` script can be used. This script can be used to create datasets from folders containing downloaded certificates. The resulting datasets can be used to train classifiers.

The script can be used to make certificates unique on a issuer and serial number level, filter a directory of certificates against a hostname blacklist and to create and combine datasets with balancing.

Generally the script is ran from the root directory of the pipeline, e.g. to print the build in help the following command can be used.
```
python -m util.create_datasets -h
python -m util.create_datasets unique -h
python -m util.create_datasets filter -h
python -m util.create_datasets create -h
python -m util.create_datasets combine -h
```

More information can be found in the docstrings in the script itself.

## Unique
The first step of the dataset creation, after the download, should be to make the certificates unique. The script can be used to make all certificate files in a folder unique.

```
python -m util.create_datasets -t path/to/output/directory -m path/to/malicious/certificates unique
```

Here the `-t` option indicates the output directory, which contains the unique certificates at the end. The input certificates can be given via the `-m` option for malicious certificates and the `-b` option for benign certificates. The main difference being, that malicious certificates are PEM files, containing the source phishing URL in the first line, while benign certificates are JSON dumps of the certificate transparency log containing the certificate in an encoded manner.

## Filter
As second step filtering of certificates can be performed using a hostname blacklist. The script will filter all certificates, which have at least one SAN on the blacklist. For malicious and benign certificates the same logic applies.

```
python -m util.create_datasets -t path/to/output/directory -m path/to/malicious/certificates filter -f path/to/benign_hostnames
```

The above command is an example for the malicious case, where the blacklist contains benign hostnames, which can be wildcards. The benign dataset creation is equivalent with `-b` instead of `-m`.

## Create
To create a balanced dataset first two datasets with only one type of certificates (benign or malicious) need to be created.

```
python -m util.create_datasets -t path/to/output/directory -b path/to/benign/certificates -n benign_dataset_demo create
```

The result of this command will be a benign dataset called `benign_dataset_demo.pkl` in the output directory. The malicious dataset creation is equivalent with `-m` instead of `-b`.

## Combine
Finally the two individual datasets can be combined to a balanced dataset for classifier training. There are two options to define the balancing, either as ratio of malicious certificates or as absolute certificate counts. To use a malicious ratio the `-r float` argument can be used. Here 0.5 would result in a 50/50 balanced dataset and 1 in a dataset containing only malicious certificates. The flag to define counts is `-c benign malicious`. These two parameters are mutually exclusive.

```
python -m util.create_datasets -t path/to/output/directory -m path/to/malicious/dataset.pkl -b path/to/benign/dataset.pkl -n combined_dataset_demo combine -r 0.5
```

The above example call will create a 50/50 balanced dataset from the two input datasets.
