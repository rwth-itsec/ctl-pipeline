# Classifier training
To train a classifier the `util/classifier_training.py` script can be used. The only requirement for training is a dataset containing both benign and malicious samples.

```
python -m util.classifier_training -d path/to/dataset.pkl -o path/to/output/directory -f feature_set_name -c classifier_model_name
```

The training set can be given to the script using the `-d` option. The output directory where the classifier and test results will be written to needs to be indicated using the `-o` option. The used feature set must be chosen using the `-f` option. Here the possible values are `all` and `selection`, which refer to the feature sets as presented in the paper. Finally a classifier model needs to be selected using the `-c` option, the possible values are `random_forest`, `lstm` and `resnet`.

In the output directory the classifier will be stored as `.pkl` file in case of random forests and in a directory for all tensorflow based classifiers. Additionally there will be a directory called `results`, which contains a threshold analysis and ROC curves.
