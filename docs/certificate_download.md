# Certificate download

## Benign
Benign certificates can be downloaded from certificate transparency logs. The idea is, to download certificates from a range in the certificate transparency log and then remove malicious certificates using the OSINT data from the `osint_downloader` or other sources.

To download certificates from the certificate transparency logs the `certificate_downloader.py` can be used as following:
```
python -m util.certificate_downloader -u path/to/url/database -p path/to/hash/database -c 10000 -o path/to/output -s 1.8.2020 -e 7.8.2020 -l XENON2020
```

The command line options explained in more detail:
- `-u` Path to an URL database created by the `osint_downloader` or the custom database as explained in the Readme.
- `-p` Path to a Google Safe Browsing database as created by the `osint_downloader` or a custom database for a different implementation.
- `-c` The number of certificates to download. This is only a target value, there may be more or less certificates at the end.
- `-o` Path to an output directory in which to store the certificates.
- `-s` Start date, certificates in the download set are valid only after this date.
- `-e` End date, certificates in the download set are valid only before this date.
- `-l` A list of logs to download certificates from.
- `-fd` Path to a malicious `DataSet` used for filtering.
- `-fh` Path to a list of malicious hostnames to filter.
- `-t` Number of parallel downloaders to use (default = 2).


### Certificate transparency log search tool
A small helper tool found in `util/ctl_search.py` can be used to find the leaf index of a certificate in a certificate transparency log, which is close to a given date.

```
python -m util.ctl_search -l XENON2020 -d 1.8.2020
```

The name of the log is given using the `-l` option, the date in `%d.%m.%Y` format using the `-d` option.

## Malicious
There is currently no method implenented in this repository to download malicious certificates. One approach would be to regularly download the certificates from phishing URLs downloaded in the `osint_downloader`.