import os

def tmp_file_path(file_name):
    return os.path.join(os.path.abspath(os.curdir), 'tmp', file_name)
