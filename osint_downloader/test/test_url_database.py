import unittest
from datetime import datetime, timezone

from ..database import URLDatabase, ValidationError
from ..common import *

class TestURLDatabase(unittest.TestCase):

    def setUp(self):
        # In memory testing database
        self.database = URLDatabase(":memory:")

    def test_save_new_malicious_url(self):
        url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK)
        self.database.save_malicious_url(url)

        # Check the url record
        url_fetch_result = self.database.cursor().execute("SELECT * FROM urls WHERE url=?", (url.url,)).fetchone()
        self.assertIsNotNone(url_fetch_result)
        self.assertEqual(url_fetch_result['url'], url.url)
        self.assertEqual(url_fetch_result['occurrence_date'], url.date)
        self.assertEqual(url_fetch_result['source'], url.source.value)
        self.assertEqual(url_fetch_result['type'], url.type.value)

        hostname_fetch_result = self.database.cursor().execute("SELECT * FROM hostnames WHERE hostname=?", ('com.test',)).fetchone()
        self.assertIsNotNone(hostname_fetch_result)
        self.assertIsNotNone(hostname_fetch_result['id'])
        self.assertEqual(hostname_fetch_result['id'], url_fetch_result['hostname_id'])

    def test_save_malicious_url_as_id_based_update(self):
        url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK)
        self.database.save_malicious_url(url)
        first_url_fetch_result = self.database.cursor().execute("SELECT * FROM urls WHERE url=?", (url.url,)).fetchone()
        # Set the ids of the url and hostname records to allow for id based updates
        url._id = first_url_fetch_result['id']
        url._hostname_id = first_url_fetch_result['hostname_id']

        # Assert on the original url
        self.assertIsNotNone(first_url_fetch_result)
        self.assertEqual(first_url_fetch_result['url'], 'http://test.com/test/asdf')

        # Update and save the new url
        url.url = "http://test.com/test/asdf_new"
        self.database.save_malicious_url(url)

        # Load and assert on the new url to verify the update in the database
        second_url_fetch_result = self.database.cursor().execute("SELECT * FROM urls WHERE url=?", (url.url,)).fetchone()
        self.assertIsNotNone(second_url_fetch_result)
        self.assertEqual(second_url_fetch_result['url'], 'http://test.com/test/asdf_new')
        self.assertEqual(second_url_fetch_result['id'], first_url_fetch_result['id'])

    def test_save_malicious_url_with_existing_url_source_pair(self):
        url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK)
        self.database.save_malicious_url(url)
        first_url_fetch_result = self.database.cursor().execute("SELECT * FROM urls WHERE url=?", (url.url,)).fetchone()

        # Assert on the original url
        self.assertIsNotNone(first_url_fetch_result)
        self.assertEqual(first_url_fetch_result['score'], None)

        # Update and save the new url
        url.score = 20
        url.date = datetime(2020, 11, 30, 16, 0)
        self.database.save_malicious_url(url)

        # Load and assert on the new url to verify the new insertion in the database
        second_url_fetch_result = self.database.cursor().execute("SELECT * FROM urls WHERE url=?", (url.url,)).fetchall()
        self.assertIsNotNone(second_url_fetch_result)
        self.assertEqual(len(second_url_fetch_result), 2)
        self.assertEqual(second_url_fetch_result[0]['score'], None)
        self.assertEqual(second_url_fetch_result[1]['score'], 20)
        self.assertEqual(second_url_fetch_result[1]['occurrence_date'], datetime(2020, 11, 30, 16, 0).astimezone(timezone.utc).replace(tzinfo=None))
        self.assertNotEqual(second_url_fetch_result[0]['id'], second_url_fetch_result[1]['id'])
        self.assertEqual(second_url_fetch_result[0]['id'], first_url_fetch_result['id'])
        self.assertNotEqual(second_url_fetch_result[1]['id'], first_url_fetch_result['id'])

    def test_save_exact_existing_malicious_url(self):
        url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK)
        self.database.save_malicious_url(url)
        first_url_fetch_result = self.database.cursor().execute("SELECT * FROM urls WHERE url = ?", (url.url,)).fetchall()
        self.assertEqual(len(first_url_fetch_result), 1)

        # Assert on the original url
        self.assertIsNotNone(first_url_fetch_result[0])
        self.assertEqual(first_url_fetch_result[0]['score'], None)

        self.database.save_malicious_url(url)
        second_url_fetch_results = self.database.cursor().execute("SELECT * FROM urls WHERE url=?", (url.url,)).fetchall()
        self.assertEqual(len(second_url_fetch_results), 1)
        self.assertEqual(first_url_fetch_result[0], second_url_fetch_results[0])

    def test_save_exact_existing_malicious_url_with_score(self):
        url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK, score = 10)
        self.database.save_malicious_url(url)
        first_url_fetch_result = self.database.cursor().execute("SELECT * FROM urls WHERE url = ?", (url.url,)).fetchall()
        self.assertEqual(len(first_url_fetch_result), 1)

        # Assert on the original url
        self.assertIsNotNone(first_url_fetch_result[0])
        self.assertEqual(first_url_fetch_result[0]['score'], 10)

        self.database.save_malicious_url(url)
        second_url_fetch_results = self.database.cursor().execute("SELECT * FROM urls WHERE url=?", (url.url,)).fetchall()
        self.assertEqual(len(second_url_fetch_results), 1)
        self.assertEqual(first_url_fetch_result[0], second_url_fetch_results[0])


    def test_save_malicious_url_hostname_update(self):
        url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK)
        self.database.save_malicious_url(url)
        first_url_fetch_result = self.database.cursor().execute("SELECT * FROM urls WHERE url=?", (url.url,)).fetchone()
        # Set the ids of the url and hostname records to allow for id based updates
        url._id = first_url_fetch_result['id']
        url._hostname_id = first_url_fetch_result['hostname_id']

        # Assert on the original url
        self.assertIsNotNone(first_url_fetch_result)

        # Update and save the new url
        url.url = 'http://other.test.com/'
        self.database.save_malicious_url(url)

        # Load and assert on the new url to verify the update in the database
        second_url_fetch_result = self.database.cursor().execute("SELECT * FROM urls WHERE url=?", (url.url,)).fetchone()
        self.assertIsNotNone(second_url_fetch_result)
        self.assertEqual(second_url_fetch_result['url'], 'http://other.test.com/')
        self.assertEqual(second_url_fetch_result['id'], first_url_fetch_result['id'])
        self.assertNotEqual(second_url_fetch_result['hostname_id'], first_url_fetch_result['hostname_id'])

        hostname_fetch_result = self.database.cursor().execute("SELECT * FROM hostnames WHERE hostname=?", ('com.test.other',)).fetchone()
        self.assertIsNotNone(hostname_fetch_result)
        self.assertEqual(second_url_fetch_result['hostname_id'], hostname_fetch_result['id'])

    def test_url_dulpicate_insertion(self):
        url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK)
        self.database.save_malicious_url(url)

        # Slightly update the url. Important: Do not change url and source, do not add the known id
        # Possible scenario: The stream contains the same url twice at different times -> we want to store all the occurences
        url.date = datetime(2019, 11, 30, 19, 0)
        self.database.save_malicious_url(url)

        # There should be two url entries
        url_results = self.database.cursor().execute("SELECT * FROM urls WHERE url=?", ('http://test.com/test/asdf',)).fetchall()
        self.assertEqual(len(url_results), 2)

        # hostname record should not be duplicated
        hostname_results = self.database.cursor().execute("SELECT * FROM hostnames WHERE hostname=?", ('com.test',)).fetchall()
        self.assertEqual(len(hostname_results), 1)


    def test_fetch_malicious_url(self):
        url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK)
        other_url = MaliciousURL(url="http://other.test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK)
        self.database.save_malicious_url(url)
        self.database.save_malicious_url(other_url)

        fetched_url = self.database.fetch_malicious_url(url='http://test.com/test/asdf')
        self.assertIsNotNone(fetched_url)
        self.assertEqual(fetched_url.url, 'http://test.com/test/asdf')
        self.assertIsNotNone(fetched_url._id)
        self.assertIsNotNone(fetched_url._hostname_id)

        fetched_other_url = self.database.fetch_malicious_url(url='http://other.test.com/test/asdf')
        self.assertIsNotNone(fetched_other_url)
        self.assertEqual(fetched_other_url.url, 'http://other.test.com/test/asdf')
        self.assertIsNotNone(fetched_other_url._id)
        self.assertIsNotNone(fetched_other_url._hostname_id)

    def test_fetch_malicious_url_with_source(self):
        url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK)
        other_url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.OPEN_PHISH)
        self.database.save_malicious_url(url)
        self.database.save_malicious_url(other_url)

        fetched_url = self.database.fetch_malicious_url(url='http://test.com/test/asdf', source = MaliciousURLSource.PHISHTANK)
        self.assertIsNotNone(fetched_url)
        self.assertEqual(fetched_url.url, 'http://test.com/test/asdf')
        self.assertIsNotNone(fetched_url._id)
        self.assertIsNotNone(fetched_url._hostname_id)
        self.assertEqual(fetched_url.source, MaliciousURLSource.PHISHTANK)

        fetched_other_url = self.database.fetch_malicious_url(url='http://test.com/test/asdf', source = MaliciousURLSource.OPEN_PHISH)
        self.assertIsNotNone(fetched_other_url)
        self.assertEqual(fetched_other_url.url, 'http://test.com/test/asdf')
        self.assertIsNotNone(fetched_other_url._id)
        self.assertIsNotNone(fetched_other_url._hostname_id)
        self.assertEqual(fetched_other_url.source, MaliciousURLSource.OPEN_PHISH)

    def test_non_valid_malicious_urls(self):
        # Test different cases of invalid malicious urls
        url = MaliciousURL(url="http://test.com/test/asdf", date = datetime(2019, 11, 30, 16, 0), source = MaliciousURLSource.PHISHTANK)
        url._url = None
        self.assertRaises(ValidationError, self.database.save_malicious_url, url)   # No url
        url._url = ''
        self.assertRaises(ValidationError, self.database.save_malicious_url, url)   # Empty url
        url.url = 'http://test.com/test/asdf'
        url._source = None
        self.assertRaises(ValidationError, self.database.save_malicious_url, url)   # No source
        url.source = MaliciousURLSource.PHISHTANK
        url._type = None
        self.assertRaises(ValidationError, self.database.save_malicious_url, url)   # No type


    def test_fetch_hostnames_with_prefix(self):
        self.assertFalse(self.database.fetch_hostnames_with_prefix('com.test.example'))

        url = MaliciousURL(url="http://example.test.com/test/asdf", source = MaliciousURLSource.PHISHTANK)
        self.database.save_malicious_url(url)

        self.assertTrue(self.database.fetch_hostnames_with_prefix('com.test.example'))
        self.assertTrue(self.database.fetch_hostnames_with_prefix('com.test'))
        self.assertFalse(self.database.fetch_hostnames_with_prefix('com.test.*'))   # No * matching supported
        self.assertFalse(self.database.fetch_hostnames_with_prefix('com.example'))

    def test_fetch_hostnames(self):
        self.assertEqual(self.database.fetch_hostnames(), [])

        url = MaliciousURL(url="http://test.com/test/asdf", source = MaliciousURLSource.PHISHTANK)
        self.database.save_malicious_url(url)

        self.assertEqual(self.database.fetch_hostnames(), ['com.test'])


    def test_fetch_malicious_urls_for_hostname_prefix(self):
        url = MaliciousURL(url="http://test.com/test/asdf", source = MaliciousURLSource.PHISHTANK)
        other_url = MaliciousURL(url="http://other.test.com/test/asdf", source = MaliciousURLSource.OPEN_PHISH)
        non_match_url = MaliciousURL(url="http://other.example.com/test/asdf", source = MaliciousURLSource.PHISHSTAT_INFO)
        self.database.save_malicious_url(url)
        self.database.save_malicious_url(other_url)
        self.database.save_malicious_url(non_match_url)

        malicious_urls = self.database.fetch_malicious_urls_for_hostname_prefix('com.test')
        self.assertEqual(len(malicious_urls), 2)
        self.assertEqual(malicious_urls[0].source, MaliciousURLSource.PHISHTANK)
        self.assertEqual(malicious_urls[1].source, MaliciousURLSource.OPEN_PHISH)


if __name__ == '__main__':
    unittest.main()
