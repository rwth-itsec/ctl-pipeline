import unittest
from unittest.mock import Mock
from datetime import datetime, timezone

from ..database import HashPrefixDatabase
from ..common.safe_browsing_helper import SafeBrowsingHelper

class TestHashPrefixDatabase(unittest.TestCase):
    def setUp(self):
        # In memory testing database
        self.database = HashPrefixDatabase(":memory:")
        self.get_possible_permutations_backup = SafeBrowsingHelper.get_possible_permutations

    def tearDown(self):
        SafeBrowsingHelper.get_possible_permutations = self.get_possible_permutations_backup

    def test_insert_prefixes(self):
        self.database.insert_hash_prefixes([b'test', b'asdf'])

        stored_prefixes = self.database.cursor().execute("SELECT * FROM hash_prefixes").fetchall()
        self.assertIsNotNone(stored_prefixes)
        self.assertEqual(stored_prefixes[0]['prefix'], b'test')
        self.assertEqual(stored_prefixes[1]['prefix'], b'asdf')
        self.assertEqual(len(stored_prefixes), 2)

        self.assertIsNone(stored_prefixes[0]['deletion_date'])
        self.assertIsNone(stored_prefixes[1]['deletion_date'])


    def test_mark_hash_prefixes_as_removed(self):
        self.database.insert_hash_prefixes([b'asdf', b'test'])

        self.database.mark_hash_prefixes_as_removed([0])

        stored_prefixes = self.database.cursor().execute("SELECT * FROM hash_prefixes").fetchall()
        self.assertIsNotNone(stored_prefixes)
        self.assertEqual(stored_prefixes[0]['prefix'], b'asdf')
        self.assertIsNotNone(stored_prefixes[0]['deletion_date'])
        self.assertEqual(stored_prefixes[1]['prefix'], b'test')
        self.assertIsNone(stored_prefixes[1]['deletion_date'])

        self.database.mark_hash_prefixes_as_removed([0])
        stored_prefixes = self.database.cursor().execute("SELECT * FROM hash_prefixes").fetchall()
        self.assertIsNotNone(stored_prefixes)
        self.assertEqual(stored_prefixes[0]['prefix'], b'asdf')
        self.assertIsNotNone(stored_prefixes[0]['deletion_date'])
        self.assertEqual(stored_prefixes[1]['prefix'], b'test')
        self.assertIsNotNone(stored_prefixes[1]['deletion_date'])


    def test_mark_all_as_deleted(self):
        self.database.insert_hash_prefixes([b'asdf', b'test'])

        self.database.mark_all_as_deleted()

        stored_prefixes = self.database.cursor().execute("SELECT * FROM hash_prefixes").fetchall()
        self.assertIsNotNone(stored_prefixes)
        self.assertEqual(stored_prefixes[0]['prefix'], b'asdf')
        self.assertIsNotNone(stored_prefixes[0]['deletion_date'])
        self.assertEqual(stored_prefixes[1]['prefix'], b'test')
        self.assertIsNotNone(stored_prefixes[1]['deletion_date'])

    def test_update_client_state(self):
        state = self.database.fetch_client_state()
        self.assertIsNone(state)

        self.database.update_client_state("test")

        state = self.database.fetch_client_state()
        self.assertEqual(state, "test")

        self.database.update_client_state("test1")

        state = self.database.fetch_client_state()
        self.assertEqual(state, "test1")

    def test_check_hostname(self):
        self.assertFalse(self.database.check_hostname('test.com'))
        self.database.insert_hash_prefixes([b'4\x0e\xaa\x16'])
        self.assertTrue(self.database.check_hostname('test.com'))

    def test_check_hostname_has_active_prefix(self):
        self.assertFalse(self.database.check_hostname_has_active_prefix('test.com'))
        self.database.insert_hash_prefixes([b'4\x0e\xaa\x16'])
        self.assertTrue(self.database.check_hostname_has_active_prefix('test.com'))
        self.database.mark_all_as_deleted()
        self.assertFalse(self.database.check_hostname_has_active_prefix('test.com'))


    def test_check_hostname_permutations(self):
        SafeBrowsingHelper.get_possible_permutations = Mock()
        SafeBrowsingHelper.get_possible_permutations.return_value = []
        self.assertFalse(self.database.check_hostname('test.com'))
        SafeBrowsingHelper.get_possible_permutations.asser_called_once()

if __name__ == '__main__':
    unittest.main()
