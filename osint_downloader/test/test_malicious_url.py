import unittest
from datetime import datetime, timezone

from ..common import *

class TestMaliciousURL(unittest.TestCase):

    def setUp(self):
        self.subject = MaliciousURL(url = 'https://test.com/path', source = MaliciousURLSource.PHISHTANK, type = MaliciousURLType.PHISHING, date = datetime(2019, 11, 30, 16, 0))

    def test_initialization_with_valid_values(self):
        url = MaliciousURL(url = 'https://test.com/path', source = MaliciousURLSource.PHISHTANK, date = datetime(2019, 11, 30, 16, 0))
        self.assertEqual(url.url, 'https://test.com/path')
        self.assertEqual(url.database_hostname, 'com.test')
        self.assertEqual(url.hostname, 'test.com')
        self.assertEqual(url.date, datetime(2019, 11, 30, 16, 0).astimezone(timezone.utc).replace(tzinfo=None))
        self.assertEqual(url.source, MaliciousURLSource.PHISHTANK)
        self.assertEqual(url.type, MaliciousURLType.PHISHING)

    def test_initialization_with_string_enums(self):
        url = MaliciousURL(url = 'https://test.com/path', source = 'phishtank', type = 'phishing', date = datetime(2019, 11, 30, 16, 0))
        self.assertEqual(url.url, 'https://test.com/path')
        self.assertEqual(url.database_hostname, 'com.test')
        self.assertEqual(url.hostname, 'test.com')
        self.assertEqual(url.date, datetime(2019, 11, 30, 16, 0).astimezone(timezone.utc).replace(tzinfo=None))
        self.assertEqual(url.source, MaliciousURLSource.PHISHTANK)
        self.assertEqual(url.type, MaliciousURLType.PHISHING)

    def test_initialization_with_valid_database_values(self):
        url = MaliciousURL(url = 'https://test.com/path', source = MaliciousURLSource.PHISHTANK, occurrence_date = datetime(2019, 11, 30, 16, 0), id = 100, hostname_id = 99)
        self.assertEqual(url.url, 'https://test.com/path')
        self.assertEqual(url.database_hostname, 'com.test')
        self.assertEqual(url.hostname, 'test.com')
        self.assertEqual(url.date, datetime(2019, 11, 30, 16, 0).astimezone(timezone.utc).replace(tzinfo=None))
        self.assertEqual(url.source, MaliciousURLSource.PHISHTANK)
        self.assertEqual(url.type, MaliciousURLType.PHISHING)
        self.assertEqual(url._id, 100)
        self.assertEqual(url._hostname_id, 99)


    def test_initialization_with_ip_url(self):
        url = MaliciousURL(url = 'https://127.0.0.0/path', source = MaliciousURLSource.PHISHTANK, date = datetime(2019, 11, 30, 16, 0))
        self.assertEqual(url.hostname, '127.0.0.0')
        self.assertEqual(url.database_hostname, '127.0.0.0')

    def test_url_property(self):
        self.subject.url = 'http://OTHER.test.com/new_path'
        self.assertEqual(self.subject.url, 'http://OTHER.test.com/new_path')
        self.assertEqual(self.subject.database_hostname, 'com.test.other')
        self.assertEqual(self.subject.hostname, 'other.test.com')

        # Expect raised errors for urls containing IP addresses and/or missing a schema prefix
        with self.assertRaises(ValueError):
            self.subject.url = 'test.com/path'
        with self.assertRaises(ValueError):
            self.subject.url = '127.0.0.0/path'

        self.subject.url = 'https://127.0.0.0/path'
        self.assertEqual(self.subject.url, 'https://127.0.0.0/path')
        self.assertEqual(self.subject.hostname, '127.0.0.0')
        self.assertEqual(self.subject.database_hostname, '127.0.0.0')

    def test_date_property(self):
        self.subject.date = datetime(2020, 11, 30, 16, 0)
        self.assertEqual(self.subject.date, datetime(2020, 11, 30, 16, 0).astimezone(timezone.utc).replace(tzinfo=None))

    def test_source_property(self):
        self.subject.source = MaliciousURLSource.PHISHSTAT_INFO
        self.assertEqual(self.subject.source, MaliciousURLSource.PHISHSTAT_INFO)

        self.subject.source = 'phishtank'
        self.assertEqual(self.subject.source, MaliciousURLSource.PHISHTANK)

    def test_type_property(self):
        self.subject.type = MaliciousURLType.PHISHING
        self.assertEqual(self.subject.type, MaliciousURLType.PHISHING)

        self.subject.type = 'phishing'
        self.assertEqual(self.subject.type, MaliciousURLType.PHISHING)

    def test_score_property(self):
        self.subject.score = 10.5
        self.assertEqual(self.subject.score, 10)

        self.subject.score = '11.9'
        self.assertEqual(self.subject.score, 11)

        with self.assertRaises(ValueError):
            self.subject.score = 'test'

if __name__ == '__main__':
    unittest.main()
