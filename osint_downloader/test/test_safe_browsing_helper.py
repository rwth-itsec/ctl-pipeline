import unittest
from unittest.mock import Mock

from ..common.safe_browsing_helper import SafeBrowsingHelper

class TestSafeBrowsingHelper(unittest.TestCase):
    def setUp(self):
        pass

    def test_get_possible_permutations(self):
        self.assertEqual(SafeBrowsingHelper.get_possible_permutations(None), [])
        self.assertEqual(SafeBrowsingHelper.get_possible_permutations('example-test.com'), ['example-test.com/'])
        self.assertEqual(SafeBrowsingHelper.get_possible_permutations('test.example-test.com'), ['test.example-test.com/', 'example-test.com/'])
        self.assertEqual(SafeBrowsingHelper.get_possible_permutations('127.0.0.1'), ['127.0.0.1/'])
        self.assertEqual(SafeBrowsingHelper.get_possible_permutations('300.0.0.1'), ['300.0.0.1/', '0.0.1/', '0.1/'])
        self.assertEqual(SafeBrowsingHelper.get_possible_permutations('*.test.com'), ['test.com/'])
        self.assertEqual(SafeBrowsingHelper.get_possible_permutations('.test.com'), ['test.com/'])
        self.assertEqual(SafeBrowsingHelper.get_possible_permutations('..test.com.'), ['test.com/'])

if __name__ == '__main__':
    unittest.main()
