import unittest
from unittest.mock import Mock
import json

from ..google_safe_browsing_update import GoogleSafeBrowsingHashPrefixUpdater

class TestGoogleSafeBrowsingUpdate(unittest.TestCase):

    def setUp(self):
        self.sut = GoogleSafeBrowsingHashPrefixUpdater('client_id', 'key', database_path = ':memory:')
        self.sut.database = Mock()
        self.sut.database.fetch_client_state.return_value = 'client_state'
        self.sut._validate_update = Mock()
        self.sut._validate_update.return_value = True

    def _mock_fetch_as_full_update(self):
        self.sut.fetch_hash_prefix_update = Mock()
        with open('osint_downloader/test/mocks/full_update_response_mock.json', 'r') as file:
            self.sut.fetch_hash_prefix_update.return_value = json.load(file)

    def _mock_fetch_as_malware_full_update(self):
        self.sut.fetch_hash_prefix_update = Mock()
        with open('osint_downloader/test/mocks/malware_full_update_response_mock.json', 'r') as file:
            self.sut.fetch_hash_prefix_update.return_value = json.load(file)

    def _mock_fetch_as_partial_update(self):
        self.sut.fetch_hash_prefix_update = Mock()
        with open('osint_downloader/test/mocks/partial_update_response_mock.json', 'r') as file:
            self.sut.fetch_hash_prefix_update.return_value = json.load(file)

    def _mock_fetch_partial_then_full_update(self):
        self.sut.fetch_hash_prefix_update = Mock()
        with open('osint_downloader/test/mocks/partial_update_response_mock.json', 'r') as partial:
            with open('osint_downloader/test/mocks/full_update_response_mock.json', 'r') as full:
                self.sut.fetch_hash_prefix_update.side_effect = [json.load(partial), json.load(full)]

    def test_partial_update(self):
        self._mock_fetch_as_partial_update()

        self.sut.update()

        self.sut.database.mark_all_as_deleted.assert_not_called()
        self.sut.database.mark_hash_prefixes_as_removed.assert_called_with([0, 2, 4])
        self.sut.database.insert_hash_prefixes.assert_called_with([b'asdf', b'1234'])
        self.sut.database.update_client_state.assert_called_with('ChAIBRADGAEiAzAwMSiAEDABEAFGpqhd')
        self.sut.fetch_hash_prefix_update.assert_called_with('client_state')

    def test_full_update(self):
        self._mock_fetch_as_full_update()

        self.sut.update()

        self.sut.database.mark_all_as_deleted.assert_called_once()
        self.sut.database.mark_hash_prefixes_as_removed.assert_not_called()
        self.sut.database.insert_hash_prefixes.assert_called_with([b'asdf', b'1234'])
        self.sut.database.update_client_state.assert_called_with('ChAIBRADGAEiAzAwMSiAEDABEAFGpqhd')
        self.sut.fetch_hash_prefix_update.assert_called_with('client_state')

    def test_full_update_with_malware(self):
        self._mock_fetch_as_malware_full_update()

        self.sut.update()

        self.sut.database.mark_all_as_deleted.assert_not_called()
        self.sut.database.mark_hash_prefixes_as_removed.assert_not_called()
        self.sut.database.insert_hash_prefixes.assert_not_called()
        self.sut.database.update_client_state.assert_not_called()
        self.sut.fetch_hash_prefix_update.assert_called_with('client_state')

    def test_invlid_update(self):
        self.sut._validate_update.side_effect = [False, True]

        self._mock_fetch_partial_then_full_update()

        self.sut.update()

        self.sut.database.mark_all_as_deleted.assert_called_once()
        self.sut.database.mark_hash_prefixes_as_removed.assert_called_once()
        self.sut.database.insert_hash_prefixes.assert_called()
        self.sut.database.update_client_state.asser_called()
        # Two validation calls, one for the invalid update and one for the valid full update
        self.assertEqual(self.sut._validate_update.call_count, 2)


if __name__ == '__main__':
    unittest.main()
