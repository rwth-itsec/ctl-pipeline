import requests
from .source import Source, UpdateFailed
from ..common import *
from ..common.date_helper import parse_date
from ..common.logger import logger
from datetime import datetime
import time
from ..config import *
import json

class PhishStatsInfo(Source):
    PAGE_SIZE = 100
    MAX_PAGES_IN_ONE_UPDATE = 25

    def __init__(self):
        super(PhishStatsInfo, self).__init__()

    def load_urls(self, **filter_params):
        filter_date = None
        if 'not_before' in filter_params and isinstance(filter_params['not_before'], datetime):
            filter_date = filter_params['not_before']

        phishes = []
        break_out = False
        for page in range(PhishStatsInfo.MAX_PAGES_IN_ONE_UPDATE):
            for phish in self._load_phishes_page(page):
                if (filter_date is not None and parse_date(phish['date']) >= filter_date) or filter_date is None:
                    phishes.append(phish)
                else:
                    break_out = True
                    break   # Assumption: The phishes are sorted by date, newest first -> then return at the first older date
            if break_out:
                break
        if break_out == False and filter_date is not None:
            logger.warning("PhishStatsInfo.load_urls: The page limit was reached without exceeding the filtering date.")

        return self._filter_none(map(lambda phish: self._entry_to_malicious_url(phish), phishes))

    def _load_phishes_page(self, page):
        time.sleep(2)   # Slow down requests to prevent/slow down rate limitation...
        url = "https://phishstats.info:2096/api/phishing?_fields=url,date,score&_sort=-date&_size=%d&_p=%d" % (PhishStatsInfo.PAGE_SIZE, page)

        try:
            response = requests.get(url, timeout = NETWORK_TIMEOUT)
            counter = 1
            while response.status_code == 503:
                time.sleep(counter * 10)    # Wait for rate limitation to expire...
                response = requests.get(url, timeout = NETWORK_TIMEOUT)
            return response.json()
        except requests.exceptions.RequestException as error:
            logger.error("PhishStatsInfo._load_phishes_page: Network error occured: %s" % (error))
            raise UpdateFailed
        except json.decoder.JSONDecodeError as error:
            logger.error("PhishStatsInfo._load_phishes_page: JSON parsing error occured: %s" % (error))
            raise UpdateFailed


    # USING THE CSV
    # The API seems to be more up to date than the csv file
    # This is why I decided to stick with using the API

    # def load_urls(self, filter_params = {}):
    #   print("Fetching phishes from PhishStats.info...")
    #   phish_csv_path = self.get_tmp_filepath("phish_score.csv")
    #   self.download_file("https://phishstats.info/phish_score.csv", phish_csv_path)
    #   parsed_phishes = self._parse_phish_csv(phish_csv_path)
    #   filter_date = None
    #   if 'not_before' in filter_params and isinstance(filter_params['not_before'], datetime):
    #       filter_date = filter_params['not_before']
    #       parsed_phishes = list(filter(lambda phish: phish['date'] >= filter_date, parsed_phishes))
    #   return self._filter_none(map(lambda phish: self._entry_to_malicious_url(phish), parsed_phishes))

    # def _parse_phish_csv(self, file_path):
    #   phishes = []
    #   with open(file_path, 'r') as file:
    #       reader = csv.reader(file)
    #       for row in reader:
    #           if len(row) >= 1 and not row[0].startswith('#'):
    #               phishes.append({ 'date': parse_date(row[0]).replace(tzinfo=timezone.utc).astimezone(timezone.utc), 'score': row[1], 'url': row[2] })
    #   return phishes

    def _entry_to_malicious_url(self, entry):
        try:
            return MaliciousURL(url = entry['url'], date = parse_date(entry['date']), score = entry['score'], source = MaliciousURLSource.PHISHSTAT_INFO)
        except ValueError:
            return None
