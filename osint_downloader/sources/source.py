import requests
from datetime import timedelta

from ..common.logger import logger
from ..config import Config, NETWORK_TIMEOUT
import os

class UpdateFailed(Exception):
    pass

class Source(object):
    """A parentclass for all the sources, which implements some useful functions."""

    @staticmethod
    def timeout():
        return timedelta(hours = 1)

    def get_cache_filepath(self, file_name):
        return os.path.join(Config.CACHE_FOLDER, file_name)

    def download_file(self, url, file_path_destination):
        try:
            with requests.get(url, stream=True, timeout = NETWORK_TIMEOUT) as response:
                response.raise_for_status()
                with open(file_path_destination, 'wb') as file:
                    for chunk in response.iter_content(chunk_size=8192):
                        if chunk: # filter out keep-alive new chunks
                            file.write(chunk)
        except (requests.exceptions.RequestException, IOError) as error:
            logger.error("Source.download_file: Failed to download file to path: %s" % (error))
            raise UpdateFailed

    def load_urls(self, **filter_params):
        """Loads the urls from the datasource.

            Filtering options:
            'not_before': datetime

            :param filter_params: A dict containing the filtering expressions.
            :returns: A list of `MaliciousURL`s
        """
        raise NotImplementedError # implement in source specific subclass

    def check_url(self, url):
        """Check an individual url for being malicious or benign.

            :returns: `MaliciousURL` if the url history indicates on malicious behavior, else `None`
        """
        raise NotImplementedError # implement in source specific subclass

    def _filter_none(self, malicious_urls):
        return list(filter(lambda obj: obj is not None, malicious_urls))
