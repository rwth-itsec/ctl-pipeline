import json
import gzip
from .source import Source, UpdateFailed
from ..common import *
from ..common.date_helper import parse_date
from ..common.logger import logger
from datetime import datetime

class PhishTank(Source):

    def __init__(self, api_key):
        super(PhishTank, self).__init__()
        self.api_key = api_key

    def load_urls(self, **filter_params):
        verified_online_path = self.get_cache_filepath("online-valid.json.gz")
        self.download_file("http://data.phishtank.com/data/%s/online-valid.json.gz" % (self.api_key), verified_online_path)
        parsed_json_phishes = self._decompress_verified_online(verified_online_path)
        if 'not_before' in filter_params and isinstance(filter_params['not_before'], datetime):
            filter_date = filter_params['not_before']
            parsed_json_phishes = list(filter(lambda phish: parse_date(phish['submission_time']) >= filter_date, parsed_json_phishes))
        return self._filter_none(map(lambda phish: self._phish_tank_entry_to_malicious_url(phish), parsed_json_phishes))

    def _decompress_verified_online(self, file_path):
        try:
            with gzip.open(file_path, 'rb') as file:
                json_content = file.read().decode("utf-8")
                parsed = json.loads(json_content)
                return parsed
        except (gzip.BadGzipFile, IOError) as error:
            logger.error("PhishTank._decompress_verified_online: Decompression of gzip file failed: %s" % (error))
            raise UpdateFailed

    def _phish_tank_entry_to_malicious_url(self, phishtank_entry):
        try:
            return MaliciousURL(url = phishtank_entry['url'], date = parse_date(phishtank_entry['submission_time']), source = MaliciousURLSource.PHISHTANK)
        except ValueError:
            return None
