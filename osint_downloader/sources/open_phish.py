import datetime
from datetime import timedelta
from .source import Source, UpdateFailed
from ..common import *
from ..common.date_helper import parse_date
from ..common.logger import logger

class OpenPhish(Source):

    def __init__(self):
        super(OpenPhish, self).__init__()

    @staticmethod
    def timeout():
        return timedelta(hours = 12)

    def load_urls(self, **filter_params):
        open_phish_path = self.get_cache_filepath("open_phish.txt")
        self.download_file("https://openphish.com/feed.txt", open_phish_path)
        parsed_phishes = self._parse_phishes(open_phish_path)
        return self._filter_none(map(lambda phish: self._entry_to_malicious_url(phish), parsed_phishes))

    def _parse_phishes(self, file_path):
        try:
            with open(file_path, "r") as file:
                return [line.strip() for line in file]
        except IOError as error:
            logger.error("OpenPhish._parse_phishes: IOError occured: %s" % (error))
            raise UpdateFailed

    def _entry_to_malicious_url(self, entry):
        try:
            return MaliciousURL(url = entry, source = MaliciousURLSource.OPEN_PHISH)
        except ValueError:
            return None
