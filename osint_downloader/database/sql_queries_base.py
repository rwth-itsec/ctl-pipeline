from ..common import *

class DatabaseError(Exception):
   pass

class SQLQueriesBase(object):
    def __init__(self, database):
        self.database = database

    def dictionary(self, record):
        dictionary = {}
        for key in record.keys():
            dictionary[key] = record[key]
        return dictionary
