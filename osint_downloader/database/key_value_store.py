import os
import json
from ..common.date_helper import parse_date

class KVStore(object):
    """A simple key value store."""
    def __init__(self, file_path):
        super(KVStore, self).__init__()
        self.file_path = file_path
        self._store_hash = {}
        if os.path.exists(file_path):
            with open(file_path, 'r') as file:
                try:
                    self._store_hash = json.load(file)
                except json.decoder.JSONDecodeError:
                    pass

    def set(self, key, value):
        # Try to convert to date string if it is a datetime
        try:
            value = value.isoformat()
        except:
            pass

        self._store_hash[key] = value
        with open(self.file_path, 'w+') as file:
            json.dump(self._store_hash, file)

    def get(self, key):
        if key not in self._store_hash:
            return None

        value = self._store_hash[key]
        # Try to parse date in case it is a date string
        try:
            value = parse_date(value)
        except:
            pass
        return value
