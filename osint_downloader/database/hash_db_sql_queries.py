import sqlite3

from datetime import datetime, timezone
from ..common import *
from .sql_queries_base import DatabaseError, SQLQueriesBase

class HashSQLQueries(SQLQueriesBase):
    def create_hash_table(self):
        self.database.cursor().execute("""
            CREATE TABLE IF NOT EXISTS hash_prefixes(
                id integer PRIMARY KEY AUTOINCREMENT,
                prefix BLOB,
                insertion_date timestamp,
                deletion_date timestamp
            );
        """)

    def create_metadata_table(self):
        self.database.cursor().execute("""
            CREATE TABLE IF NOT EXISTS client_metadata(
                id integer PRIMARY KEY AUTOINCREMENT,
                client_state TEXT,
                updated_at timestamp
            );
        """)

    def create_prefix_index(self):
        self.database.cursor().execute("CREATE INDEX IF NOT EXISTS hash_prefix_index ON hash_prefixes(prefix)")

    def insert_hash_prefixes(self, prefixes):
        date = datetime.now(timezone.utc).replace(tzinfo=None)
        query_base = "INSERT INTO hash_prefixes(prefix, insertion_date, deletion_date) VALUES (?,?,?)"
        insertion_data = [(sqlite3.Binary(prefix), date, None) for prefix in prefixes]
        self.database.cursor().executemany(query_base, insertion_data)
        self.database.commit()

    def mark_hash_prefixes_as_removed(self, indices):
        date = datetime.now(timezone.utc).replace(tzinfo=None)
        query_base = "SELECT id FROM hash_prefixes WHERE deletion_date IS NULL ORDER BY prefix"
        cursor = self.database.cursor()
        prefixids_to_remove = cursor.execute(query_base).fetchall()
        prefixids_to_remove = [prefixids_to_remove[i]['id'] for i in indices]
        update_query_base = "UPDATE hash_prefixes SET deletion_date = ? WHERE id = ?"
        insertion_data = [(date, prefix_id) for prefix_id in prefixids_to_remove]
        cursor.executemany(update_query_base, insertion_data)
        self.database.commit()

    def mark_all_as_deleted(self):
        date = datetime.now(timezone.utc).replace(tzinfo=None)
        query_base = "UPDATE hash_prefixes SET deletion_date = ?"
        self.database.cursor().execute(query_base, (date,))
        self.database.commit()

    def update_client_state(self, new_client_state):
        date = datetime.now(timezone.utc).replace(tzinfo=None)
        query_base = "INSERT INTO client_metadata (id, client_state, updated_at) VALUES(1, ?, ?) ON CONFLICT(id) DO UPDATE SET client_state = excluded.client_state, updated_at = excluded.updated_at"
        self.database.cursor().execute(query_base, (new_client_state, date,))
        self.database.commit()

    def fetch_client_state(self):
        result = self.database.cursor().execute("SELECT client_state FROM client_metadata WHERE id = 1").fetchone()
        if result is None:
            return None
        return result["client_state"]

    def find_prefix(self, prefix):
        prefix_results = self.database.cursor().execute("SELECT * FROM hash_prefixes WHERE prefix = ?", (sqlite3.Binary(prefix),)).fetchall()
        if prefix_results is not None:
            return [self.dictionary(result) for result in prefix_results]
        return []

    def find_active_prefix(self, prefix):
        prefix_results = self.database.cursor().execute("SELECT * FROM hash_prefixes WHERE prefix = ? AND deletion_date is NULL", (sqlite3.Binary(prefix),)).fetchall()
        if prefix_results is not None:
            return [self.dictionary(result) for result in prefix_results]
        return []

    def fetch_all_current_hashes_sorted(self):
        query_base = "SELECT prefix FROM hash_prefixes WHERE deletion_date IS NULL ORDER BY prefix"
        hashes = self.database.cursor().execute(query_base).fetchall()
        return [h['prefix'] for h in hashes]
