import sqlite3

from ..common.logger import logger

class ValidationError(Exception):
    pass

class DatabaseBase(object):

    def __init__(self, database_file, uri = False, skip_schema_initialization = False):
        super(DatabaseBase, self).__init__()
        self.database_file = database_file
        self.uri = uri
        self._common_init_database(skip_schema_initialization)

    def cursor(self):
        try:
            return self.database_connection.cursor()
        except Error as error:
            raise error
        return None

    def commit(self):
        self.database_connection.commit()

    def close(self):
        self.database_connection.close()

    def init_database():
        pass

    def before_database_init(self):
        pass

    def _common_init_database(self, skip_schema_initialization):
        self.database_connection = sqlite3.connect(self.database_file, detect_types = sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES, uri = self.uri)
        self.database_connection.row_factory = sqlite3.Row
        self.before_database_init()

        if not skip_schema_initialization:
            self.init_database()

        logger.info("Initialized database at path: %s" % (self.database_file))

    def copy_from(self, other_database):
        other_database.database_connection.backup(self.database_connection)
        self.commit()
