import sqlite3

from ..common import *
from .sql_queries_base import DatabaseError, SQLQueriesBase

class URLSQLQueries(SQLQueriesBase):
    def create_hostname_table(self):
        self.database.cursor().execute("""
            CREATE TABLE IF NOT EXISTS hostnames(
                id integer PRIMARY KEY AUTOINCREMENT,
                hostname text
            );
        """)

    def create_url_table(self):
        self.database.cursor().execute("""
            CREATE TABLE IF NOT EXISTS urls(
                id integer PRIMARY KEY AUTOINCREMENT,
                url text,
                source text,
                occurrence_date timestamp,
                type text,
                score integer,
                hostname_id integer
            );
        """)

    def create_hostname_index(self):
        self.database.cursor().execute("CREATE INDEX IF NOT EXISTS reversed_hostname_index ON hostnames(hostname COLLATE NOCASE)")
        self.database.cursor().execute("CREATE INDEX IF NOT EXISTS hostname_id_index ON urls(hostname_id)")


    def save_malicious_url(self, malicious_url):
        # Create or fetch hostname id
        hostname_id, newly_created = self._fetch_or_create_hostname_record(malicious_url.database_hostname)
        malicious_url._hostname_id = hostname_id    # Update hostname_id in case the url (and hostname) changed

        if malicious_url._id is not None and malicious_url._id > 0:
            # Update the existing record identified by the id
            self._update_malicious_url_by_id(malicious_url)
        else:
            if newly_created:
                self._insert_malicious_url(malicious_url, hostname_id)
            else:
                # Skip identically existing urls
                existing_urls = self.database.cursor().execute("SELECT * FROM urls WHERE hostname_id = ? AND url = ? AND source = ? AND score IS ? AND type = ? AND occurrence_date = ?", (hostname_id, malicious_url.url, malicious_url.source.value, malicious_url.score, malicious_url.type.value, malicious_url.date)).fetchall()
                if existing_urls is None or len(existing_urls) == 0:
                    self._insert_malicious_url(malicious_url, hostname_id)

        self.database.commit()

    def fetch_malicious_url(self, url = None, source = None):
        # Try to stringify the source, if it is given as enumeration
        try:
            source = source.value
        except AttributeError:
            pass

        filter_params = { 'url': url, 'source': source }
        query_items = []
        query_string_items = []
        query_string = ""
        for key in filter_params.keys():
            if filter_params[key] is not None:
                query_string_items.append("%s = ?" % (key))
                query_items.append(filter_params[key])

        query_string = " WHERE " + " AND ".join(query_string_items)
        url_record = self.database.cursor().execute("SELECT * FROM urls JOIN hostnames ON hostnames.id = urls.hostname_id" + query_string, tuple(query_items)).fetchone()
        if url_record is not None:
            return MaliciousURL(**self.dictionary(url_record))
        return None

    def fetch_hostnames(self):
        records = self.database.cursor().execute("SELECT hostname FROM hostnames ORDER BY hostname").fetchall()
        return [record["hostname"] for record in records]

    def fetch_hostnames_with_prefix(self, hostname):
        records = self.database.cursor().execute("SELECT hostname FROM hostnames WHERE hostname LIKE ?", (hostname + '%',)).fetchall()
        return [record['hostname'] for record in records]

    def fetch_malicious_urls_for_hostname_prefix(self, hostname):
        records = self.database.cursor().execute("SELECT * FROM hostnames JOIN urls ON urls.hostname_id = hostnames.id WHERE hostnames.hostname LIKE ?", (hostname + '%',)).fetchall()
        return [MaliciousURL(**self.dictionary(record)) for record in records]


    def _insert_malicious_url(self, malicious_url, hostname_id):
        self.database.cursor().execute("INSERT INTO urls(url, source, occurrence_date, type, score, hostname_id) VALUES (?,?,?,?,?,?)", (malicious_url.url, malicious_url.source.value, malicious_url.date, malicious_url.type.value, malicious_url.score, hostname_id))
        self.database.commit()


    def _update_malicious_url_by_id(self, malicious_url):
        if malicious_url._id is None or malicious_url._id <= 0:
            raise DatabaseError("A valid id is needed to update a `MaliciousURL`.")

        self.database.cursor().execute("UPDATE urls SET url = ?, source = ?, occurrence_date = ?, type = ?, score = ?, hostname_id = ? WHERE id = ?", (malicious_url.url, malicious_url.source.value, malicious_url.date, malicious_url.type.value, malicious_url.score, malicious_url._hostname_id, malicious_url._id))
        self.database.commit()


    def _fetch_or_create_hostname_record(self, hostname):
        cursor = self.database.cursor()

        # Check whether there is a hostname record for the url already in the database
        id_result = cursor.execute("SELECT id FROM hostnames WHERE hostname=?", (hostname,)).fetchone()
        hostname_id = None
        created = True
        if id_result is not None:
            hostname_id = id_result["id"]
            created = False
        else:
            # Create the hostname record if it does not exist yet
            cursor.execute("INSERT INTO hostnames(hostname) VALUES (?)", (hostname,))
            hostname_id = cursor.lastrowid
            created = True

        return hostname_id, created
