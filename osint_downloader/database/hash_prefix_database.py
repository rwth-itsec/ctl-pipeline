import sqlite3
import hashlib

from .hash_db_sql_queries import HashSQLQueries
from ..common.logger import logger
from ..common.safe_browsing_helper import SafeBrowsingHelper
from .database_base import DatabaseBase, ValidationError

class HashPrefixDatabase(DatabaseBase):

    def before_database_init(self):
        self._queries = HashSQLQueries(self)

    def init_database(self):
        self._queries.create_hash_table()
        self._queries.create_metadata_table()
        self._queries.create_prefix_index()
        self.commit()

    def insert_hash_prefixes(self, prefixes):
        """Insert a list of hash prefixes into the database. The insertion date is the current date."""
        self._queries.insert_hash_prefixes(prefixes)

    def mark_hash_prefixes_as_removed(self, indices):
        """Mark the hash prefixes at the given indices as removed in the database."""
        self._queries.mark_hash_prefixes_as_removed(indices)

    def mark_all_as_deleted(self):
        self._queries.mark_all_as_deleted()

    def update_client_state(self, new_client_state):
        self._queries.update_client_state(new_client_state)

    def fetch_client_state(self):
        return self._queries.fetch_client_state()

    def find_prefix(self, prefix):
        return self._queries.find_prefix(prefix)

    def list_checksum(self):
        all_hashes = self._queries.fetch_all_current_hashes_sorted()
        joined = b''.join([bytes(h) for h in all_hashes])
        return hashlib.sha256(joined).digest()

    def check_hostname(self, hostname):
        permutations = SafeBrowsingHelper.get_possible_permutations(hostname)
        for candidate in permutations:
            prefix = hashlib.sha256(candidate.encode('utf-8')).digest()[0:4]
            if len(self._queries.find_prefix(prefix)) > 0:
                return True
        return False

    def check_hostname_has_active_prefix(self, hostname):
        permutations = SafeBrowsingHelper.get_possible_permutations(hostname)
        for candidate in permutations:
            prefix = hashlib.sha256(candidate.encode('utf-8')).digest()[0:4]
            if len(self._queries.find_active_prefix(prefix)) > 0:
                return True
        return False
