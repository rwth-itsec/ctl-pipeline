import sqlite3

from .url_db_sql_queries import URLSQLQueries
from ..common.logger import logger
from .database_base import DatabaseBase, ValidationError

class URLDatabase(DatabaseBase):

    def before_database_init(self):
        self._queries = URLSQLQueries(self)

    def init_database(self):
        self._queries.create_hostname_table()
        self._queries.create_url_table()
        self._queries.create_hostname_index()
        self.commit()

    def save_malicious_url(self, malicious_url):
        """Inserts a new `MaliciousURL` into the database.

        A `MaliciousURL` is identified by its id. If the id is set, the malicious url will be updated in the database.
        If the id is not set, but there is already a `MaliciousURL` with the same url and source, this record will not be updated but a new one will be made.
        If there is no hostname set, the hostname will automatically be created or found in the database (if it already exists). The hostname
        is only identified by the id and not the string representation.
        """
        if malicious_url is None or not malicious_url.valid():
            raise ValidationError("MaliciousURL is not valid.")
            return False

        self._queries.save_malicious_url(malicious_url)

    def fetch_malicious_url(self, url = None, source = None):
        """Fetch one `MaliciousURL` matching the given url and/or source."""
        return self._queries.fetch_malicious_url(url = url, source = source)

    def fetch_hostnames(self):
        """Fetch all hostnames from the database."""
        return self._queries.fetch_hostnames()

    def fetch_hostnames_with_prefix(self, hostname):
        """Get all hostnames with the given hostname prefix.

        :param hostname: A hostname prefix in database form. E.g. com.example. This may also be an IP address.
        :returns: All the hostnames with the given prefix.
        """
        return self._queries.fetch_hostnames_with_prefix(hostname)

    def fetch_malicious_urls_for_hostname_prefix(self, hostname):
        """Get malicious urls matching the given hostname grouped by the source.

        :param hostname: A hostname prefix in database form. E.g. com.example. This may also be an IP address.
        :returns: All the malicious urls matching the given prefix.
        """
        return self._queries.fetch_malicious_urls_for_hostname_prefix(hostname)
