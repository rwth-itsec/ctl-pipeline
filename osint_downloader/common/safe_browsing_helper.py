import ipaddress

class SafeBrowsingHelper:
    def get_possible_permutations(hostname):
        if hostname is None:
            return []

        # Clean up the hostname
        hostname = hostname.lower().strip()
        while hostname.startswith('.'):
            hostname = hostname[1:]
        while hostname.endswith('.'):
            hostname = hostname[:-1]
        while '..' in hostname:
            hostname = hostname.replace('..', '.')
        while '*.' in hostname:
            hostname = hostname.replace('*.', '')

        # Check if it is an IP address
        try:
            ip = ipaddress.ip_address(hostname)
            return [hostname + '/']
        except ValueError:
            pass

        # Generate permutations for all subdomains
        permutations = [hostname + '/']
        hostname_shortened = hostname
        while hostname_shortened.count('.') >= 2:
            # Remove the first subdomain component
            hostname_shortened = '.'.join(hostname_shortened.split('.')[1:])
            permutations.append(hostname_shortened + '/')

        return permutations
