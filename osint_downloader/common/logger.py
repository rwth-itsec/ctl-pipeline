import logging
import os
import sys
from datetime import datetime

logger = logging.getLogger('osint_downloader')
logger.disabled = True  # Disable the logger on default (setup_logging needs to be called first)

def setup_logging(log_directory, file_name):
    global logger
    logger.disabled = False
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s', datefmt='%Y.%m.%d %H:%M:%S')

    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(formatter)
    logger.addHandler(stdout_handler)

    if log_directory is not None:
        # Create logging directory if needed
        if not os.path.exists(log_directory):
            os.makedirs(log_directory)

        log_filepath = log_directory + "/" + file_name
        log_file_handler = logging.FileHandler(log_filepath)
        log_file_handler.setLevel(logging.DEBUG)
        log_file_handler.setFormatter(formatter)
        logger.addHandler(log_file_handler)

    # Exception hook to log all uncaught excpetions
    sys.excepthook = handle_exception


def handle_exception(exc_type, exc_value, exc_traceback):
    global logger
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
