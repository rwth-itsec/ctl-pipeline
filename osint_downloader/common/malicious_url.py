from enum import Enum
from datetime import datetime, timezone
from urllib.parse import urlparse
from .model import Model
import ipaddress
import math
from ..common.logger import logger
from ..common.hostname_helper import HostnameHelper

class MaliciousURLType(Enum):
    """An enumeration indicating the threat type of an URL."""
    PHISHING = "phishing"


class MaliciousURLSource(Enum):
    """An enumeration indicating a source of a URL."""
    PHISHTANK = "phishtank"
    OPEN_PHISH = "open_phish"
    PHISHSTAT_INFO = "phishstat_info"
    VIRUSTOTAL = "virustotal"
    GOOGLE_SAFE_BROWSING_LOOKUP = "google_safe_browsing_lookup"
    OTHER = "other"

class MaliciousURL(Model):
    """A class representing a malicious URL"""

    def __init__(self, **kwargs):
        super(MaliciousURL, self).__init__()

        self.url = kwargs.get('url')
        self.date = kwargs.get('date', kwargs.get('occurrence_date', datetime.now(timezone.utc)))
        self.source = kwargs.get('source')
        self.type = kwargs.get('type', MaliciousURLType.PHISHING)
        self.score = kwargs.get('score')

        # Do not modify these ids unless you know what you are doing
        self._id = kwargs.get('id')
        self._hostname_id = kwargs.get('hostname_id')

    #
    # Properties
    #
    @property
    def hostname(self):
        """The hostname of the url."""
        return self._hostname_from_url(self.url)

    @property
    def database_hostname(self):
        """The hostname of the url as it is stored in the database.

        Domain based hsotnames are inversed: www.example.com -> com.example.www
        IPs (v4 and v6) are returned as they are: 120.0.0.1 -> 127.0.0.1
        """
        return self._database_hostname_from_hostname(self.hostname)

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, new_url):
        self._assert_valid_url(new_url)
        self._url = new_url

    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, new_date):
        if new_date is not None:
            # Convert the date to UTC and then remove the timezone information
            self._date = new_date.astimezone(timezone.utc).replace(tzinfo=None)
        else:
            self._date = None

    @property
    def source(self):
        return self._source

    @source.setter
    def source(self, new_source):
        if isinstance(new_source, MaliciousURLSource):
            self._source = new_source
        else:
            self._source = MaliciousURLSource(new_source)

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, new_type):
        if isinstance(new_type, MaliciousURLType):
            self._type = new_type
        else:
            self._type = MaliciousURLType(new_type)

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, new_score):
        if new_score is not None:
            self._score = int(math.floor(float(new_score)))
        else:
            self._score = None

    #
    # Model validation
    #
    def valid(self):
        return self.is_not_empty(self.url) and self.is_not_none(self.date) and self.is_not_none(self.source) and self.is_not_none(self.type) and self.is_not_none(self.hostname)

    #
    # Value assertions
    #
    def _assert_valid_url(self, url):
        if self._hostname_from_url(url) is None:
            logger.error("MaliciousURL._assert_valid_url: Invalid URL: %s" % (url))
            raise ValueError("MaliciousURL: An invalid URL or an URL with an invalid hostname was set.")

    #
    # Hostname logic
    #
    def _hostname_from_url(self, url):
        if url is None:
            return None
        return urlparse(url).hostname

    def _database_hostname_from_hostname(self, hostname):
        return HostnameHelper.database_hostname_from_hostname(hostname)

    #
    # Other
    #
    def __str__(self):
        return "MaliciousURL: url = %s, source = %s, type = %s, score = %s" % (self.url, self.source, self.type, self.score)
