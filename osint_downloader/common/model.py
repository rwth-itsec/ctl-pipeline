class Model(object):

    # Validations
    def valid(self):
        raise NotImplementedError

    def is_not_none(self, attribute):
        return attribute is not None

    def is_not_empty(self, attribute):
        return self.is_not_none(attribute) and len(attribute.strip()) > 0