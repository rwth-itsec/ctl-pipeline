import ipaddress

class HostnameHelper:
    def database_hostname_from_hostname(hostname):
        if hostname is None:
            return None

        ip = None
        try:
            ip = ipaddress.ip_address(hostname)
        except ValueError:
            # If there is a ValueError, the hostname is not an IP -> return the inversion
            return '.'.join(list(reversed(hostname.split('.')))).lower()

        # There was no ValueError during IP address initialization -> the hostname is an IP address
        return hostname


    def remove_wildcards(hostname):
        while hostname.startswith('*.') or hostname.endswith('.*'):
            if hostname.startswith('*.'):
                hostname = hostname[2:]
            elif hostname.endswith('.*'):
                hostname = hostname[:-2]
        return hostname