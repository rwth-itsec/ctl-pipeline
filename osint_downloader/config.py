NETWORK_TIMEOUT = 10

class Config(object):
    LOG_DIRECTORY = None
    DATABASE_PATH = None
    KVSTORE_PATH = None
    CACHE_FOLDER = None