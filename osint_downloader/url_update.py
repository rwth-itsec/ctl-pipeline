from credentials import *
from .sources import *
from .common import *
from .database import *
from .path_helper import *
from .common.logger import logger, setup_logging
from .config import Config

import argparse
from datetime import datetime, timedelta, timezone

#
# Script to update the phish database.
#

class PhishDatabaseUpdater(object):
    def __init__(self):
        super(PhishDatabaseUpdater, self).__init__()
        self.kv_store = KVStore(Config.KVSTORE_PATH)
        self.phish_database = URLDatabase(Config.DATABASE_PATH)
        self.start_time = datetime.now(timezone.utc)

    def update_from_source(self, source):
        malicious_urls = []
        last_fetch_date = self.kv_store.get(type(source).__name__ + '.last_fetch_date')     # For rate limiting and performance
        last_update_date = self.kv_store.get(type(source).__name__ + '.last_update_date')   # For knowing which urls to filter out -> performance
        if last_update_date is None:
            last_update_date = self.start_time
        if last_fetch_date is None or (last_fetch_date <= self.start_time - (source.timeout() - timedelta(minutes = 5))):
            logger.info("Initialize phish update from source: %s" % (type(source).__name__))
            try:
                # Get all urls which are newer than 12 hours back from the last update date
                # The goal is to miss no (only very few) urls, which are detected as phishing in the retrospective
                # So they apprear with an older date in the stream, since they occured long ago, but were verified
                # only a short time ago.
                malicious_urls = source.load_urls(not_before = last_update_date - timedelta(hours = 24))
                self.kv_store.set(type(source).__name__ + '.last_update_date', self.start_time)
                logger.info("%s: Fetched %s url(s) to check for insertion." % (type(source).__name__, len(malicious_urls)))
            except UpdateFailed:
                pass    # Do not update the last update date in case of a failure
            self.kv_store.set(type(source).__name__ + '.last_fetch_date', self.start_time)
        return malicious_urls

    def update_phish_url_streams(self):
        malicious_urls = []
        malicious_urls.extend(self.update_from_source(PhishTank(PHISHTANK_API_KEY)))
        malicious_urls.extend(self.update_from_source(OpenPhish()))
        malicious_urls.extend(self.update_from_source(PhishStatsInfo()))

        logger.info("Total new url(s): %s" % (len(malicious_urls)))

        for url in malicious_urls:
            self.phish_database.save_malicious_url(url)

        logger.info("Stored urls in database.")



    def __del__(self):
        # When the object is deinitialized the database should be closed
        self.phish_database.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to update the malicious url database.')
    parser.add_argument('--log-directory', '-l', type=str, default=tmp_file_path('log/'), help="The directory in which to store the logs.")
    parser.add_argument('--kvstore-path', '-k', type=str, default=tmp_file_path('key_value_cache.json'), help="Path to a cache file in which process metadata is stored (*.json).")
    parser.add_argument('--sources-cache-directory', '-s', type=str, default=tmp_file_path('sources_cache/'), help="Path to a temporary cache directory used for file based stream downloads.")
    parser.add_argument('--database-path', '-d', type=str, default=tmp_file_path('urls.db'), help="Path at which the database of the malicious urls is kept (*.db).")
    arguments = parser.parse_args()

    Config.LOG_DIRECTORY = arguments.log_directory
    Config.KVSTORE_PATH = arguments.kvstore_path
    Config.DATABASE_PATH = arguments.database_path
    Config.CACHE_FOLDER = arguments.sources_cache_directory

    # Create folders if they do not exist
    if not os.path.exists(Config.CACHE_FOLDER):
        os.makedirs(Config.CACHE_FOLDER)

    if Config.LOG_DIRECTORY is None or Config.KVSTORE_PATH is None or Config.DATABASE_PATH is None and Config.CACHE_FOLDER is None:
        print("Argument initialization is missing values.")
        exit(1)

    setup_logging(Config.LOG_DIRECTORY, "update.log")
    logger.info('Start phish url update...')

    PhishDatabaseUpdater().update_phish_url_streams()
