from credentials import *
from .database import *
from .path_helper import *
from .common.logger import logger, setup_logging
from .config import *

import argparse
from base64 import b64decode
import requests

class GoogleSafeBrowsingHashPrefixUpdater(object):

    def __init__(self, client_id, api_key, database_path = tmp_file_path('hash_prefixes.db')):
        super(GoogleSafeBrowsingHashPrefixUpdater, self).__init__()
        self.client_id = client_id
        self.api_key = api_key
        self.database = HashPrefixDatabase(database_path)

    def update(self):
        client_state = self.database.fetch_client_state()

        valid = self._update_prefix_database(client_state)
        if not valid:
            logger.warning('The list update resulted in an invalid state.')
            logger.info('Performing full update...')
            full_update_valid = self._update_prefix_database(None)
            if not full_update_valid:
                logger.error('The full update was invalid again. Stopping updates in this run.')
        else:
            logger.info('The list update resulted in a valid state.')
            logger.info('Finished hash prefix update.')

    def _update_prefix_database(self, client_state):
        json_answer = self.fetch_hash_prefix_update(client_state)
        expected_checksum = None

        if 'listUpdateResponses' in json_answer:
            for update_response in json_answer['listUpdateResponses']:
                if update_response['threatType'] != 'SOCIAL_ENGINEERING' or update_response['threatEntryType'] != 'URL':
                    continue

                # In case of a full update mark all hashes as deleted
                if update_response['responseType'] == 'FULL_UPDATE':
                    self.database.mark_all_as_deleted()

                # update the database using the deltas provided
                # First the removals, then the additions (See docs: https://developers.google.com/safe-browsing/v4/local-databases#partial-updates)
                if 'removals' in update_response:
                    for removal in update_response['removals']:
                        removal_indices = removal['rawIndices']['indices']
                        self.database.mark_hash_prefixes_as_removed(removal_indices)
                        logger.info("Removed %s hash prefixes." % (len(removal_indices)))

                if 'additions' in update_response:
                    for addition in update_response['additions']:
                        prefix_size = addition['rawHashes']['prefixSize']
                        hash_prefixes_decoded = b64decode(addition['rawHashes']['rawHashes'])
                        new_prefixes = [hash_prefixes_decoded[i:i+prefix_size] for i in range(0, len(hash_prefixes_decoded), prefix_size)]
                        self.database.insert_hash_prefixes(new_prefixes)
                        logger.info("Inserted %s new hash prefixes." % (len(new_prefixes)))

                if 'newClientState'in update_response:
                    new_client_state = update_response['newClientState']
                    self.database.update_client_state(new_client_state)

                # Actually only the last checksum is checked, but there should only be one list update which is processed in the first place
                expected_checksum = b64decode(update_response['checksum']['sha256'])

        if expected_checksum is not None:
            return self._validate_update(expected_checksum)
        else:
            return True

    def fetch_hash_prefix_update(self, client_state):
        try:
            update_url = "https://safebrowsing.googleapis.com/v4/threatListUpdates:fetch?key=%s" % (self.api_key)
            body = {
                'client': { 'clientId': self.client_id, 'clientVersion': '0.1' },
                'listUpdateRequests': [{
                    'threatType': 'SOCIAL_ENGINEERING',
                    'platformType': 'WINDOWS',
                    'threatEntryType': 'URL',
                    'state': client_state,
                    'constraints': {
                        'supportedCompressions': ['RAW']
                    }
                }]
            }
            response = requests.post(update_url, json = body, headers = { 'Content-Type': 'application/json' }, timeout = NETWORK_TIMEOUT)
            if response.status_code != 200:
                logger.error("GoogleSafeBrowsingHashPrefixUpdater.fetch_hash_prefix_update: The GSB API returned status code: %s" %(response.status_code))
                return {}
            return response.json()
        except requests.exceptions.RequestException as error:
            logger.error("GoogleSafeBrowsingHashPrefixUpdater.fetch_hash_prefix_update: Network error occured: %s" % (error))
            raise UpdateFailed

    def _validate_update(self, expected_checksum):
        local_checksum = self.database.list_checksum()
        return local_checksum == expected_checksum


    def __del__(self):
        # When the object is deinitialized the database should be closed
        self.database.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to update the GoogleSafeBrowsing hash prefix database.')
    parser.add_argument('--log-directory', '-l', type=str, default=tmp_file_path('log/'), help='The directory in which to store the logs.')
    parser.add_argument('--database-path', '-d', type=str, default=tmp_file_path('hash_prefixes.db'), help='Path at which the database of the hash prefixes is kept (*.db).')
    arguments = parser.parse_args()

    setup_logging(arguments.log_directory, 'gsb_update.log')
    logger.info('Start hash prefix update...')

    GoogleSafeBrowsingHashPrefixUpdater(GOOGLE_SAFE_BROWSING_CLIENT_ID, GOOGLE_SAFE_BROWSING_API_KEY, database_path = arguments.database_path).update()
