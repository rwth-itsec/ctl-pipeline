import argparse
import pipeline_components
from sklearn.metrics import accuracy_score, confusion_matrix, roc_auc_score
import csv
import glob
import os
import math
from util.hostname_filter import HostnameFilter
from itertools import repeat
from enum import Enum

class FilteringMode(Enum):
    """A filtering mode for true positives.

    NONE: No filtering
    FP: Mark true positives, which are negatives as false positives.
    """
    NONE = 0
    FP = 1

    def file_name_suffix(self):
        if self == FilteringMode.NONE:
            return ''
        elif self == FilteringMode.FP:
            return '_to_fp'

def write_statistics_to_file(statistics, file_name):
    """"Write a standardized statistic line as csv file with table headers."""
    with open(file_name, "w+") as file:
        writer = csv.writer(file)
        writer.writerow(['ACC', 'AUC', 'TPR', 'FPR', 'TNR', 'FNR', '#TP', '#FP', '#TN', '#FN', 'SUM'])
        writer.writerow(statistics)

def write_results_to_file(results, file_name):
    """Write a file with a list of certificates. E.g. write all false positives to a file."""
    with open(file_name, "w+") as file:
        writer = csv.writer(file)
        writer.writerow(['certificate_identifier', 'classifier_score', 'sans'])
        for result in results:
            writer.writerow([f'{result.ctl.name}:{result.ctl_index}', result.classification_score] + [san.san for san in result.sans])

def threshold_prediction(prediction, threshold):
    return 1 if prediction >= threshold else 0

def filter_tps(tps, filter_mode):
    """Apply the filtering to a list of true positives.

    :param tps: A list of true positives (type: PipelineResult)
    :param filter_mode: The filtering mode to use
    :returns: A list of true positives and a list of false positives (together they are identical to the input true positives list)
    """
    if filter_mode == FilteringMode.NONE:
        return tps, []
    else:
        benign_hostnames = HostnameFilter('evaluation/benign_hostnames')
        new_tps = []
        new_fps = []
        for tp in tps:
            has_benign_san = False
            for san in tp.sans:
                if benign_hostnames.matches(san.san):
                    has_benign_san = True
                    break
            if has_benign_san:
                if filter_mode == FilteringMode.FP:
                    new_fps.append(tp)
            else:
                new_tps.append(tp)
        return new_tps, new_fps

def load_predictions_and_ground_truth(database_path, negatives_directory, filter_mode = None):
    """Load the predictions from the pipeline raw data together with the generated ground truth.

    :param database_path: Path to the positives database of the pipeline.
    :param negatives_directory: The directory in which the pipeline negatives csv files are stored.
    :param filter_mode: An optional filtering mode (default: None)
    """
    database = pipeline_components.PipelineDatabase(database_path)

    # Read positives from SQLite database
    print('Loading positives...')
    results = database.fetch_all()
    tps = [result for result in results if len(result.verification_hits) > 0 and result.is_positive]
    fps = [result for result in results if len(result.verification_hits) == 0 and result.is_positive]

    ground_truth = []
    predictions = []

    if filter_mode is not None:
        tps, new_fps = filter_tps(tps, filter_mode)
        fps.extend(new_fps)

    predictions += [tp.classification_score for tp in tps]
    ground_truth += [1 for _ in tps]
    predictions += [fp.classification_score for fp in fps]
    ground_truth += [0 for _ in fps]

    # Read negatives from CSV files
    print('Loading negatives...')
    for file_name in glob.glob(f'{negatives_directory}/*.csv'):
        print(f'Reading from file: {file_name}')
        with open(file_name, 'r') as file:
            reader = csv.reader(file, delimiter = ';')
            _ = next(reader) # Remove header line
            for components in reader:
                score = float(components[-2])
                hits_count = int(components[-1])
                if math.isnan(score):
                    print(f'Found a nan value in line: {line.strip()}')
                    continue
                predictions.append(score)
                if hits_count > 0:
                    ground_truth.append(1)
                elif hits_count == 0:
                    ground_truth.append(0)
    return predictions, ground_truth, tps, fps

def create_output_directory(output_directory, classifier_name):
    output_base_path = os.path.join(output_directory, 'evaluation_' + classifier_name)
    if not os.path.isdir(output_base_path):
        os.makedirs(output_base_path)
    return output_base_path

def calculate_scores(ground_truth, predictions):
    """ Calculate the scores from the ground truth and prediction

    :param ground_truth: The ground truth of the results.
    :param predictions: The classifier predictions of the results.
    :returns: #tps, #fps, #tns, #fns, tpr, tnr, fpr, fnr, accuracy, auc score
    """
    tp = 0
    fp = 0
    tn = 0
    fn = 0
    for prediction, truth in zip(predictions, ground_truth):
        if prediction == truth and truth == 0:
            tn += 1
        elif prediction == truth and truth == 1:
            tp += 1
        elif prediction == 1 and truth == 0:
            fp += 1
        elif prediction == 0 and truth == 1:
            fn += 1
        else:
            print(f'Not binary: {prediction}, {truth}')

    tpr = float(tp) / float(tp + fn)
    tnr = float(tn) / float(tn + fp)
    fpr = float(fp) / float(fp + tn)
    fnr = float(fn) / float(fn + tp)

    # Accuracy and auc scores
    accuracy = accuracy_score(ground_truth, predictions)
    auc_score = roc_auc_score(ground_truth, predictions)

    return tp, fp, tn, fn, tpr, tnr, fpr, fnr, accuracy, auc_score

def main_single_threshold(classifier_name, database_path, negatives_directory, output_directory, threshold, filter_mode):
    """Perform an evaluation with a single threshold.

    At the end of the evaluation the scores of the classifier (tps, fpr, accuracy, ...) will be written to a file in the output directory.
    The true positives and false positives will be written to individual filea in the output directory as well.

    :param classifier_name: Name if the classifier to evaluate
    :param database_path: The path to the positives database written by the pipeline.
    :param negatives_directory: The directory in which the pipeline negatives csv files are stored.
    :param output_directory: The directory in which to store the results.
    :param threshold: The positives threshold.
    :param filter_mode: The filtering mode to use for true positives.
    """
    predictions, ground_truth, tps, fps = load_predictions_and_ground_truth(database_path, negatives_directory, filter_mode=filter_mode)
    predictions = [threshold_prediction(score, threshold) for score in predictions]

    # Count TPs, FPs, TNs, FNs and calculate the rates
    print('Calculating rates and scores...')
    tp, fp, tn, fn, tpr, tnr, fpr, fnr, accuracy, auc_score = calculate_scores(ground_truth, predictions)

    # Write results
    print('Writing results...')
    output_base_path = create_output_directory(output_directory, classifier_name)
    write_statistics_to_file([accuracy, auc_score, tpr, fpr, tnr, fnr, tp, fp, tn, fn, tp + fp + tn + fn], os.path.join(output_base_path, 'summary.csv'))
    write_results_to_file(tps, os.path.join(output_base_path, 'tps.csv'))
    write_results_to_file(fps, os.path.join(output_base_path, 'fps.csv'))

    print('Done!')

def threshold_mapper(prediction, ground_truth, threshold):
    print(f'Calculating scores for threshold: {threshold}')
    predictions = [threshold_prediction(score, threshold) for score in prediction]
    return (threshold, *calculate_scores(ground_truth, predictions))

def frange(start, end, step_size, include_end = False):
    return (x / (1 / step_size) for x in range(int(start * (1 / step_size)), int(end * (1 / step_size) + int(include_end))))

def main_threshold_evaluation(classifier_name, database_path, negatives_directory, output_directory, analysis_range, filter_mode):
    """Perform a threshold evaluation over a given threshold range.

    At the end of the evaluation the scores of the classifier for the different thresholds will be written to a file in the output directory.

    :param classifier_name: Name if the classifier to evaluate
    :param database_path: The path to the positives database written by the pipeline.
    :param negatives_directory: The directory in which the pipeline negatives csv files are stored.
    :param output_directory: The directory in which to store the results.
    :param analysis_range: The range for the positives threshold.
    :param filter_mode: The filtering mode to use for true positives.
    """
    threshold_range = frange(0.9, 1, 0.001, include_end=True)
    if analysis_range is not None and len(analysis_range) == 3:
        threshold_range = frange(analysis_range[0], analysis_range[1], analysis_range[2], include_end=True)

    output_base_path = create_output_directory(output_directory, classifier_name)
    predictions, ground_truth, tps, fps = load_predictions_and_ground_truth(database_path, negatives_directory, filter_mode=filter_mode)
    with open(os.path.join(output_base_path, f'thresholds{filter_mode.file_name_suffix()}.csv'), "w+") as file:
        writer = csv.writer(file)
        writer.writerow(['threshold', '#TP', '#FP', '#TN', '#FN', 'TPR', 'FPR', 'TNR', 'FNR', 'ACC', 'AUC'])
        for threshold in threshold_range:
            threshold, tp, fp, tn, fn, tpr, tnr, fpr, fnr, accuracy, auc_score = threshold_mapper(predictions, ground_truth, threshold)
            writer.writerow([threshold, tp, fp, tn, fn, tpr, fpr, tnr, fnr, accuracy, auc_score])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--base-path', '-b', type=str, required=True, help="Path to the pipeline results directory.")
    parser.add_argument('--classifier-name', '-n', type=str, required=True, help="Name of the classifier to evaluate.")
    parser.add_argument('--output-directory', '-o', type=str, help="Path to the directory where the output should be written to.")

    threshold_group = parser.add_mutually_exclusive_group(required=True)
    threshold_group.add_argument('--threshold', '-t', type=float, help='Threshold from which the results will be valued as positive.')
    threshold_group.add_argument('--threshold-evaluation', '-e', action='store_true', help='Perform a threshold evaluation.')
    parser.add_argument('--range', '-r', nargs=3, metavar=('start', 'end', 'step_size'), type=float, help='The range to analyze in threshold analysis mode (-e option). (optional)')

    filtering_group = parser.add_mutually_exclusive_group()
    filtering_group.add_argument('--benign-fp', '-fp', action='store_true', help='If a hostname is detected as benign, it is marked as false positive.')
    filtering_group.add_argument('--benign-ignore', '-bi', action='store_true', help='Do not perform any filtering of benign hostnames. (default)')

    arguments = parser.parse_args()

    database_path = os.path.join(arguments.base_path, f'positives_{arguments.classifier_name}.db')
    negatives_directory = os.path.join(arguments.base_path, f'negatives_{arguments.classifier_name}')
    output_directory = arguments.output_directory or os.path.join(arguments.base_path, 'evaluation')

    filter_mode = FilteringMode.NONE
    if arguments.benign_fp:
        filter_mode = FilteringMode.FP
        print('Interpreting benign hostnames as false positives.')
    elif arguments.benign_ignore:
        filter_mode = FilteringMode.NONE

    if arguments.threshold is not None:
        if arguments.range is not None:
            print('[WARNING] The range (-r) argument is only used in the threshold analysis. It is ignored in this analysis.')
        main_single_threshold(arguments.classifier_name, database_path, negatives_directory, output_directory, arguments.threshold, filter_mode)
    elif arguments.threshold_evaluation:
        main_threshold_evaluation(arguments.classifier_name, database_path, negatives_directory, output_directory, arguments.range, filter_mode)
