from cryptography import x509
import pickle
import pandas as pd
import logging
import sklearn
import numpy as np
import csv
from sklearn.ensemble import ExtraTreesClassifier
from .feature_extractor import CertificateFeatures, CertificateSANFeatures
from . import classifier_training_helper
from enum import Enum

log = logging.getLogger('log')

class MetaClassifier(Enum):
    avg = 'avg'
    med = 'med'
    min = 'min'
    max = 'max'


class RFClassifier:

    def __init__(self):
        self.classifier = ExtraTreesClassifier(n_estimators=200, n_jobs=-1, bootstrap=True)
        self.classification_threshold = 0.925
        self.meta_classifier = MetaClassifier.avg
        self.feature_names = CertificateFeatures.feature_vector_column_names()
        self.used_features = self.feature_names[:]

    def fit(self, X_train, y_train):
        assert len(X_train) == len(y_train)

        log.info('Pre-processing %d certificates (%d Subject Alternative Names)', len(X_train),
                 sum([len(x.feature_vectors()) for x in X_train]))

        san_features = [y for x in X_train for y in x.feature_vectors()]
        san_features = pd.DataFrame(san_features, columns=self.feature_names)
        san_features = san_features.loc[:, self.used_features]

        labels = []
        for i in range(len(X_train)):
            labels += [y_train[i]] * len(X_train[i].feature_vectors())

        log.info('Start training on %d certificates (%d Subject Alternative Names)', len(X_train), len(san_features))
        self.classifier.fit(san_features, labels)

    def predict(self, X_test, y_test=None, log_confusion_matrix=False, plot_roc=False, filename="rf"):
        san_features = [y for x in X_test for y in x.feature_vectors()]
        san_features = pd.DataFrame(san_features, columns=self.feature_names)
        san_features = san_features.loc[:, self.used_features]

        log.info('Classifying %d certificates (%d Subject Alternative Names)', len(X_test), len(san_features))
        probs = self.classifier.predict_proba(san_features)
        probs = np.array([x[1] for x in probs])

        san_counter = 0
        cert_results = []
        for i in range(len(X_test)):
            cert_results.append(probs[san_counter:san_counter + len(X_test[i].feature_vectors())])
            san_counter += len(X_test[i].feature_vectors())

        if self.meta_classifier == MetaClassifier.avg:
            res = np.array([[np.average(x)] for x in cert_results])
        elif self.meta_classifier == MetaClassifier.med:
            res = np.array([[np.median(x)] for x in cert_results])
        elif self.meta_classifier == MetaClassifier.min:
            res = np.array([[np.min(x)] for x in cert_results])
        elif self.meta_classifier == MetaClassifier.max:
            res = np.array([[np.max(x)] for x in cert_results])
        else:
            log.error('Unknown MetaClassifier selected')
            exit(1)

        if y_test is not None:
            labels = []
            for i in range(len(X_test)):
                labels += [y_test[i]] * len(X_test[i].feature_vectors())

            acc = sklearn.metrics.accuracy_score(labels, probs > self.classification_threshold)
            auc = sklearn.metrics.roc_auc_score(labels, probs)
            log.info('Subject Alternative Names: ACC = %f AUC = %f', acc, auc)

            if log_confusion_matrix:
                confusion_matrix = sklearn.metrics.confusion_matrix(labels, probs > self.classification_threshold)
                log.info('Subject Alternative Names: Confusion matrix:\n' + str(confusion_matrix))

            acc = sklearn.metrics.accuracy_score(y_test, res > self.classification_threshold)
            auc = sklearn.metrics.roc_auc_score(y_test, res)
            log.info('Certificates_avg: ACC = %f AUC = %f', acc, auc)
            if log_confusion_matrix:
                confusion_matrix = sklearn.metrics.confusion_matrix(y_test, res > self.classification_threshold)
                log.info('Certificates_avg: Confusion matrix:\n' + str(confusion_matrix))

            if plot_roc:
                classifier_training_helper.plot_roc(labels, probs, y_test, cert_results, filename)
                classifier_training_helper.create_threshold_report(labels, probs, y_test, cert_results, filename)

            return res, acc, auc

        return res

        return res

    def load(self, path):
        log.info('Loading classifier')
        with open(path, "rb") as f:
            self.classifier = pickle.load(f)

    def serialize(self, path):
        log.info('Classifier serialized')
        with open(path, 'wb') as f:
            pickle.dump(self.classifier, f)
