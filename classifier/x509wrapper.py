from cryptography import x509
from cryptography.x509.oid import ExtensionOID
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec,rsa,dsa,ed25519, ed448
import re
import logging
from urllib.parse import urlparse
from pathlib import Path


"""
This (cryptography/x509 by /pyca/cryptography) is the recommended method of working with x509.

Problems:
    hazmat backend, can not change this though.
    (as of 2019/04/15: older version in official Fedora repos (2.3 vs 2.5))
"""

"""
Variables: OIDs
For faster typing
"""
oid_o = x509.OID_ORGANIZATION_NAME #O
oid_ou = x509.OID_ORGANIZATIONAL_UNIT_NAME #OU
oid_cn = x509.OID_COMMON_NAME #CN
oid_c = x509.OID_COUNTRY_NAME #C
oid_st = x509.OID_STATE_OR_PROVINCE_NAME #ST
oid_l = x509.OID_LOCALITY_NAME #L
oid_san = x509.OID_SUBJECT_ALTERNATIVE_NAME #SAN
oid_bc = x509.ObjectIdentifier("2.5.4.15") #Business Category


def subject_get_from_oid(cert, oid):
    """Returns subject field of cert correspoding to oid.

    Returns empty string if there is no such field/ the field is empty.
    Returns first entry if there are more than one.
    """
    #get_attributes_for_oid does not raise errors, but returns empty if not found
    retVal = cert.subject.get_attributes_for_oid(oid)
    return oid_sanitizer(retVal)

def issuer_get_from_oid(cert, oid):
    """Returns subject field of cert correspoding to oid.

    Returns empty string if there is no such field/ the field is empty.
    Returns first entry if there are more than one.
    """
    #get_attributes_for_oid does not raise errors, but returns empty if not found
    retVal = cert.issuer.get_attributes_for_oid(oid)
    return oid_sanitizer(retVal)

def get_serial_number(cert):
    return "{:x}".format(cert.serial_number)

def oid_sanitizer(field):
    """Helper function.
    """
    if len(field) == 1:
        return field[0].value
    elif len(field) == 0:
        return ""
    else:
        logging.info("More than one match for oid in certificate.")
        logging.info(field)
        return "\n".join([x.value for x in field])

def get_o(cert):
    return subject_get_from_oid(cert, oid_o)

def issuer_get_from_oid(cert, oid):
    #get_attributes_for_oid does not raise errors, but returns empty if not found
    retVal = cert.issuer.get_attributes_for_oid(oid)
    return oid_sanitizer(retVal)

def get_san(cert):
    # SAN is extension -> raises exception if not present
    try:
        retVal = cert.extensions.get_extension_for_oid(x509.OID_SUBJECT_ALTERNATIVE_NAME).value.get_values_for_type(x509.DNSName)
    except x509.ExtensionNotFound as err:
        logging.debug(str(err))
        logging.info("No SAN Extension found.")
        return []

    return retVal

def get_subject_length(cert):
    length = 0
    for attribute in cert.subject:
        #logging.debug(attribute.value)
        length = length + len(attribute.value)
    return length

def get_subject_dn_count(cert):
    dn_length = len(cert.subject) # number of dn in subject
    return dn_length

def get_extension_count(cert):
    return len(cert.extensions)

def valid_from(cert):
    return cert.not_valid_before

def valid_until(cert):
    return cert.not_valid_after

def get_url(cert_data):
    url = re.match(r"^.*$", cert_data, re.M).group(0)
    return url

def get_issuer(cert):
    return cert.issuer


def get_policies(cert):
    """Raises an Exception if no SANs defined
    """
    retValue = []
    for curPol in cert.extensions.get_extension_for_oid(x509.OID_CERTIFICATE_POLICIES).value: # [0].policy_identifier
        curPolId = curPol.policy_identifier
        curOid = curPolId.dotted_string
        retValue.append(curOid)

    return retValue

def get_key_algorithm(cert):
    if isinstance(cert.public_key(), rsa.RSAPublicKey):
        return "rsa"
    elif isinstance(cert.public_key(), ec.EllipticCurvePublicKey):
        return "ec"
    elif isinstance(cert.public_key(), dsa.DSAPublicKey):
        return "dsa"
    elif isinstance(cert.public_key(), ed25519.Ed25519PublicKey):
        return "ed25519"
    elif isinstance(cert.public_key(), ed448.Ed448PublicKey):
        return "ed448"
    else:
        logging.error("Unknown key type in certificate: {}.".format(cert))
        return "unknown"

def get_key_size(cert):
    assert isinstance(cert, x509.Certificate)
    if isinstance(cert.public_key(), ed25519.Ed25519PublicKey):
        return 32 * 8 # ed25519 uses 32 byte keys
    elif isinstance(cert.public_key(), ed448.Ed448PublicKey):
        return 57 * 8 # ed448 uses 57 byte keys
    else:
        return cert.public_key().key_size

def has_ocsp_in_aia(cert):
    try:
        aia = cert.extensions.get_extension_for_oid(x509.OID_AUTHORITY_INFORMATION_ACCESS)
        for val in aia.value:
            if val.access_method == x509.OID_OCSP:
                return True
    except Exception as err:
        return False
    return False

def has_cdp(cert):
    try:
        cdp = cert.extensions.get_extension_for_oid(x509.OID_CRL_DISTRIBUTION_POINTS)
        return True
    except Exception as err:
        return False
    return False

def help_make_reg(name):
    ret = re.sub(r'\.', '\\.', name)
    ret = re.sub(r'\*', r'[A-Za-z0-9-]*', ret)
    ret = "^" + ret + "$"
    return ret

def is_valid(cert_data,cert,url):
    """Return verification status of certificate from text representation of the connection.

    Returns false if openssl returned verification errors.
    Returns false if hostmatch does not match.
    Returns false if there are any problems in the verification process above
    (e.g. file does not include expected information).
    Returns true otherwise.
    """
    return get_valid_status(cert_data, cert, url) == "Verification: OK"

def get_valid_status(cert_data, cert, url):
    """Return valid status of certificate and url.

    Possible results:
    Verification error - something went wrong during verification
    Verification error: hostname does not match
    """

    curHostname = urlparse(url).hostname
    if not curHostname:
        logging.warning("Unable to extract hostname from url: {}.".format(url))
        return "Verification error"

    # Get verification status from raw openssl output
    try:
        valCheck = re.compile(r'^Verification.*',re.M) # Match whole lines starting with Verification (. does not match \n)
        matchObj = valCheck.search(cert_data) # Find first matching line
        if not matchObj: # Return false if there was no match (something went wrong, probably)
            logging.warning("No match in verification for url {}.".format(url))
            return "Verification error"
    except Exception as err:
        logging.warning("'{}' occured during verification matching for url {}.".format(err, url))
        return "Verification error"

    validString = matchObj.group(0)
    # Check if openssl thought the certificate is valid
    try:
        okCheck = re.compile(r'OK')
        matchObj = okCheck.search(validString)
        if not matchObj:
            logging.debug("Not valid")
            return validString
        #return True
    except Exception as err:
        logging.warning("'{}' occured during verification matching for url {}.".format(err, url))
        return "Verification error"

    # Perform hostname matching (openssl does not do this by default)
    for san in get_san(cert):
        curReg = help_make_reg(san)
        if re.search(curReg, curHostname):
            return validString
    currCn = subject_get_from_oid(cert,oid_cn)
    if currCn:
        curReg = help_make_reg(currCn)
        if re.search(curReg, curHostname):
            return validString

    # Return false if openssl said valid but hostname did not match
    return "Verification error: hostname does not match"

def is_ov(cert):
    return len(cert.subject.get_attributes_for_oid(oid_o)) > 0

def is_ev(cert):
    return len(cert.subject.get_attributes_for_oid(oid_bc)) > 0

def is_dv(cert):
    return not (is_ev(cert) or is_ov(cert))

def is_wildcard(cert):
    cn = subject_get_from_oid(cert,oid_cn)
    san = get_san(cert)
    if "*" in cn:
        return True
    for name in san:
        if "*" in name:
            return True
    return False

def sct_timestamps(certificate):
    try:
        scts = certificate.extensions.get_extension_for_oid(ExtensionOID.PRECERT_SIGNED_CERTIFICATE_TIMESTAMPS)
        return [sct.timestamp for sct in scts.value]
    except x509.extensions.ExtensionNotFound:
        return []

def earliest_sct_timestamp(certificate):
    try:
        return min(sct_timestamps(certificate))
    except ValueError:
        return None

def all_sans(certificate):
    sans = get_san(certificate)
    common_name = subject_get_from_oid(certificate, oid_cn)
    if common_name not in sans: sans.append(common_name)
    return sans

def get_cert_from_pem(pem_data):
    cert = x509.load_pem_x509_certificate(bytes(pem_data,'ascii'), default_backend())
    return cert

def get_cert_from_der(der_data):
    cert = x509.load_der_x509_certificate(bytes(der_data,'ascii'), default_backend())
    return cert

def get_cert_from_file_path(file_path):
    p = Path(file_path)
    file_content = p.open().read()
    cert_data = get_cert_from_pem(file_content)
    return cert_data
