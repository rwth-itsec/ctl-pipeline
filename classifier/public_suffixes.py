import os

def clean_domain_list(domain_list):
    """
    Cleans a given domain list from invalid domains and cleans each single domain in the list.
    :param domain_list:
    :param dga:
    :return:
    """

    domain_list = [d.strip().lower() for d in domain_list]
    domain_list = list(filter(None, domain_list))   # Remove all entries which are falsey
    return domain_list

class PublicSuffixes:
    """
    Represents the official public suffixes list maintained by Mozilla  https://publicsuffix.org/list/
    """

    data = None
    tlds = None
    exclude_rules = None

    def load():
        file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'public_suffixes')
        with open(file, encoding='utf-8') as f:
            PublicSuffixes.data = f.readlines()

        PublicSuffixes.data = clean_domain_list(PublicSuffixes.data)
        # Get the punycode IDNs
        xn = ['.' + s.split(' ')[1] for s in PublicSuffixes.data if s.startswith('// xn--')]
        # Remove comment lines and prefix each line with a dot
        PublicSuffixes.data = ['.' + s for s in PublicSuffixes.data if not s.startswith('/')]
        # If the line contained a wildcard restore it (it is now .*. but should be *.)
        PublicSuffixes.data = [s[1:] if s.startswith('.*') else s for s in PublicSuffixes.data]
        # Clean up list and add IDNs
        PublicSuffixes.data = clean_domain_list(PublicSuffixes.data) + xn

        # Separate the exclude rules from all matchers
        PublicSuffixes.exclude_rules = [s[2:] for s in PublicSuffixes.data if s.startswith('.!')]
        PublicSuffixes.data = [s for s in PublicSuffixes.data if not s.startswith('.!')]

        # Sort suffixes by number of subdomain parts (to be able to match the longest suffix faster)
        PublicSuffixes.data = list(sorted(PublicSuffixes.data, key=lambda suffix: suffix.count('.'), reverse=True))


    def get_valid_tlds():
        """Returns all the TLDs in the dataset in a list of the form: ['.de', '.example']"""
        if PublicSuffixes.data is None:
            PublicSuffixes.load()
        if PublicSuffixes.tlds is None:
            # Get only TLDs
            PublicSuffixes.tlds = [s for s in PublicSuffixes.data if len(s.split('.')) == 2]
            # Remove eventual wildcards
            PublicSuffixes.tlds = [s[1:] if s.startswith('*.') else s for s in PublicSuffixes.tlds]

        return PublicSuffixes.tlds

    def get_valid_public_suffixes():
        if PublicSuffixes.data is None:
            PublicSuffixes.load()
        return PublicSuffixes.data

    def split_public_suffix_and_registered_name(domain_dot_split):
        """Finds the largest matching public suffix
        :param domain_dot_split: The domain split at dots
        :return: public suffix free domain as dot split, public suffix
        """
        dot_split_without_wildcards = PublicSuffixes._remove_leading_wildscards_from_dot_split(domain_dot_split)
        dot_split_without_wildcards = PublicSuffixes._downcase(dot_split_without_wildcards)

        for public_suffix in PublicSuffixes.get_valid_public_suffixes():
            domain_parts, suffix = PublicSuffixes._match(dot_split_without_wildcards, public_suffix)
            if domain_parts is not None and suffix is not None:
                return tuple(domain_parts), suffix

        # There was no public suffix match, return the dot split with an empty suffix
        return tuple(domain_dot_split), ''


    def _remove_leading_wildscards_from_dot_split(dot_split):
        while len(dot_split) > 0 and dot_split[0] == '*':
            dot_split = dot_split[1:]
        return dot_split

    def _downcase(dot_split):
        return tuple(map(lambda element: element.lower(), dot_split))

    def _match(dot_split, public_suffix):
        public_suffix_without_wildcard = public_suffix[1:] if public_suffix.startswith('*') else public_suffix

        if ('.' + '.'.join(dot_split)).endswith(public_suffix_without_wildcard):
            # Get number of elements in the dot split, which belong to the public suffix
            elements_count = public_suffix.count('.')
            # If there is a wildcard, there is one more element which belongs to the public suffix
            if public_suffix != public_suffix_without_wildcard:
                elements_count += 1

            # The public suffix matches the whole dot split
            # Work around this problem taking the first part of the dot split as registered name
            if elements_count == len(dot_split):
                elements_count -= 1

            suffix_parts = dot_split[-elements_count:]

            # There is an exclude rule, which needs to be respected
            # Drag the excluded part into the registered domain
            if '.'.join(suffix_parts) in PublicSuffixes.exclude_rules:
                elements_count -= 1

            # Remove the public suffix parts from the dot split
            suffix_parts = dot_split[-elements_count:]
            registered_name_parts = dot_split[:-elements_count]

            return registered_name_parts, '.' + '.'.join(suffix_parts)
        return None, None