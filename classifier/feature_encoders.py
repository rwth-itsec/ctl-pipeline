class FeatureEncoderBase:
    def encode(self, value):
        """Returns the encoded representation of the feature."""
        raise NotImplemented

    def feature_names(self, base_name):
        """Returns a list of new feature column names for the encoded representation."""
        raise NotImplemented


class OneHotIntegerFeatureEncoder(FeatureEncoderBase):
    def __init__(self, number_of_entries, max_if_exceeding = True):
        """Initializer

        :param number_of_entries: The number of entries the vector should have (first entry is 0)
        :param max_if_exceeding: Indicating, whether to set value = number_of_entries - 1 if value >= number_of_entries
        """
        self.number_of_entries = number_of_entries
        self.max_if_exceeding = max_if_exceeding

    def encode(self, value):
        """Helper to one hot encode an integer.

        E.g. value = 2 and self.number_of_entries = 5 returns [0,0,1,0,0]
        :param value: The integer value to encode
        :returns: A list representing the one hot encoding
        """
        one_hot = [0] * self.number_of_entries
        if self.max_if_exceeding and value >= self.number_of_entries:
            value = self.number_of_entries - 1
        if value < len(one_hot) and value >= 0:
            one_hot[value] = 1
        return one_hot

    def feature_names(self, base_name):
        return [f'{base_name}_one_hot_{i}' for i in range(self.number_of_entries)]

