from tensorflow.keras.layers import Activation, add, Concatenate, Conv1D, Dense, Embedding, Flatten, Input, MaxPooling1D
from tensorflow.keras.layers import GlobalMaxPooling1D, LSTM, Bidirectional, Reshape
from tensorflow.keras.models import Model, Sequential
from .. import settings

def build_model(max_features=settings.max_features, slen=settings.maxlen, flen=settings.flen):
    paths = []
    subject_input = Input(shape=(slen,), name='subject_input')
    features_input = Input(shape=(flen,), name='features_input')

    features_output = Reshape((1, flen))(features_input)
    features_output = Bidirectional(LSTM(32))(features_output)

    paths.append(features_output)

    embedd = Embedding(input_dim=max_features, output_dim=128, input_length=slen)(subject_input)
    subject_output = LSTM(32)(embedd)

    paths.append(subject_output)

    con = Concatenate()(paths)
    con = Dense(10)(con)
    res = Dense(1, activation='sigmoid')(con)

    model = Model(inputs=[subject_input, features_input], outputs=res)

    model.summary()
    model.compile(loss='binary_crossentropy', optimizer='adam')

    return model
