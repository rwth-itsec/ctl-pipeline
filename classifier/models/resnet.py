from tensorflow.keras.layers import Activation, add, Concatenate, Conv1D, Dense, Embedding, Flatten, Input, MaxPooling1D
from tensorflow.keras.layers import GlobalMaxPooling1D, LSTM, Bidirectional, Reshape
from tensorflow.keras.models import Model, Sequential
from .. import settings

def residual(input_dim, filters, kernels):
    input = Input((None, input_dim))
    res = input
    if input_dim != filters:
        res = Conv1D(filters=filters, kernel_size=1, strides=1, padding="same")(input)

    out = Conv1D(filters=filters, kernel_size=kernels[0], strides=1, padding="same")(input)
    out = Activation("relu")(out)
    out = Conv1D(filters=filters, kernel_size=kernels[1], strides=1, padding="same")(out)
    out = add([res, out])

    return Model(inputs=input, outputs=out)


def build_model(max_features=settings.max_features, slen=settings.maxlen, flen=settings.flen):
    paths = []
    subject_input = Input(shape=(slen,), name='subject_input')
    features_input = Input(shape=(flen,), name='features_input')

    features_output = Reshape((1, flen))(features_input)
    features_output = residual(flen, 64, [6, 6])(features_output)
    features_output = Activation("relu")(features_output)
    features_output = residual(64, 64, [12, 12])(features_output)
    features_output = Activation("relu")(features_output)
    features_output = residual(64, 64, [12, 12])(features_output)
    features_output = Flatten()(features_output)

    paths.append(features_output)

    embedd = Embedding(input_dim=max_features, output_dim=128, input_length=slen)(subject_input)

    subject_output = residual(128, 256, [4, 4])(embedd)
    subject_output = Activation("relu")(subject_output)
    subject_output = residual(256, 128, [3, 3])(subject_output)
    subject_output = Activation("relu")(subject_output)
    subject_output = residual(128, 128, [2, 2])(subject_output)
    subject_output = Activation("relu")(subject_output)
    subject_output = Flatten()(subject_output)
    subject_output = Dense(64)(subject_output)

    paths.append(subject_output)

    con = Concatenate()(paths)
    con = Dense(10)(con)
    res = Dense(1, activation='sigmoid')(con)

    model = Model(inputs=[subject_input, features_input], outputs=res)

    model.summary()
    model.compile(loss='binary_crossentropy', optimizer='adam')

    return model
