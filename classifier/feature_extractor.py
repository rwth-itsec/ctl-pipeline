import scipy.stats
from sklearn.feature_extraction.text import CountVectorizer
import numpy
import re
import os
from collections import defaultdict, Counter, Iterable
from enum import Enum

from .public_suffixes import PublicSuffixes
from .multivalued_feature import MultivaluedFeature, NGramFeature, MaliciousTokenFeature, MaliciousKeywordsFeatures
from .hostname_validator import assert_valid_hostname, HostnameValidationError
import classifier.x509wrapper as xw

class CertificateIssuer(Enum):
    OTHER = 0
    LETS_ENCRYPT_AUTHORITY_X3 = 1
    CPANEL_INC_CERTIFICATION_AUTHORITY = 2
    RAPIDSSL_TLS_RSA_CA_G1 = 3
    COMODO_RSA_DOMAIN_VALIDATION_SECURE_SERVER_CA = 4
    COMODO_ECC_DOMAIN_VALIDATION_SECURE_SERVER_CA_2 = 5
    CLOUDFLARE_INC_ECC_CA2 = 6
    DIGICERT_SHA2_SECURE_SERVER_CA = 7
    GO_DADDY_SECURE_CERTIFICATE_AUTHORITY_G2 = 8
    GOOGLE_INTERNET_AUTHORITY_G3 = 9
    RAPIDSSL_RSA_CA_2018 = 10

    def name_mapping():
        return {
            "Let's Encrypt Authority X3": CertificateIssuer.LETS_ENCRYPT_AUTHORITY_X3,
            "cPanel, Inc. Certification Authority": CertificateIssuer.CPANEL_INC_CERTIFICATION_AUTHORITY,
            "RapidSSL TLS RSA CA G1": CertificateIssuer.RAPIDSSL_TLS_RSA_CA_G1,
            "COMODO RSA Domain Validation Secure Server CA": CertificateIssuer.COMODO_RSA_DOMAIN_VALIDATION_SECURE_SERVER_CA,
            "COMODO ECC Domain Validation Secure Server CA 2": CertificateIssuer.COMODO_ECC_DOMAIN_VALIDATION_SECURE_SERVER_CA_2,
            "CloudFlare Inc ECC CA-2": CertificateIssuer.CLOUDFLARE_INC_ECC_CA2,
            "DigiCert SHA2 Secure Server CA": CertificateIssuer.DIGICERT_SHA2_SECURE_SERVER_CA,
            "Go Daddy Secure Certificate Authority - G2": CertificateIssuer.GO_DADDY_SECURE_CERTIFICATE_AUTHORITY_G2,
            "Google Internet Authority G3": CertificateIssuer.GOOGLE_INTERNET_AUTHORITY_G3,
            "RapidSSL RSA CA 2018": CertificateIssuer.RAPIDSSL_RSA_CA_2018
        }

    def from_name(name):
        if name in CertificateIssuer.name_mapping():
            return CertificateIssuer.name_mapping()[name]
        return CertificateIssuer.OTHER

class CertificateAlgorithm(Enum):
    UNKNOWN = 0
    RSA = 1
    EC = 2
    DSA = 3
    ED25519 = 4
    ED448 = 5

    def name_mapping():
        return {
            "rsa": CertificateAlgorithm.RSA,
            "ec": CertificateAlgorithm.EC,
            "dsa": CertificateAlgorithm.DSA,
            "ed25519": CertificateAlgorithm.ED25519,
            "ed448": CertificateAlgorithm.ED448,
        }

    def from_identifier(name):
        if name in CertificateAlgorithm.name_mapping():
            return CertificateAlgorithm.name_mapping()[name]
        return CertificateAlgorithm.UNKNOWN

class FeatureExtractionError(Exception):
    pass

class CertificateFeatures:
    """All the feature names, for which extractor methods are defined. These are general and not dependent on a SAN."""
    general_features = ['is_ov', 'is_ev', 'is_dv', 'sub_has_c', 'sub_has_st', 'sub_has_l', 'sub_only_cn', 'sub_has_cn', 'sub_dn_count', 'sub_char_count', 'sub_ext_count', 'valid_period', 'policies_count', 'is_wildcard', 'has_ocsp', 'has_cdp', 'san_count', 'average_sd_count', 'san_tld_count', 'issuer', 'issuer_name']

    all_features = ['is_ov', 'is_ev', 'is_dv', 'sub_has_c', 'sub_has_st', 'sub_has_l', 'sub_only_cn', 'sub_has_cn', 'sub_dn_count', 'sub_char_count', 'sub_ext_count', 'valid_period', 'policies_count', 'is_wildcard', 'has_ocsp', 'has_cdp', 'san_count', 'average_sd_count', 'san_tld_count', 'key_algorithm', 'key_size', 'issuer', 'issuer_name']

    """Character frequencies of names in alexa top 1 million."""
    # Taken from some random top 1m file, we should run this again
    _alexa_top_one_million_frequencies = None

    feature_name_mappings = {}

    def __init__(self):
        super(CertificateFeatures, self).__init__()

    def get_alexa_top_one_million_frequencies():
        if CertificateFeatures._alexa_top_one_million_frequencies is None:
            with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'top_domains.csv'), 'r') as file:
                counts = None
                for line in file:
                    hostname = line.split(',')[-1].strip()
                    if counts is not None:
                        counts += numpy.array(CertificateSANFeatures._getWordFreq(hostname))
                    else:
                        counts = numpy.array(CertificateSANFeatures._getWordFreq(hostname))
                CertificateFeatures._alexa_top_one_million_frequencies = list(counts)
        return CertificateFeatures._alexa_top_one_million_frequencies


    def extract(certificate):
        """Extract features from given X.509 certificate.

        :param certificate: The X.509 certificate from which to extract the features.
        :returns: A list of `CertificateFeatures`, one for each SAN in the certificate.
        """
        features = CertificateFeatures()
        for feature in CertificateFeatures.all_features:
            getattr(features, '_extract_' + feature)(certificate)

        try :
            features.extracted_san_features = []
            for san in features._extract_all_names(certificate):
                assert_valid_hostname(san.lower())
                san_features = CertificateSANFeatures(features.all_names, san)
                for feature in CertificateSANFeatures.all_features:
                    getattr(san_features, '_extract_' + feature)(certificate)
                features.extracted_san_features.append(san_features)
        except HostnameValidationError as validationError:
            raise FeatureExtractionError(f'Certificate contains an invalid hostname as san or cn: {validationError}')

        return features

    def build_feature_vector(instance, features, encoders):
        feature_vector = []
        for feature_identifier in features:
            value = None
            if isinstance(feature_identifier, MultivaluedFeature):
                base_values = getattr(instance, feature_identifier.summary_name)
                value = feature_identifier.get_feature_value(base_values)
                feature_identifier = feature_identifier.sub_feature_name
            else:
                value = getattr(instance, feature_identifier)
            if value is None:
                raise AttributeError(f'The input feature {feature_identifier} cannot be extracted.')
            if feature_identifier in encoders:
                value = encoders[feature_identifier].encode(value)
            if isinstance(value, Iterable) and not isinstance(value, str):
                feature_vector.extend(value)
            else:
                feature_vector.append(value)
        return feature_vector

    def feature_vector_column_names(features = None, san_features = None, encoders = {}):
        """Get the column names for the feature vectors which result from your given configuration.

        When extracting a feature vector there are the options `features`, `san_features` and `encoders`. If you pass the same
        parameters to this method you will get a list of feature names, which are in the same order as the extracted features. Basically this method generated the table header for the feature vectors.

        This method respects all multi valued features by expanding the summary name to the individual names. Furthermore the
        encoders are asked to provide column name(s) for their encoding.

        :param features: List of features to get from the certificate.
        :param san_features: List of san features to get from the certificate.
        :param encoders: The used encoders.
        :returns: A list of strings representing the column names.
        """
        if features is None:
            features = CertificateFeatures.all_features
        if san_features is None:
            san_features = CertificateSANFeatures.all_features

        name_mapping = {**CertificateFeatures.feature_name_mappings, **CertificateSANFeatures.feature_name_mappings}

        feature_names = san_features + features
        all_names = []
        for identifier in feature_names:
            if isinstance(identifier, MultivaluedFeature):
                identifier = identifier.sub_feature_name
            if identifier in name_mapping:
                all_names.extend(name_mapping[identifier])
            else:
                if identifier in encoders:
                    all_names.extend(encoders[identifier].feature_names(identifier))
                else:
                    all_names.append(identifier)
        return all_names


    def feature_vectors(self, features = None, san_features = None, encoders = {}):
        """Get a feature vector from the certificate represented by the instance of the class.

        The features lists may also contain subclasses of the MultiValuedFeature class to get granular control over which
        features to get from multi valued features.

        :param features: A list of features to include in the vector. E.g. `CertificateFeatures.general_features` or `CertificateFeatures.all_features` (default: CertificateFeatures.all_features)
        :param san_features: A list of san features to include in the vector. E.g. `CertificateSANFeatures.san_features` or `CertificateSANFeatures.all_features` or `CertificateSANFeatures.fancy_features` (default: CertificateSANFeatures.all_features)
        :param encoders: A dict of feature encoders. The keys are the feature names to encode, the values are FeatureEncoderBase subclasses. E.g. for integer one hot encoding: { 'integer_feature_name': OneHotIntegerFeatureEncoder(5) }
        :returns: A feature vector as list
        """
        if features is None:
            features = CertificateFeatures.all_features

        if san_features is None:
            san_features = CertificateSANFeatures.all_features

        all_vectors = []
        base_feature_vector = CertificateFeatures.build_feature_vector(self, features, encoders)
        for san in self.extracted_san_features:
            all_vectors.append(san.feature_vector(features = san_features, encoders = encoders) + base_feature_vector)
        return all_vectors

    #
    # Decorator for feature extracting methods
    #
    def feature_extracting(function):
        """A decorator function, which should be used for all feature extracting functions.

        The decorator handles the caching of the extracted feature values in attributes on the instance of the current `CertificateFeatures` instance.
        The convention is, that each feature has a name (e.g. is_ov). For each feature a method prefixed with _extract_ should be implemented (e.g. _extract_is_ov).
        When the feature is requested the first time with a certificate it is extracted using the extractor function, the return value of this function is then cached
        as an attribute on the instance named after the feature. (e.g. self.is_ov). When the extractor method is now called again the cached value is returned.
        """
        def wrapped(self, certificate):
            attribute_name = function.__name__.replace('_extract_', '')
            # If the feature is already extracted, early exit by returning the value
            if hasattr(self, attribute_name):
                return getattr(self, attribute_name)
            assert certificate is not None, f'The feature {attribute_name} cannot be accessed before extraction.'
            # The feature gets extracted and will be set as attribute on the class
            try:
                value = function(self, certificate)
                setattr(self, attribute_name, value)
                return value
            except Exception as exception:
                raise FeatureExtractionError(f'An exception was raised while extracting {attribute_name} feature: ' + str(exception))
        return wrapped

    #
    # Methods to extract features
    #
    @feature_extracting
    def _extract_is_dv(self, certificate = None):
        if xw.is_dv(certificate):
            return 1
        return 0

    @feature_extracting
    def _extract_is_ov(self, certificate = None):
        if xw.is_ov(certificate):
            return 1
        return 0

    @feature_extracting
    def _extract_is_ev(self, certificate = None):
        if xw.is_ev(certificate):
            return 1
        return 0

    @feature_extracting
    def _extract_sub_has_c(self, certificate = None):
        if self._xw_subject_get_from_oid_with_cache(certificate, xw.oid_c):
            return 1
        return 0

    @feature_extracting
    def _extract_sub_has_st(self, certificate = None):
        if self._xw_subject_get_from_oid_with_cache(certificate, xw.oid_st):
            return 1
        return 0

    @feature_extracting
    def _extract_sub_has_l(self, certificate = None):
        if self._xw_subject_get_from_oid_with_cache(certificate, xw.oid_l):
            return 1
        return 0

    @feature_extracting
    def _extract_sub_only_cn(self, certificate = None):
        if self._extract_sub_dn_count(certificate) == 1 and self._xw_subject_get_from_oid_with_cache(certificate, xw.oid_cn):
            return 1
        return 0

    @feature_extracting
    def _extract_sub_has_cn(self, certificate = None):
        if self._xw_subject_get_from_oid_with_cache(certificate, xw.oid_cn):
            return 1
        return 0

    @feature_extracting
    def _extract_cn_in_san(self, certificate = None):
        if self._xw_subject_get_from_oid_with_cache(certificate, xw.oid_cn) in self._extract_sans(certificate):
            return 1
        return 0

    @feature_extracting
    def _extract_sub_dn_count(self, certificate = None):
        return xw.get_subject_dn_count(certificate)

    @feature_extracting
    def _extract_sub_char_count(self, certificate = None):
        return xw.get_subject_length(certificate)

    @feature_extracting
    def _extract_sub_ext_count(self, certificate = None):
        return xw.get_extension_count(certificate)

    @feature_extracting
    def _extract_policies_count(self, certificate = None):
        pol = []
        try:
            pol = xw.get_policies(certificate)
        except Exception as err:
            pass
        return len(pol)

    @feature_extracting
    def _extract_valid_period(self, certificate = None):
        return (xw.valid_until(certificate) - xw.valid_from(certificate)).days

    @feature_extracting
    def _extract_san_count(self, certificate = None):
        """SAN and CN"""
        return len(self._extract_all_names(certificate))

    @feature_extracting
    def _extract_average_sd_count(self, certificate = None):
        """Actually returns average number of components separated by dots in sans+cn"""
        total_count = self._extract_san_count(certificate)
        if total_count == 0:
            return 0
        sans = self._extract_all_names(certificate)
        total_sd = 0
        for name in sans:
            total_sd += len(name.split("."))
        return total_sd / self._extract_san_count(certificate)

    @feature_extracting
    def _extract_san_tld_count(self, certificate = None):
        sans = self._extract_all_names(certificate)
        tlds = []
        for name in sans:
            if name.count('.') > 0:
                tld = name.split(".")[-1]
                if tld not in tlds:
                    tlds.append(tld)
        return len(tlds)

    @feature_extracting
    def _extract_all_names(self, certificate = None):
        sans = self._extract_sans(certificate)[:]
        if (not self._extract_cn_in_san(certificate)) and self._extract_sub_has_cn(certificate):
            cn = self._xw_subject_get_from_oid_with_cache(certificate, xw.oid_cn)
            sans.append(cn)
        return sans

    @feature_extracting
    def _extract_sans(self, certificate = None):
        """Only SANs not containing CN"""
        return xw.get_san(certificate)

    @feature_extracting
    def _extract_is_wildcard(self, certificate = None):
        if xw.is_wildcard(certificate):
            return 1
        return 0

    @feature_extracting
    def _extract_has_ocsp(self, certificate = None):
        if xw.has_ocsp_in_aia(certificate):
            return 1
        return 0

    @feature_extracting
    def _extract_has_cdp(self, certificate = None):
        if xw.has_cdp(certificate):
            return 1
        return 0

    @feature_extracting
    def _extract_key_algorithm(self, certificate = None):
        algo = xw.get_key_algorithm(certificate)
        return CertificateFeatures._convert_key_algorithm(algo)

    def _convert_key_algorithm(algo):
        return CertificateAlgorithm.from_identifier(algo).value

    @feature_extracting
    def _extract_key_size(self, certificate = None):
        return xw.get_key_size(certificate)

    @feature_extracting
    def _extract_issuer(self, certificate = None):
        iss = xw.issuer_get_from_oid(certificate, xw.oid_cn)
        return CertificateFeatures._convert_issuer(iss)

    def _convert_issuer(issuer):
        return CertificateIssuer.from_name(issuer).value

    def _xw_subject_get_from_oid_with_cache(self, certificate, oid):
        if not hasattr(self, 'subject_get_from_oid_cache'):
            self.subject_get_from_oid_cache = {}

        if oid not in self.subject_get_from_oid_cache:
            self.subject_get_from_oid_cache[oid] = xw.subject_get_from_oid(certificate, oid)

        return self.subject_get_from_oid_cache[oid]

    @feature_extracting
    def _extract_issuer_name(self, certificate = None):
        return xw.issuer_get_from_oid(certificate, xw.oid_cn)


class CertificateSANFeatures:
    """All the feature names for SAN dependent features, for which extractor methods are defined."""
    san_features = ['san', 'sub_cn_entropy', 'sub_cn_is_com', 'name_san_entropy']
    all_san_features = ['san', 'sub_cn_entropy', 'sub_cn_is_com', 'name_san_entropy', 'has_uppercase_letters', 'num_dash', 'num_dash_rd', 'num_tokens', 'tld_in_token', 'https_in_domain', 'longest_token', 'special_char_ratio', 'is_ip', 'is_idn_domain', 'san_to_alexa_entropy']
    fancy_features = ['vowel_ratio', 'digit_ratio', 'length', 'contains_wwwdot', 'contains_subdomain_of_only_digits', 'subdomain_lengths_mean', 'parts', 'contains_digits', 'has_valid_tld', 'contains_one_char_subdomains', 'prefix_repetition', 'char_diversity', 'contains_tld_as_infix', 'alphabet_size', 'shannon_entropy', 'hex_part_ratio', 'underscore_ratio', 'ratio_of_repeated_chars', 'consecutive_consonant_ratio', 'consecutive_digits_ratio', 'n_grams']
    malicious_token_features = ['malicious_keywords_features', 'contains_malicious_token', 'malicious_token_count', 'top_malicious_tokens']

    all_features = all_san_features + fancy_features + malicious_token_features

    feature_name_mappings = {
        'n_grams': NGramFeature.all_sub_names(),
        'top_malicious_tokens': MaliciousTokenFeature.all_sub_names(),
        'malicious_keywords_features': MaliciousKeywordsFeatures.load(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'malicious_keywords_features.txt')).all_sub_names(),
    }


    hex_digits = set('0123456789abcdef')
    vowels = set('aeiou')

    # Pattern matching ipv4 addresses according to RFC
    ipv4_pattern = re.compile("(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])")

    # Pattern matching visual separation tokens '.', '-' and '_'
    token_separators_regex = re.compile("\.|-|_")

    # Pattern matching idn indicator xn--
    idn_regex = re.compile("xn--")

    _malicious_keywords = None
    _malicious_keywords_features = None

    def __init__(self, all_names, san):
        """Initialize a SAN feature object.

        :param all_names: All names of the certificate.
        :param san: The san for which this object should extract the features.
        """
        super(CertificateSANFeatures, self).__init__()
        self.original_san = san
        self.san = san.lower()
        self.all_names = all_names
        self.tokens = [token.lower() for token in CertificateSANFeatures.token_separators_regex.split(self.san)]

    def get_malicious_keywords():
        if CertificateSANFeatures._malicious_keywords is None:
            with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'malicious_keywords.txt'), 'r') as file:
                keywords = []
                for line in file:
                    if len(line) > 0:
                        token = line.strip().split(' ')[0].lower()
                        keywords.append(token)
                CertificateSANFeatures._malicious_keywords = keywords
        return CertificateSANFeatures._malicious_keywords

    def get_malicious_features_keywords():
        if CertificateSANFeatures._malicious_keywords_features is None:
            with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'malicious_keywords_features.txt'), 'r') as file:
                keywords = []
                for line in file:
                    if len(line) > 0:
                        token = line.strip().split(' ')[0].lower()
                        keywords.append(token)
                CertificateSANFeatures._malicious_keywords_features = keywords
        return CertificateSANFeatures._malicious_keywords_features

    def feature_vector(self, features = None, encoders = {}):
        """See CertificateFeatures.feature_vector for reference."""
        if features is None:
            features = CertificateSANFeatures.all_features

        return CertificateFeatures.build_feature_vector(self, features, encoders)

    def _extract_san(self, certificate = None):
        # dummy extractor for the san to be able to include it in the features list
        return self.san

    #
    # Fancy intermediate features
    #
    @CertificateFeatures.feature_extracting
    def _extract_dot_split(self, certificate = None):
        return tuple(self.san.split('.'))


    @CertificateFeatures.feature_extracting
    def _extract_dot_split_suffix_free(self, certificate = None):
        """Extracts the dot_split_suffix_free and public_suffix intermediate features.
        :returns: The dot_split_suffix_free
        """
        dot_split_suffix_free, public_suffix = PublicSuffixes.split_public_suffix_and_registered_name(self._extract_dot_split(certificate))
        self.public_suffix = public_suffix
        return dot_split_suffix_free

    @CertificateFeatures.feature_extracting
    def _extract_public_suffix(self, certificate = None):
        """Extracts the dot_split_suffix_free and public_suffix intermediate features.
        :returns: The public_suffix
        """
        dot_split_suffix_free, public_suffix = PublicSuffixes.split_public_suffix_and_registered_name(self._extract_dot_split(certificate))
        self.dot_split_suffix_free = dot_split_suffix_free
        return public_suffix

    @CertificateFeatures.feature_extracting
    def _extract_joined_dot_split_suffix_free(self, certificate = None):
        return ''.join(self._extract_dot_split_suffix_free(certificate))

    @CertificateFeatures.feature_extracting
    def _extract_unigram(self, certificate = None):
        ngram_vectorizer = CountVectorizer(analyzer='char', ngram_range=(1, 1))
        counts = ngram_vectorizer.build_analyzer()(self._extract_joined_dot_split_suffix_free(certificate))
        npa = numpy.array(list(Counter(counts).values()), dtype=int)
        return npa

    #
    # Actual features
    #
    @CertificateFeatures.feature_extracting
    def _extract_sub_cn_entropy(self, certificate = None):
        return scipy.stats.entropy(CertificateSANFeatures._getWordFreq(self.san))

    @CertificateFeatures.feature_extracting
    def _extract_sub_cn_is_com(self, certificate = None):
        if self.san.endswith(".com"):
            return 1
        return 0

    @CertificateFeatures.feature_extracting
    def _extract_name_san_entropy(self, certificate = None):
        sans = self.all_names
        san_chars = ""
        for sa in sans:
            san_chars += sa
        if san_chars:
            return scipy.stats.entropy(CertificateSANFeatures._getWordFreq(self.san), CertificateSANFeatures._getWordFreq(san_chars))
        else:
            return -1

    @CertificateFeatures.feature_extracting
    def _extract_has_uppercase_letters(self, certificate = None):
        if any(character.isupper() for character in self.original_san):
            return 1
        return 0

    @CertificateFeatures.feature_extracting
    def _extract_san_to_alexa_entropy(self, certificate = None):
        return scipy.stats.entropy(CertificateSANFeatures._getWordFreq(self.san.replace('*','')), CertificateFeatures.get_alexa_top_one_million_frequencies())


    #
    # Malicious keyword features
    #
    @CertificateFeatures.feature_extracting
    def _extract_contains_malicious_token(self, certificate = None):
        for token in self._extract_dot_split_suffix_free(certificate):
            if token in CertificateSANFeatures.get_malicious_keywords():
                return 1
        return 0

    @CertificateFeatures.feature_extracting
    def _extract_malicious_token_count(self, certificate = None):
        count = 0
        for token in self._extract_dot_split_suffix_free(certificate):
            if token in CertificateSANFeatures.get_malicious_keywords():
                count += 1
        return count

    @CertificateFeatures.feature_extracting
    def _extract_top_malicious_tokens(self, certificate = None):
        top_10_tokens = CertificateSANFeatures.get_malicious_keywords()[:10]
        vector = [0] * 10
        for index, token in enumerate(top_10_tokens):
            if token in self._extract_dot_split_suffix_free(certificate):
                vector[index] = 1
        return vector

    @CertificateFeatures.feature_extracting
    def _extract_malicious_keywords_features(self, certificate = None):
        keywords = CertificateSANFeatures.get_malicious_features_keywords()
        vector = [0] * len(keywords)
        for index, token in enumerate(keywords):
            if token in self._extract_dot_split_suffix_free(certificate):
                vector[index] = 1
        return vector

    #
    # Fancy features
    # Adapted from: https://github.com/fanci-dga-detection/fanci/blob/master/learning/feature_extraction.py
    #
    @CertificateFeatures.feature_extracting
    def _extract_vowel_ratio(self, certificate = None):
        """Ratio of vowels to non-vowels"""
        vowel_count = 0
        alpha_count = 0
        for c in self._extract_joined_dot_split_suffix_free(certificate):
            if c in CertificateSANFeatures.vowels:
                vowel_count += 1
            if c.isalpha():
                alpha_count += 1
        if alpha_count > 0:
            return vowel_count/alpha_count
        else:
            return 0

    @CertificateFeatures.feature_extracting
    def _extract_digit_ratio(self, certificate = None):
        """Determine ratio of digits to domain length"""
        domain = self._extract_joined_dot_split_suffix_free(certificate)
        digit_count = 0
        if len(domain) == 0:
            print(certificate)
            return 0
        for c in domain:
            if c.isdigit():
                digit_count += 1

        return digit_count/len(domain)

    @CertificateFeatures.feature_extracting
    def _extract_special_char_ratio(self, certificate = None):
        """Determine ratio of special characters (*.-_) to domain (=complete san) length"""
        domain = self.san
        special_re = re.compile("\.|-|_|\*") #matches .-_*
        if len(domain) == 0:
            print(certificate)
        return len(special_re.findall(domain))/len(domain)

    @CertificateFeatures.feature_extracting
    def _extract_length(self, certificate = None):
        """Determine domain length"""
        return len(self._extract_san(certificate))

    @CertificateFeatures.feature_extracting
    def _extract_contains_wwwdot(self, certificate = None):
        """1 if 'www. is contained' 0 else"""
        if 'www.' in self._extract_san(certificate):
            return 1
        else:
            return 0

    @CertificateFeatures.feature_extracting
    def _extract_contains_subdomain_of_only_digits(self, certificate = None):
        """Checks if subdomains of only digits are contained."""
        for part in self._extract_dot_split(certificate):
            if not any(character.isalpha() for character in part):
                return 1
        return 0

    @CertificateFeatures.feature_extracting
    def _extract_subdomain_lengths_mean(self, certificate = None):
        """Calculates average subdomain length"""
        overall_len = 0
        for p in self._extract_dot_split_suffix_free(certificate):
            overall_len += len(p)
        return overall_len / len(self._extract_dot_split_suffix_free(certificate))

    @CertificateFeatures.feature_extracting
    def _extract_parts(self, certificate = None):
        """Calculate the number of domain levels present in a domain, where rwth-aachen.de evaluates to 1"""
        return len(self._extract_dot_split_suffix_free(certificate))

    @CertificateFeatures.feature_extracting
    def _extract_contains_ipv4_addr(self, certificate = None):
        """Check if the domain contains a valid IPv4 address."""
        match_v4 = re.search(CertificateSANFeatures.ipv4_pattern, self._extract_san(certificate))
        if match_v4:
            return 1
        else:
            return 0

    @CertificateFeatures.feature_extracting
    def _extract_contains_digits(self, certificate = None):
        if any(char.isdigit() for char in self._extract_san(certificate)):
            return 1
        else:
            return 0

    @CertificateFeatures.feature_extracting
    def _extract_has_valid_tld(self, certificate = None):
        """Checks if the domain ends with a valid TLD"""
        if self._extract_public_suffix(certificate):
            return 1
        return 0

    @CertificateFeatures.feature_extracting
    def _extract_contains_one_char_subdomains(self, certificate = None):
        """Checks if the domain contains subdomains of only one character"""
        parts = self._extract_dot_split(certificate)
        if len(parts) > 2:
            parts = parts[:-1]
        for p in parts:
            if len(p) == 1:
                return 1
        return 0

    @CertificateFeatures.feature_extracting
    def _extract_prefix_repetition(self, certificate = None):
        """Checks if the string is prefix repeating exclusively.
        Example: 123123 and abcabcabc are prefix repeating 1231234 and ababde are not.
        """
        san = self._extract_san(certificate)
        i = (san + san).find(san, 1, -1)
        return 0 if i == -1 else 1

    @CertificateFeatures.feature_extracting
    def _extract_char_diversity(self, certificate = None):
        """Counts different characters, divided by domain length."""
        counter = defaultdict(int)
        domain = self._extract_joined_dot_split_suffix_free(certificate)
        if len(domain) == 0:
            print(certificate)
            return 0
        for c in domain:
            counter[c] += 1
        return len(counter)/len(domain)

    @CertificateFeatures.feature_extracting
    def _extract_contains_tld_as_infix(self, certificate = None):
        """Checks for infixes that are valid TLD endings like .de in 123.de.rwth-aachen.de
        If such a infix is found 1 is returned, 0 else
        """
        for tld in PublicSuffixes.get_valid_tlds():
            if tld[1:] in self._extract_dot_split_suffix_free(certificate):
                return 1
        return 0

    @CertificateFeatures.feature_extracting
    def _extract_n_grams(self, certificate = None):
        """Calculates various statistical features over the 1-,2- and 3-grams of the suffix and dot free domain"""
        feature = []
        feature += CertificateSANFeatures._stats_over_n_grams(self._extract_unigram(certificate))
        for i in range(2,4):
            ngram_vectorizer = CountVectorizer(analyzer='char', ngram_range=(i, i))
            counts = ngram_vectorizer.build_analyzer()(self._extract_joined_dot_split_suffix_free(certificate))
            npa = numpy.array(list(Counter(counts).values()), dtype=int)
            feature += CertificateSANFeatures._stats_over_n_grams(npa)
        return feature

    @CertificateFeatures.feature_extracting
    def _extract_alphabet_size(self, certificate = None):
        """Calculates the alphabet size of the domain"""
        return len(self._extract_unigram(certificate))

    @CertificateFeatures.feature_extracting
    def _extract_shannon_entropy(self, certificate = None):
        """Calculates the Shannon Entropy based on the frequencies of characters."""
        return scipy.stats.entropy(self._extract_unigram(certificate), base=2)

    @CertificateFeatures.feature_extracting
    def _extract_hex_part_ratio(self, certificate = None):
        """Counts all parts that are only hex. Normalized by the overall part count"""
        hex_parts = 0
        for p in self._extract_dot_split_suffix_free(certificate):
            if all(c in CertificateSANFeatures.hex_digits for c in p):
                hex_parts += 1
        return hex_parts / len(self._extract_dot_split_suffix_free(certificate))

    @CertificateFeatures.feature_extracting
    def _extract_underscore_ratio(self, certificate = None):
        """Calculates the ratio of occuring underscores in all domain parts excluding the public suffix"""
        domain = self._extract_joined_dot_split_suffix_free(certificate)
        underscore_counter = 0
        if len(domain) == 0:
            print(certificate)
            return 0
        for c in self._extract_joined_dot_split_suffix_free(certificate):
            if c == '_':
                underscore_counter += 1
        return underscore_counter / len(domain)

    @CertificateFeatures.feature_extracting
    def _extract_ratio_of_repeated_chars(self, certificate = None):
        """Calculates the ratio of characters repeating in the string"""
        repeating = 0
        uni = self._extract_unigram(certificate)
        if len(uni) == 0:
            print(certificate)
            return 0
        for i in self._extract_unigram(certificate):
            if i > 1:
                repeating += 1
        return repeating / len(self._extract_unigram(certificate))

    @CertificateFeatures.feature_extracting
    def _extract_consecutive_consonant_ratio(self, certificate = None):
        """Calculates the ratio of consecutive consonants"""
        consecutive_counter = 0
        domain = self._extract_joined_dot_split_suffix_free(certificate)
        if len(domain) == 0:
            print(certificate)
            return 0
        for p in self._extract_dot_split_suffix_free(certificate):
            counter = 0
            i = 0
            for c in p:
                if c.isalpha() and c not in CertificateSANFeatures.vowels:
                    counter +=1
                else:
                    if counter > 1:
                        consecutive_counter += counter
                    counter = 0
                i += 1
                if i == len(p) and counter > 1:
                    consecutive_counter += counter
        return consecutive_counter / len(domain)

    @CertificateFeatures.feature_extracting
    def _extract_consecutive_digits_ratio(self, certificate = None):
        """Calculates the ratio of consecutive digits"""
        consecutive_counter = 0
        domain = self._extract_joined_dot_split_suffix_free(certificate)
        if len(domain) == 0:
            print(certificate)
            return 0
        for p in self._extract_dot_split_suffix_free(certificate):
            counter = 0
            i = 0
            for c in p:
                if c.isdigit():
                    counter +=1
                else:
                    if counter > 1:
                        consecutive_counter += counter
                    counter = 0
                i += 1
                if i == len(p) and counter > 1:
                    consecutive_counter += counter

        return consecutive_counter / len(domain)

    @CertificateFeatures.feature_extracting
    def _extract_num_dash(self, certificate = None):
        """Calculates the number of dashes (-)."""
        return self.san.count('-')

    @CertificateFeatures.feature_extracting
    def _extract_is_ip(self, certificate = None):
        """Checks if the san is a valid IP address."""
        dot_split = self.san.split(".")
        if len(dot_split) != 4:
            return 0
        for part in dot_split:
            try:
                int_rep = int(part)
                if int_rep > 255 or int_rep < 0:
                    return 0
            except ValueError:
                return 0
        return 1

    @CertificateFeatures.feature_extracting
    def _extract_num_dash_rd(self, certificate = None):
        """Calculates the number of dashes (-) only in the registrable domain."""
        dot_split_suffix_free = self._extract_dot_split_suffix_free(certificate)
        public_suffix = self._extract_public_suffix(certificate)
        rd = ''.join(dot_split_suffix_free[-1:]) + public_suffix
        return rd.count('-')

    @CertificateFeatures.feature_extracting
    def _extract_num_tokens(self, certificate = None):
        """Calculates the number of tokens (parts delimited by '.-_'.).
                We do not match asterisk (*) as it represents other tokens."""
        return len(self.tokens)

    @CertificateFeatures.feature_extracting
    def _extract_longest_token(self, certificate = None):
        """Calculates the number of tokens (parts delimited by '.-_'.).
                We do not match asterisk (*) as it represents other tokens."""
        return max(map(len,self.tokens))

    @CertificateFeatures.feature_extracting
    def _extract_tld_in_token(self, certificate = None):
        """Returns 1 iff a valid tld exactly matches a token from the dot split suffix free."""
        dot_split_suffix_free = self._extract_dot_split_suffix_free(certificate)
        tokens = CertificateSANFeatures.token_separators_regex.split(".".join(dot_split_suffix_free))
        for tld in PublicSuffixes.get_valid_tlds():
            if tld[1:] in tokens:
                return 1
        return 0

    @CertificateFeatures.feature_extracting
    def _extract_https_in_domain(self, certificate = None):
        """Returns 1 iff the domain includes the substring 'https'."""
        dot_split_suffix_free = self._extract_dot_split_suffix_free(certificate)
        domain_string = ".".join(dot_split_suffix_free)
        if 'https' in domain_string:
            return 1
        return 0

    @CertificateFeatures.feature_extracting
    def _extract_is_idn_domain(self, certificate = None):
        """Returns 1 iff at least one domain label starts with 'xn--', which indicates punycode."""
        domain_labels = self.san.split('.')
        for label in domain_labels:
            if CertificateSANFeatures.idn_regex.match(label): # match only at beginning of labels
                return 1
        return 0


    #
    # Static helper functions
    #
    def _getWordFreq(word):
        """Helper for entropy calculation."""
        ret = [0] * (26 + 10 + 4) # a-z + 0-9 + '*', '-', '.', '_'
        for char in word.lower():
            if ((ord(char) < ord('a') or ord(char) > ord('z')) and (ord(char) < ord('0') or ord(char) > ord('9'))):
                if char == "*":
                    ret[35 + 1] += 1
                elif char == ".":
                    ret[35 + 2] += 1
                elif char == "-":
                    ret[35 + 3] += 1
                elif char == "_":
                    ret[35 + 4] += 1
            elif (ord(char) >= ord('0') and ord(char) <= ord('9')):
                ret[26 + ord(char) - ord('0')] += 1
            elif (ord(char) >= ord('a') and ord(char) <= ord('z')):
                ret[ord(char)-ord('a')] += 1
            else:
                pass # Ignore other chars
        return ret

    #
    # Helpers for fancy
    #

    def _stats_over_n_grams(npa):
        """Calculates statistical features over ngrams decoded in numpy arrays
        stddev, median, mean, min, max, quartils, alphabetsize (length of the ngram)
        :param npa: A numpy array for which to calculate the statistics
        """
        if npa.size > 0:
            stats = [npa.std(), numpy.median(npa), npa.mean(), numpy.min(npa), numpy.max(npa), numpy.percentile(npa, 25),
                numpy.percentile(npa, 75)]
        else:
            stats = [-1, -1, -1, -1, -1, -1, -1]
        return stats
