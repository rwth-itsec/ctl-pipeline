"""How to add a MultivaluedFeature?

1) Subclass the MultivaluedFeature class and ocerride the non implemented methods.
2) Add the reference to the names for the multi valued feature name into the `feature_name_mappings`
of either the CertificateFeatures or CertificateSANFeatures depending on where the feature is located.
"""


class MultivaluedFeature:
    def __init__(self, sub_feature_name):
        super(MultivaluedFeature, self).__init__()
        assert sub_feature_name in type(self).all_sub_names(), "The sub feature does not exist for the multivalued feature"
        self.sub_feature_name = sub_feature_name

    @property
    def summary_name(self):
        """The base name of the feature, which would return multiple features in the feature extractor."""
        raise NotImplemented

    def all_sub_names():
        """All the column names, which are extracted by the summary name."""
        raise NotImplemented

    def get_feature_value(self, value):
        """Method to get the value of the sub feature, represented by the instance, from the multi valued feature.

        :param value: A list containing all the feature values (must be in the same order as the all_sub_names list).
        :returns: The corresponding individual value from the multi valued feature.
        """
        index = type(self).all_sub_names().index(self.sub_feature_name)
        assert index is not None, "The sub feature does not exist for the multivalued feature"
        return value[index]


class NGramFeature(MultivaluedFeature):
    _all_sub_names = [
        '1_gram_std', '1_gram_median', '1_gram_mean', '1_gram_min', '1_gram_max', '1_gram_bottom_quartile', '1_gram_top_quartile',
        '2_gram_std', '2_gram_median', '2_gram_mean', '2_gram_min', '2_gram_max', '2_gram_bottom_quartile', '2_gram_top_quartile',
        '3_gram_std', '3_gram_median', '3_gram_mean', '3_gram_min', '3_gram_max', '3_gram_bottom_quartile', '3_gram_top_quartile'
    ]

    def __init__(self, n, metric):
        super(NGramFeature, self).__init__(f'{n}_gram_{metric}')

    @property
    def summary_name(self):
        return 'n_grams'

    def all_sub_names():
        return NGramFeature._all_sub_names


class MaliciousTokenFeature(MultivaluedFeature):
    _all_sub_names = [
        'mal_token_top_1', 'mal_token_top_2', 'mal_token_top_3', 'mal_token_top_4', 'mal_token_top_5',
        'mal_token_top_6', 'mal_token_top_7', 'mal_token_top_8', 'mal_token_top_9', 'mal_token_top_10'
    ]

    def __init__(self, n):
        super(MaliciousTokenFeature, self).__init__(f'mal_token_top_{n}')

    @property
    def summary_name(self):
        return 'top_malicious_tokens'

    def all_sub_names():
        return MaliciousTokenFeature._all_sub_names


class MaliciousKeywordsFeatures(MultivaluedFeature):

    _all_sub_names = None

    def load(keywords_file_path):
        with open(keywords_file_path, 'r') as file:
            keywords = []
            for line in file:
                if len(line) > 0:
                    token = line.strip().split(' ')[0].lower()
                    keywords.append(token)
            MaliciousKeywordsFeatures._all_sub_names = keywords

        MaliciousKeywordsFeatures._all_sub_names = [f'keyword_{keyword}' for keyword in keywords]
        return MaliciousKeywordsFeatures

    @property
    def summary_name(self):
        return 'malicious_keywords_features'

    def all_sub_names():
        return MaliciousKeywordsFeatures._all_sub_names


