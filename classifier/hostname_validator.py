from .public_suffixes import PublicSuffixes
import ipaddress

valid_chars = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8, 'i': 9, 'j': 10, 'k': 11, 'l': 12,
               'm': 13, 'n': 14, 'o': 15, 'p': 16, 'q': 17, 'r': 18, 's': 19, 't': 20, 'u': 21, 'v': 22, 'w': 23,
               'x': 24, 'y': 25, 'z': 26, '0': 27, '1': 28, '2': 29, '3': 30, '4': 31, '5': 32, '6': 33, '7': 34,
               '8': 35, '9': 36, '-': 37, '_': 38, '.': 39, '*': 40}

class HostnameValidationError(Exception):
    pass

def _is_ip(hostname):
    try:
        ip = ipaddress.ip_address(hostname)
        return True
    except ValueError:
        return False

def _is_valid_hostname(hostname):
    if _is_ip(hostname):
        return True
    hostname_split, suffix = PublicSuffixes.split_public_suffix_and_registered_name(hostname.split('.'))
    return len(hostname_split) > 0 and len(hostname_split[0]) > 0 and len(suffix) > 0

def _contains_invalid_characters(hostname):
    return any(character not in valid_chars.keys() for character in hostname)


def assert_valid_hostname(hostname):
    """Validate a string to be a valid hostname.

    The validation checks if the hostname has a correct public suffix and only contains valid characters.

    :param hostname: The hostname to validate.
    :raises HostnameValidationError: Will raise a HostnameValidationError if the hostname is not a valid hostname.
    """

    if not _is_valid_hostname(hostname) or _contains_invalid_characters(hostname):
        raise HostnameValidationError(f'Invalid hostname: "{hostname}"')