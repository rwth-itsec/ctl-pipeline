import sys
from io import StringIO
import numpy as np
import pandas as pd
import logging
import copy
import sklearn
from joblib import Parallel, delayed
import multiprocessing
from sklearn.model_selection import train_test_split
from .feature_extractor import CertificateFeatures, CertificateSANFeatures
from .rf_classifier import MetaClassifier
from . import classifier_training_helper
from . import settings

log = logging.getLogger('log')


def encode_certs(certs, feature_names, used_features):
    from tensorflow.keras.preprocessing import sequence
    certs_encoded = []
    for i in range(len(certs)):
        sans = [[settings.valid_chars[y] for y in x[0].lower()] for x in certs[i].feature_vectors()]
        sans = sequence.pad_sequences(sans, maxlen=settings.maxlen)
        features = np.array([x[1:] for x in certs[i].feature_vectors()])
        features = pd.DataFrame(features, columns=feature_names[1:])
        features = features.loc[:, used_features[1:]]
        features = features.to_numpy(dtype=float)
        certs_encoded += [[[sans[j], features[j]] for j in range(len(sans))]]
    return certs_encoded


class DLClassifier:
    def __init__(self, model = None):
        self.model = model
        self.feature_names = CertificateFeatures.feature_vector_column_names()
        self.used_features = self.feature_names[:]
        self.classification_threshold = 0.99
        self.meta_classifier = MetaClassifier.avg

    def fit(self, X_train, y_train, max_epochs=100, batch_size=128, class_weighting=False):
        assert len(X_train) == len(y_train)

        log.info('Pre-processing %d certificates (%d Subject Alternative Names)', len(X_train),
                 sum([len(x.feature_vectors()) for x in X_train]))

        n_cores = multiprocessing.cpu_count()
        certs = np.array_split(X_train, n_cores)
        certs_encoded = Parallel(n_jobs=n_cores)(
            delayed(encode_certs)(i, self.feature_names, self.used_features) for i in certs)
        certs_encoded = [y for x in certs_encoded for y in x]

        c_train, c_holdout, l_train, l_holdout = train_test_split(certs_encoded, y_train, test_size=0.05,
                                                                  stratify=y_train)
        sans_train = []
        features_train = []
        labels_train = []
        for i in range(len(c_train)):
            for x in c_train[i]:
                sans_train.append(x[0])
                features_train.append(x[1])
                labels_train.append(l_train[i])

        subject_train = np.array(sans_train)
        feature_train = np.array(features_train)
        labels_train = np.array(labels_train)

        best_acc = 0.0
        best_ep = 0
        best_model = self.model

        if class_weighting:
            class_weight = {0: len(labels_train) / len([x for x in labels_train if x == 0]),
                            1: len(labels_train) / len([x for x in labels_train if x == 1])}

        log.info('Start training on %d certificates (%d Subject Alternative Names)', len(c_train), len(subject_train))
        for ep in range(max_epochs):
            old_stdout = sys.stdout
            sys.stdout = mystdout = StringIO()
            if class_weighting:
                self.model.fit([subject_train, feature_train], labels_train, batch_size=batch_size, epochs=1, verbose=2,
                               class_weight=class_weight)
            else:
                self.model.fit([subject_train, feature_train], labels_train, batch_size=batch_size, epochs=1, verbose=2)
            sys.stdout = old_stdout
            log.info('Epoch %d - ' + mystdout.getvalue().split('\n')[1], ep + 1)

            preds_ho, acc_ho, auc_ho = self.predict(c_holdout, l_holdout, encoded=True)

            log.info('Epoch %d - ACC = %f (best=%f), AUC = %f' % (ep + 1, acc_ho, best_acc, auc_ho))

            if acc_ho > best_acc:
                best_acc = acc_ho
                best_ep = ep

                best_model = copy.copy(self.model)

                if acc_ho == 1:
                    break
            else:
                if (ep - best_ep) > 2:
                    break

        self.model = best_model

    def predict(self, X_test, y_test=None, encoded=False, log_confusion_matrix=False, plot_roc=False, filename='dl'):
        if not encoded:
            log.info('Pre-processing %d certificates (%d Subject Alternative Names)', len(X_test),
                     sum([len(x.feature_vectors()) for x in X_test]))

            n_cores = multiprocessing.cpu_count()
            certs = np.array_split(X_test, n_cores)
            certs_encoded = Parallel(n_jobs=n_cores)(
                delayed(encode_certs)(i, self.feature_names, self.used_features) for i in certs)
            certs_encoded = [y for x in certs_encoded for y in x]

        else:
            certs_encoded = X_test

        sans_test = []
        features_test = []

        for i in range(len(certs_encoded)):
            for x in certs_encoded[i]:
                sans_test.append(x[0])
                features_test.append(x[1])

        sans_test = np.array(sans_test)
        features_test = np.array(features_test)

        log.info('Classifying %d certificates (%d Subject Alternative Names)', len(certs_encoded), len(sans_test))
        probs = self.model.predict([sans_test, features_test])

        san_counter = 0
        cert_results = []
        for i in range(len(certs_encoded)):
            cert_results.append(probs[san_counter:san_counter + len(certs_encoded[i])])
            san_counter += len(certs_encoded[i])

        if self.meta_classifier == MetaClassifier.avg:
            res = np.array([[np.average(x)] for x in cert_results])
        elif self.meta_classifier == MetaClassifier.med:
            res = np.array([[np.median(x)] for x in cert_results])
        elif self.meta_classifier == MetaClassifier.min:
            res = np.array([[np.min(x)] for x in cert_results])
        elif self.meta_classifier == MetaClassifier.max:
            res = np.array([[np.max(x)] for x in cert_results])
        else:
            log.error('Unknown MetaClassifier selected')
            exit(1)

        if y_test is not None:
            labels_test = []
            for i in range(len(certs_encoded)):
                labels_test += [y_test[i]] * len(certs_encoded[i])
            labels_test = np.array(labels_test)

            acc_sans = sklearn.metrics.accuracy_score(labels_test, probs > self.classification_threshold)
            auc_sans = sklearn.metrics.roc_auc_score(labels_test, probs)
            log.info('Subject Alternative Names: ACC = %f AUC = %f', acc_sans, auc_sans)

            if log_confusion_matrix:
                confusion_matrix = sklearn.metrics.confusion_matrix(labels_test, probs > self.classification_threshold)
                log.info('Subject Alternative Names: Confusion matrix:\n' + str(confusion_matrix))

            acc = sklearn.metrics.accuracy_score(y_test, res > self.classification_threshold)
            auc = sklearn.metrics.roc_auc_score(y_test, res)
            log.info('Certificates_avg: ACC = %f AUC = %f', acc, auc)
            if log_confusion_matrix:
                confusion_matrix = sklearn.metrics.confusion_matrix(y_test, res > self.classification_threshold)
                log.info('Certificates_avg: Confusion matrix:\n' + str(confusion_matrix))

            if plot_roc:
                classifier_training_helper.plot_roc(labels_test, probs, y_test, cert_results, filename)
                classifier_training_helper.create_threshold_report(labels_test, probs, y_test, cert_results, filename)

            return [res[i][0] for i in range(len(res))], acc_sans, auc_sans

        return [res[i][0] for i in range(len(res))]

    def load(self, path):
        from tensorflow.keras.models import load_model
        log.info('Loading classifier')
        self.model = load_model(path)

    def serialize(self, path):
        self.model.save(path)
        log.info('Classifier serialized')
