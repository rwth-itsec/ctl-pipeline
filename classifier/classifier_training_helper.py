import numpy as np
import sklearn
import csv
import matplotlib
import matplotlib.pyplot as plt

def plot_roc(san_y_test, san_probs, cert_y_test, cert_probs, filename):
    labels = []
    values = []
    cert_results_min = np.array([min(x) for x in cert_probs])
    labels.append('dom-min')
    values.append(cert_results_min)
    cert_results_avg = np.array([[np.average(x)] for x in cert_probs])
    labels.append('dom-avg')
    values.append(cert_results_avg)
    cert_results_median = np.array([[np.median(x)] for x in cert_probs])
    labels.append('dom-med')
    values.append(cert_results_median)
    cert_results_max = np.array([max(x) for x in cert_probs])
    labels.append('dom-max')
    values.append(cert_results_max)

    plt.figure(0, figsize=(7, 5))
    lw = 2
    plt.plot([0, 1.0], [0, 1.0], color='black', lw=lw, linestyle='--')
    plt.xlim([0.0, 0.003])
    plt.ylim([0.0, 0.4])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic')
    color = matplotlib.cm.jet(np.linspace(0, 1, len(labels) + 1))

    fpr, tpr, thresh = sklearn.metrics.roc_curve(san_y_test, san_probs)
    min_fpr = min([x for x in fpr if x != 0])
    tpr_thresh = max([(tpr[i], thresh[i + 1]) for i in range(len(tpr)) if fpr[i] == min_fpr])

    for l in range(len(labels)):
        fpr, tpr, thresh = sklearn.metrics.roc_curve(cert_y_test, values[l])

        min_fpr = min([x for x in fpr if x != 0])
        tpr_thresh = max([(tpr[i], thresh[i + 1]) for i in range(len(tpr)) if fpr[i] == min_fpr])
        text = labels[l]

        plt.plot(fpr, tpr, color=color[l + 1], lw=lw, label=text)

    plt.legend(loc="upper left")
    plt.savefig('results/' + filename + '.pdf')
    plt.close(0)

def create_threshold_report(san_y_test, san_probs, cert_y_test, cert_probs, filename):
    labels = []
    values = []
    results_tpr = []
    results_fpr = []
    cert_results_min = np.array([min(x) for x in cert_probs])
    labels.append('min')
    values.append(cert_results_min)
    cert_results_avg = np.array([[np.average(x)] for x in cert_probs])
    labels.append('avg')
    values.append(cert_results_avg)
    cert_results_median = np.array([[np.median(x)] for x in cert_probs])
    labels.append('med')
    values.append(cert_results_median)
    cert_results_max = np.array([max(x) for x in cert_probs])
    labels.append('max')
    values.append(cert_results_max)

    threshholds = [x for x in np.arange(0.50, 1.001, 0.001)]

    tpr = []
    fpr = []
    for t in threshholds:
        tmp = [1 if x > t else 0 for x in san_probs]
        tp = [1 if san_y_test[i] == 1 and tmp[i] == 1 else 0 for i in range(len(san_y_test))]
        tp = len([x for x in tp if x == 1])
        p = len([x for x in san_y_test if x == 1])
        tpr.append((tp / p))

        fp = [1 if san_y_test[i] == 0 and tmp[i] == 1 else 0 for i in range(len(san_y_test))]
        fp = len([x for x in fp if x == 1])
        n = len([x for x in san_y_test if x == 0])
        fpr.append((fp / n))

    results_tpr.append(tpr)
    results_fpr.append(fpr)

    for l in range(len(labels)):
        tpr = []
        fpr = []
        for t in threshholds:
            tmp = [1 if x > t else 0 for x in values[l]]
            tp = [1 if cert_y_test[i] == 1 and tmp[i] == 1 else 0 for i in range(len(cert_y_test))]
            tp = len([x for x in tp if x == 1])
            p = len([x for x in cert_y_test if x == 1])
            tpr.append((tp / p))

            fp = [1 if cert_y_test[i] == 0 and tmp[i] == 1 else 0 for i in range(len(cert_y_test))]
            fp = len([x for x in fp if x == 1])
            n = len([x for x in cert_y_test if x == 0])
            fpr.append((fp / n))

        results_tpr.append(tpr)
        results_fpr.append(fpr)

    with open('results/' + filename + '.csv', 'w') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',')
        csvwriter.writerow(
            ['Threshold', 'SAN TPR', 'SAN FPR', 'MIN TPR', 'MIN FPR', 'AVG TPR', 'AVG FPR', 'MED TPR', 'MED FPR', 'MAX TPR',
             'MAX FPR'])
        for i in range(len(threshholds)):
            row = [threshholds[i]]
            for j in range(len(results_tpr)):
                row += [results_tpr[j][i], results_fpr[j][i]]
            csvwriter.writerow(row)


