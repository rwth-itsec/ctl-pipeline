import re

matcher = {
    'PayPal' : { 'positive': ['paypal','paypai','paypa1'], 'negative': ['paypal.com'] },
    'Runescape' : { 'positive': ['runescape'], 'negative': ['runescape.com'] },
    'Google' : { 'positive': ['google'], 'negative': {'google.com', 'android.com', 'appengine.google.com', 'cloud.google.com', 'g.co', 'gcp.gvt2.com', 'ggpht.cn', 'google-analytics.com', 'google.ca', 'google.cl', 'google.co.in', 'google.co.jp', 'google.co.uk', 'google.com.ar', 'google.com.au', 'google.com.br', 'google.com.co', 'google.com.mx', 'google.com.tr', 'google.com.vn', 'google.de', 'google.es', 'google.fr', 'google.hu', 'google.it', 'google.nl', 'google.pl', 'google.pt', 'googleadapis.com', 'googleapis.cn', 'googlecommerce.com', 'googlevideo.com', 'gstatic.cn', 'gstatic.com', 'gstaticcnapps.cn', 'gvt1.com', 'gvt2.com', 'metric.gstatic.com', 'urchin.com', 'url.google.com', 'youtube-nocookie.com', 'youtube.com', 'youtubeeducation.com', 'youtubekids.com', 'yt.be', 'ytimg.com', 'android.clients.google.com', 'android.com', 'developer.android.google.cn', 'developers.android.google.cn', 'g.co', 'ggpht.cn', 'goo.gl', 'google-analytics.com', 'google.com', 'googlecommerce.com', 'source.android.google.cn', 'urchin.com', 'www.goo.gl', 'youtu.be', 'youtube.com', 'youtubeeducation.com', 'youtubekids.com', 'yt.be', 'googleusercontent.com', 'apps.googleusercontent.com', 'appspot.com.storage.googleapis.com', 'audiobook-additional-material-staging.googleusercontent.com', 'audiobook-additional-material.googleusercontent.com', 'blogspot.com', 'bp.blogspot.com', 'commondatastorage.googleapis.com', 'content-storage-download.googleapis.com', 'content-storage-upload.googleapis.com', 'content-storage.googleapis.com', 'dev.amp4mail.googleusercontent.com', 'doubleclickusercontent.com', 'ggpht.com', 'googledrive.com', 'googlesyndication.com', 'googleweblight.com', 'playground-internal.amp4mail.googleusercontent.com', 'playground.amp4mail.googleusercontent.com', 'prod.amp4mail.googleusercontent.com', 'safenup.googleusercontent.com', 'sandbox.googleusercontent.com', 'storage-download.googleapis.com', 'storage-upload.googleapis.com', 'storage.googleapis.com', 'storage.select.googleapis.com', 'translate.goog', 'blogspot.com', 'bp.blogspot.com', 'commondatastorage.googleapis.com', 'doubleclickusercontent.com', 'ggpht.com', 'googledrive.com', 'googleusercontent.com', 'googleweblight.com', 'manifest.lh3-da.googleusercontent.com', 'manifest.lh3-db.googleusercontent.com', 'manifest.lh3-dc.googleusercontent.com', 'manifest.lh3-dd.googleusercontent.com', 'manifest.lh3-de.googleusercontent.com', 'manifest.lh3-df.googleusercontent.com', 'manifest.lh3-dg.googleusercontent.com', 'manifest.lh3-dz.googleusercontent.com', 'manifest.lh3.googleusercontent.com', 'manifest.lh3.photos.google.com', 'static.panoramio.com.storage.googleapis.com', 'storage.googleapis.com', 'storage.select.googleapis.com', 'translate.goog', 'unfiltered.news', 'google.us','google.com.sa','appspot.com', 'an.r.appspot.com', 'app.google', 'df.r.appspot.com', 'dt.r.appspot.com', 'el.r.appspot.com', 'ew.r.appspot.com', 'ey.r.appspot.com', 'nn.r.appspot.com', 'nw.r.appspot.com', 'oa.r.appspot.com', 'rj.r.appspot.com', 'thinkwithgoogle.com', 'ts.r.appspot.com', 'uc.r.appspot.com', 'ue.r.appspot.com', 'uk.r.appspot.com', 'withgoogle.com', 'withyoutube.com', 'wl.r.appspot.com', 'app.google', 'appspot.com', 'thinkwithgoogle.com', 'withgoogle.com', 'withyoutube.com','googlecnapps.cn'
            }

            },
    'Apple' : { 'positive': ['apple', 'icloud'], 'negative': ['apple.com','icloud.com'] },
    'Amazon' : { 'positive': ['amazon'], 'negative': ['amazon.com', 'amazon.de', 'amazonaws.com','amazon-corp.com', 'twitch.amazon.eu', 'twitch.amazon.de', 'twitch.amazon.es', 'twitch.amazon.fr', 'twitch.amazon.it', 'twitch.amazon.co.uk', 'twitch.amazon.ae', 'twitch.amazon.in'] },
    'Microsoft' : { 'positive': ['windows','microsoft','onedrive'], 'negative': ['onedrive.com', 'windows.net','live.com', 'msn.com', 'live.net','sharepoint.com', 'sharepoint.apac.microsoftonline.com', 'sharepoint.emea.microsoftonline.com', 'sharepoint.microsoftonline.com','microsoft.com','microsoftazure.de']
            }, # office
    'Facebook' : { 'positive': ['facebook'], 'negative': ['facebook.com'] },
    'Ebay' : { 'positive': ['ebay'], 'negative': ['ebay.com'] },
    'bankofamerica' : { 'positive': ['bankofamerica'], 'negative': ['bankofamerica.com'] },
    'umbler' : { 'positive': ['umbler'], 'negative': ['umbler.com'] },
    'wellsfargo' : { 'positive': ['wellsfargo', 'wells-fargo'], 'negative': ['wellsfargo.com', 'wf.com', 'wellsfargo.net'] },
    'dropbox' : { 'positive': ['dropbox'], 'negative': ['dropbox.com'] },
    'Yahoo' : { 'positive': ['yahoo'], 'negative': ['yahoo.com'] },
    'other' : { 'positive': ['secureserver'], 'negative': [] },
}



class SimpleKeywordClassifier:
    def __init__(self, keyword_dict=matcher):
        self.keywords = keyword_dict
        self.classification_threshold = 1.0

    def fit(self, X, y):
        pass

    def predict(self, certificates):
        return [self.predict_single(certificate) for certificate in certificates]

    def predict_single(self, certificate):
        """Checks if the certificate includes a URL that:
            - DOES include one of the keywords in the keyword dictionary positives
            - DOES NOT have the same (registrable) domain of a known negative
                - This is done by checking if the url has a known negative as suffix
                  and is either of the same length or has a dot in front of the suffix.
        """
        urls = certificate.feature_vectors()
        urls = [vec[0] for vec in urls]
        mal = []
        for url in urls:
            for keyword_index in self.keywords:
                keywords = self.keywords[keyword_index]['positive']
                for keyword in keywords:
                    if keyword in url:
                        is_negative = False
                        for positive in self.keywords[keyword_index]['negative']:
                            if url.endswith(positive) and (len(url) == len(positive) or (len(url) > len(positive) and url[len(url) - len(positive) - 1] == ".")):
                                is_negative = True
                        if is_negative:
                            pass
                        else:
                            mal += url
        if len(mal) > 1:
            # print(urls)
            return 1
        return 0

    token_separators_regex = re.compile("\W|_") # Use string (not byte) regex to match all unicode word chars (not only [a-zA-Z0-9_])

    @staticmethod
    def tokenize_url(url):
        # Pattern matching visual separation tokens '.', '-' and '_'
        tokens = SimpleKeywordClassifier.token_separators_regex.split(url)
        tokens = list(filter(None, tokens))
        return tokens

    def predict_token(self, certificate):
        """Same as above but tokenizes URL and checks if one token is equal to a keyword.
            This might reduce FPR.
        """
        urls = certificate.feature_vectors()
        urls = [vec[0] for vec in urls]
        mal = []
        for url in urls:
            url_tokens = self.tokenize_url(url)
            for keyword_index in self.keywords:
                keywords = self.keywords[keyword_index]['positive']
                for keyword in keywords:
                    if keyword in url_tokens:
                        is_negative = False
                        for positive in self.keywords[keyword_index]['negative']:
                            if url.endswith(positive) and (len(url) == len(positive) or (len(url) > len(positive) and url[len(url) - len(positive) - 1] == ".")):
                                is_negative = True
                        if is_negative:
                            pass
                        else:
                            mal += url
        if len(mal) > 1:
            return 1
        return 0
