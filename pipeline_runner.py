import argparse
import logging
import sys
import os
import time
import datetime
from multiprocessing import Process, Queue
from multiprocessing.queues import Empty as QueueEmptyException

from certificate import CertificateTransparencyLogSource
import pipeline_components
from classifier_configurations import ClassifierConfigurations
from helper import valid_existing_directory_path, valid_existing_file_path, get_env


class PipelineRunner:

    """How many downloaders should run in parallel."""
    NUMBER_OF_DOWNLOADERS = get_env('PL_NUMBER_OF_DOWNLOADERS', 4, int)

    """How many classification runners should be used per classifier."""
    NUMBER_OF_CLASSIFIERS = get_env('PL_NUMBER_OF_CLASSIFIERS', 1, int)

    """How many verification runners should be used per classifier (not per classification runner)."""
    NUMBER_OF_VERIFIERS = get_env('PL_NUMBER_OF_VERIFIERS', 1, int)

    """How many screenshot grabbers should be running per classifier (not per classification runner)."""
    NUMBER_OF_SCREENSHOT_GRABBERS = get_env('PL_NUMBER_OF_SCREENSHOT_GRABBERS', 1, int)

    """How many file writers should be used to save the negatives (per classifier)."""
    NUMBER_OF_NEGATIVES_WRITER = get_env('PL_NUMBER_OF_NEGATIVES_WRITER', 1, int)

    """Number of certificates which should be classified in parallel by the classification pipeline."""
    NUMBER_OF_PARALLEL_CLASSIFICATIONS = 100

    """Write up to 100 results to a file in one batch."""
    FILE_WRITER_BATCH_SIZE = 100

    """Number of certificates, which should be downloaded in one request to the CTL."""
    NUMBER_OF_REQUESTED_CERTIFICATES = 100

    """Maximum size of any queue in the pipeline setup."""
    MAX_QUEUE_SIZE = 10_000


    def __init__(self, configuration):
        self.logger = logging.getLogger('pipeline')
        self._configuration = configuration
        self._setup_logging()

    def start(self):
        processes = []

        # Download stage
        ctl_download_outputs = []
        for _ in self._configuration.classifier_configurations():
            ctl_download_outputs.append(Queue(PipelineRunner.MAX_QUEUE_SIZE))
        assert len(ctl_download_outputs) > 0, 'No classification configurations found.'

        for begin in range(PipelineRunner.NUMBER_OF_DOWNLOADERS):
            historical_downloader = pipeline_components.HistoricalDownloader(ctl_download_outputs[0], self._configuration.ctl, self._configuration.start_index, self._configuration.end_index, begin, PipelineRunner.NUMBER_OF_DOWNLOADERS, PipelineRunner.NUMBER_OF_REQUESTED_CERTIFICATES)
            for output in ctl_download_outputs[1:]:
                historical_downloader.add_additional_output(output) # Add additional outputs
            historical_downloader.set_logger(self.logger)
            processes.append(historical_downloader.process())

        for index, classifier_configuration in enumerate(self._configuration.classifier_configurations()):
            # Classification stage
            classification_output = Queue(PipelineRunner.MAX_QUEUE_SIZE)
            for _ in range(PipelineRunner.NUMBER_OF_CLASSIFIERS):
                processes.append(self._build_classifier_stage(ctl_download_outputs[index], classification_output, classifier_configuration).process())

            # Verification stage
            verification_output_positive = Queue(PipelineRunner.MAX_QUEUE_SIZE)
            verification_output_negative = Queue(PipelineRunner.MAX_QUEUE_SIZE)

            for _ in range(PipelineRunner.NUMBER_OF_VERIFIERS):
                processes.append(self._build_verification_stage(classification_output, verification_output_positive, verification_output_negative).process())

            for i in range(PipelineRunner.NUMBER_OF_NEGATIVES_WRITER):
                processes.append(self._build_file_writer_stage(verification_output_negative, os.path.join(classifier_configuration['negatives_directory'], f'negatives_{i}.csv')).process())

            next_queue = verification_output_positive
            number_of_predecessors = PipelineRunner.NUMBER_OF_VERIFIERS
            if self._configuration.take_screenshots:
                screenshot_output_queue = Queue(PipelineRunner.MAX_QUEUE_SIZE)
                next_queue = screenshot_output_queue
                number_of_predecessors = PipelineRunner.NUMBER_OF_SCREENSHOT_GRABBERS
                for _ in range(PipelineRunner.NUMBER_OF_SCREENSHOT_GRABBERS):
                    processes.append(self._build_screenshot_stage(verification_output_positive, screenshot_output_queue, classifier_configuration['screenshot_base_directory']).process())

            # Store results stage
            processes.append(self._build_saver_stage(next_queue, classifier_configuration['positives_output_db'], number_of_predecessors = number_of_predecessors).process())

        for process in processes:
            process.start()

        try:
            for process in processes:
                process.join()
        except (KeyboardInterrupt, SystemExit):
            pass

    def _build_classifier_stage(self, input_queue, output_queue, classifier_configuration):
        classification_runner = pipeline_components.ClassificationRunner(input_queue, output_queue, classifier_configuration, PipelineRunner.NUMBER_OF_PARALLEL_CLASSIFICATIONS)
        classification_runner.set_logger(self.logger)
        classification_runner.set_number_of_direct_predecessors(int(PipelineRunner.NUMBER_OF_DOWNLOADERS / PipelineRunner.NUMBER_OF_CLASSIFIERS))
        classification_runner.set_number_of_direct_successors(int(PipelineRunner.NUMBER_OF_VERIFIERS / PipelineRunner.NUMBER_OF_CLASSIFIERS))
        return classification_runner

    def _build_verification_stage(self, input_queue, positives_output_queue, negatives_output_queue):
        verifier = pipeline_components.VerificationPipelineRunner(input_queue, positives_output_queue, negatives_output_queue, self._configuration.url_database_path, self._configuration.hash_prefix_database_path)
        verifier.set_logger(self.logger)
        verifier.set_number_of_direct_predecessors(1)
        return verifier

    def _build_screenshot_stage(self, input_queue, output_queue, screenshot_base_directory):
        screenshot_grabber = pipeline_components.ScreenshotGrabber(input_queue, output_queue, screenshot_base_directory, 'true_positives', 'false_positives')
        screenshot_grabber.set_logger(self.logger)
        screenshot_grabber.set_number_of_direct_predecessors(int(PipelineRunner.NUMBER_OF_VERIFIERS / PipelineRunner.NUMBER_OF_SCREENSHOT_GRABBERS))
        return screenshot_grabber

    def _build_saver_stage(self, input_queue, database_path, number_of_predecessors):
        saver = pipeline_components.ResultSaver(input_queue, None, database_path)
        saver.set_logger(self.logger)
        saver.set_number_of_direct_predecessors(number_of_predecessors)
        return saver

    def _build_file_writer_stage(self, input_queue, output_file_path):
        file_writer = pipeline_components.ResultFileWriter(input_queue, None, output_file_path, PipelineRunner.FILE_WRITER_BATCH_SIZE)
        file_writer.set_logger(self.logger)
        file_writer.set_number_of_direct_predecessors(int(PipelineRunner.NUMBER_OF_VERIFIERS / PipelineRunner.NUMBER_OF_NEGATIVES_WRITER))
        return file_writer


    def _setup_logging(self):
        # Setup logging
        self.logger.setLevel(logging.WARNING)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s', datefmt='%Y.%m.%d %H:%M:%S')

        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(logging.WARNING)
        stdout_handler.setFormatter(formatter)
        self.logger.addHandler(stdout_handler)


class PipelineRunnerConfiguration:
    def __init__(self, arguments):
        super(PipelineRunnerConfiguration, self).__init__()
        self._arguments = arguments
        self._timestamp = int(time.time())
        self.output_directory_base = arguments.output_directory
        self.classifier_pathes = arguments.classifiers
        self.log_name = arguments.ct_log_name
        self.start_index = arguments.start_index
        self.end_index = arguments.end_index
        self.url_database_path = arguments.url_database_path
        self.hash_prefix_database_path = arguments.hash_prefix_database_path
        self.ctl = CertificateTransparencyLogSource[self.log_name.upper()]
        self.take_screenshots = arguments.take_screenshots

        self.assert_validity()
        self.prepare_directories()

    def assert_validity(self):
        assert os.path.isdir(self.output_directory_base), f'The base output directory {self.output_directory_base} does not exist.'
        assert os.path.isfile(self.hash_prefix_database_path), 'The given hash prefix database does not exist.'
        assert os.path.isfile(self.url_database_path), 'The given url database does not exist.'
        assert CertificateTransparencyLogSource[self.log_name.upper()] is not None, 'The provided log name is not known.'
        assert self.start_index < self.end_index, 'The start index must be smaller than the end index.'
        assert self.start_index >= 0, 'The start index must not be smaller than 0.'
        for path in self.classifier_pathes:
            self._assert_is_valid_classifier_identifier(path)

    def _assert_is_valid_classifier_identifier(self, identifier):
        if identifier in ClassifierConfigurations.special_classifiers:
            return
        assert os.path.isfile(identifier), f'Cannot find classifier: {identifier}'

    @property
    def base_subdirectory_path(self):
        return os.path.join(self.output_directory_base, f'run_{self.ctl.name}_{self.start_index}_{self.end_index}_{self._timestamp}')

    def prepare_directories(self):
        os.makedirs(self.base_subdirectory_path)
        for classifier_configuration in self.classifier_configurations():
            os.makedirs(classifier_configuration['negatives_directory'])
            os.makedirs(classifier_configuration['screenshot_base_directory'])
            os.makedirs(os.path.join(classifier_configuration['screenshot_base_directory'], 'true_positives'))
            os.makedirs(os.path.join(classifier_configuration['screenshot_base_directory'], 'false_positives'))

        with open(os.path.join(self.base_subdirectory_path, 'used_config.txt'), 'w+') as file:
            file.write(f'Started run at: {datetime.datetime.now()}\n\n')
            for key in self._arguments.__dict__:
                file.write(f"{key}: {self._arguments.__dict__[key]}\n")
            file.write("\nClassifier configurations:\n")
            classifier_configurations = list(self.classifier_configurations())
            for configuration in classifier_configurations:
                file.write(f"- {configuration['name']}\n")
                for key in configuration:
                    file.write(f"{key}: {str(configuration[key] or '-')}\n")
                file.write("\n")

    def classifier_configurations(self):
        for path in self.classifier_pathes:
            name = None
            if path in ClassifierConfigurations.special_classifiers:
                name = path
                path = None
            else:
                name = os.path.splitext(os.path.basename(os.path.normpath(path)))[0]

            classifier_class, thresholds, used_features, meta_classifier = ClassifierConfigurations.configuration_for_classifier(name)
            yield {
                'path': path,
                'name': name,
                'negatives_directory': os.path.join(self.base_subdirectory_path, f'negatives_{name}'),
                'positives_output_db': os.path.join(self.base_subdirectory_path, f'positives_{name}.db'),
                'used_features': used_features,
                'threshold': thresholds,
                'meta_classifier': meta_classifier,
                'classifier_class': classifier_class,
                'screenshot_base_directory': os.path.join(self.base_subdirectory_path, f'screenshots_{name}'),
            }

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Classify a CTL on the cluster.')
    parser.add_argument('--url-database-path', '-u', type=valid_existing_file_path, required=True, help="Path to the url database.")
    parser.add_argument('--hash-prefix-database-path', '-p', type=valid_existing_file_path, required=True, help="Path to a GSB hash prefix database.")
    parser.add_argument('--start-index', '-b', type=int, required=True, help="A start index from which to start classifying the CTL in historical mode.")
    parser.add_argument('--end-index', '-e', type=int, required=True, help="A end index at which to stop classifying the CTL in historical mode.")
    parser.add_argument('--ct-log-name', '-l', type=str, required=True, help="The name of the CT log to use.")
    parser.add_argument('--output-directory', '-o', type=valid_existing_directory_path, required=True, help="A directory where the output should be stores (all files will be placed in a sub-directory).")
    parser.add_argument('--classifiers', '-c', type=str, nargs='+', required=True, help="A list of classifiers to evaluate.")
    parser.add_argument('--take-screenshots', '-s', action='store_true', help="A flag indicating whether or not to take screenshots of the positives.")

    arguments = parser.parse_args()

    if 0 in [PipelineRunner.NUMBER_OF_DOWNLOADERS, PipelineRunner.NUMBER_OF_CLASSIFIERS, PipelineRunner.NUMBER_OF_VERIFIERS, PipelineRunner.NUMBER_OF_NEGATIVES_WRITER]:
        print('At least one downloader, classifier, verifier and negatives writer is needed.')
        exit(1)

    if PipelineRunner.NUMBER_OF_DOWNLOADERS % PipelineRunner.NUMBER_OF_CLASSIFIERS != 0:
        print('The number of number of downloaders cannot be matched equally to the number of classifiers.')
        exit(1)

    if PipelineRunner.NUMBER_OF_VERIFIERS % PipelineRunner.NUMBER_OF_NEGATIVES_WRITER != 0:
        print('The number of number of verifiers cannot be matched equally to the number of negative writers.')
        exit(1)

    if PipelineRunner.NUMBER_OF_VERIFIERS % PipelineRunner.NUMBER_OF_CLASSIFIERS != 0:
        print('The number of number of verifiers cannot be matched equally to the number of classifiers.')
        exit(1)

    if PipelineRunner.NUMBER_OF_VERIFIERS % PipelineRunner.NUMBER_OF_SCREENSHOT_GRABBERS != 0:
        print('The number of number of screenshot grabbers cannot be matched equally to the number of verifiers.')
        exit(1)

    configuration = PipelineRunnerConfiguration(arguments)
    PipelineRunner(configuration).start()
